        Label        -mem-manager-initialize   
        DLabel       $heap-start-ptr           
        DataZ        4                         
        DLabel       $heap-after-ptr           
        DataZ        4                         
        DLabel       $heap-first-free          
        DataZ        4                         
        DLabel       $mmgr-newblock-block      
        DataZ        4                         
        DLabel       $mmgr-newblock-size       
        DataZ        4                         
        PushD        $heap-memory              
        Duplicate                              
        PushD        $heap-start-ptr           
        Exchange                               
        StoreI                                 
        PushD        $heap-after-ptr           
        Exchange                               
        StoreI                                 
        PushI        0                         
        PushD        $heap-first-free          
        Exchange                               
        StoreI                                 
        DLabel       $frame-pointer            
        DataI        0                         
        PushD        $frame-pointer            
        Memtop                                 
        StoreI                                 
        DLabel       $stack-pointer            
        DataI        0                         
        PushD        $stack-pointer            
        Memtop                                 
        StoreI                                 
        Jump         $$main                    
        DLabel       $eat-location-zero        
        DataZ        8                         
        DLabel       $print-format-integer     
        DataC        37                        %% "%d"
        DataC        100                       
        DataC        0                         
        DLabel       $print-format-floating    
        DataC        37                        %% "%g"
        DataC        103                       
        DataC        0                         
        DLabel       $print-format-boolean     
        DataC        37                        %% "%s"
        DataC        115                       
        DataC        0                         
        DLabel       $print-format-string      
        DataC        37                        %% "%s"
        DataC        115                       
        DataC        0                         
        DLabel       $print-format-mixed       
        DataC        37                        %% "%d_%d/%d"
        DataC        100                       
        DataC        95                        
        DataC        37                        
        DataC        100                       
        DataC        47                        
        DataC        37                        
        DataC        100                       
        DataC        0                         
        DLabel       $print-format-fraction    
        DataC        95                        %% "_%d/%d"
        DataC        37                        
        DataC        100                       
        DataC        47                        
        DataC        37                        
        DataC        100                       
        DataC        0                         
        DLabel       $print-format-neg-fraction 
        DataC        45                        %% "-_%d/%d"
        DataC        95                        
        DataC        37                        
        DataC        100                       
        DataC        47                        
        DataC        37                        
        DataC        100                       
        DataC        0                         
        DLabel       $print-format-char        
        DataC        37                        %% "%c"
        DataC        99                        
        DataC        0                         
        DLabel       $print-format-newline     
        DataC        10                        %% "\n"
        DataC        0                         
        DLabel       $print-format-array-start 
        DataC        91                        %% "["
        DataC        0                         
        DLabel       $print-format-array-end   
        DataC        93                        %% "]"
        DataC        0                         
        DLabel       $print-format-array-separator 
        DataC        44                        %% ", "
        DataC        32                        
        DataC        0                         
        DLabel       $print-format-space       
        DataC        32                        %% " "
        DataC        0                         
        DLabel       $print-format-tab         
        DataC        9                         %% "\t"
        DataC        0                         
        DLabel       $boolean-true-string      
        DataC        116                       %% "true"
        DataC        114                       
        DataC        117                       
        DataC        101                       
        DataC        0                         
        DLabel       $boolean-false-string     
        DataC        102                       %% "false"
        DataC        97                        
        DataC        108                       
        DataC        115                       
        DataC        101                       
        DataC        0                         
        DLabel       $errors-general-message   
        DataC        82                        %% "Runtime error: %s\n"
        DataC        117                       
        DataC        110                       
        DataC        116                       
        DataC        105                       
        DataC        109                       
        DataC        101                       
        DataC        32                        
        DataC        101                       
        DataC        114                       
        DataC        114                       
        DataC        111                       
        DataC        114                       
        DataC        58                        
        DataC        32                        
        DataC        37                        
        DataC        115                       
        DataC        10                        
        DataC        0                         
        Label        $$general-runtime-error   
        PushD        $errors-general-message   
        Printf                                 
        Halt                                   
        DLabel       $errors-int-divide-by-zero 
        DataC        105                       %% "integer divide by zero"
        DataC        110                       
        DataC        116                       
        DataC        101                       
        DataC        103                       
        DataC        101                       
        DataC        114                       
        DataC        32                        
        DataC        100                       
        DataC        105                       
        DataC        118                       
        DataC        105                       
        DataC        100                       
        DataC        101                       
        DataC        32                        
        DataC        98                        
        DataC        121                       
        DataC        32                        
        DataC        122                       
        DataC        101                       
        DataC        114                       
        DataC        111                       
        DataC        0                         
        Label        $$i-divide-by-zero        
        PushD        $errors-int-divide-by-zero 
        Jump         $$general-runtime-error   
        DLabel       $errors-float-divide-by-zero 
        DataC        102                       %% "floating divide by zero"
        DataC        108                       
        DataC        111                       
        DataC        97                        
        DataC        116                       
        DataC        105                       
        DataC        110                       
        DataC        103                       
        DataC        32                        
        DataC        100                       
        DataC        105                       
        DataC        118                       
        DataC        105                       
        DataC        100                       
        DataC        101                       
        DataC        32                        
        DataC        98                        
        DataC        121                       
        DataC        32                        
        DataC        122                       
        DataC        101                       
        DataC        114                       
        DataC        111                       
        DataC        0                         
        Label        $$f-divide-by-zero        
        PushD        $errors-float-divide-by-zero 
        Jump         $$general-runtime-error   
        DLabel       $errors-rat-divide-by-zero 
        DataC        114                       %% "rational divide by zero"
        DataC        97                        
        DataC        116                       
        DataC        105                       
        DataC        111                       
        DataC        110                       
        DataC        97                        
        DataC        108                       
        DataC        32                        
        DataC        100                       
        DataC        105                       
        DataC        118                       
        DataC        105                       
        DataC        100                       
        DataC        101                       
        DataC        32                        
        DataC        98                        
        DataC        121                       
        DataC        32                        
        DataC        122                       
        DataC        101                       
        DataC        114                       
        DataC        111                       
        DataC        0                         
        Label        $$r-divide-by-zero        
        PushD        $errors-rat-divide-by-zero 
        Jump         $$general-runtime-error   
        DLabel       $errors-null-array        
        DataC        110                       %% "null array"
        DataC        117                       
        DataC        108                       
        DataC        108                       
        DataC        32                        
        DataC        97                        
        DataC        114                       
        DataC        114                       
        DataC        97                        
        DataC        121                       
        DataC        0                         
        Label        $$a-null-array            
        PushD        $errors-null-array        
        Jump         $$general-runtime-error   
        DLabel       $errors-index-out-of-bounds 
        DataC        105                       %% "index out of bounds"
        DataC        110                       
        DataC        100                       
        DataC        101                       
        DataC        120                       
        DataC        32                        
        DataC        111                       
        DataC        117                       
        DataC        116                       
        DataC        32                        
        DataC        111                       
        DataC        102                       
        DataC        32                        
        DataC        98                        
        DataC        111                       
        DataC        117                       
        DataC        110                       
        DataC        100                       
        DataC        115                       
        DataC        0                         
        Label        $$a-out-of-bounds         
        PushD        $errors-index-out-of-bounds 
        Jump         $$general-runtime-error   
        DLabel       $errors-negative-array-length 
        DataC        110                       %% "negative array length"
        DataC        101                       
        DataC        103                       
        DataC        97                        
        DataC        116                       
        DataC        105                       
        DataC        118                       
        DataC        101                       
        DataC        32                        
        DataC        97                        
        DataC        114                       
        DataC        114                       
        DataC        97                        
        DataC        121                       
        DataC        32                        
        DataC        108                       
        DataC        101                       
        DataC        110                       
        DataC        103                       
        DataC        116                       
        DataC        104                       
        DataC        0                         
        Label        $$a-neg-length            
        PushD        $errors-negative-array-length 
        Jump         $$general-runtime-error   
        DLabel       $errors-deleted-record-access 
        DataC        100                       %% "deleted record access"
        DataC        101                       
        DataC        108                       
        DataC        101                       
        DataC        116                       
        DataC        101                       
        DataC        100                       
        DataC        32                        
        DataC        114                       
        DataC        101                       
        DataC        99                        
        DataC        111                       
        DataC        114                       
        DataC        100                       
        DataC        32                        
        DataC        97                        
        DataC        99                        
        DataC        99                        
        DataC        101                       
        DataC        115                       
        DataC        115                       
        DataC        0                         
        Label        $$rec-deleted-access      
        PushD        $errors-deleted-record-access 
        Jump         $$general-runtime-error   
        DLabel       $errors-no-return-statement 
        DataC        102                       %% "function ended with no return"
        DataC        117                       
        DataC        110                       
        DataC        99                        
        DataC        116                       
        DataC        105                       
        DataC        111                       
        DataC        110                       
        DataC        32                        
        DataC        101                       
        DataC        110                       
        DataC        100                       
        DataC        101                       
        DataC        100                       
        DataC        32                        
        DataC        119                       
        DataC        105                       
        DataC        116                       
        DataC        104                       
        DataC        32                        
        DataC        110                       
        DataC        111                       
        DataC        32                        
        DataC        114                       
        DataC        101                       
        DataC        116                       
        DataC        117                       
        DataC        114                       
        DataC        110                       
        DataC        0                         
        Label        $$l-no-return-statement   
        PushD        $errors-no-return-statement 
        Jump         $$general-runtime-error   
        DLabel       $errors-index-range-invalid 
        DataC        105                       %% "invalid index range. In [i,j] i must be less than j"
        DataC        110                       
        DataC        118                       
        DataC        97                        
        DataC        108                       
        DataC        105                       
        DataC        100                       
        DataC        32                        
        DataC        105                       
        DataC        110                       
        DataC        100                       
        DataC        101                       
        DataC        120                       
        DataC        32                        
        DataC        114                       
        DataC        97                        
        DataC        110                       
        DataC        103                       
        DataC        101                       
        DataC        46                        
        DataC        32                        
        DataC        73                        
        DataC        110                       
        DataC        32                        
        DataC        91                        
        DataC        105                       
        DataC        44                        
        DataC        106                       
        DataC        93                        
        DataC        32                        
        DataC        105                       
        DataC        32                        
        DataC        109                       
        DataC        117                       
        DataC        115                       
        DataC        116                       
        DataC        32                        
        DataC        98                        
        DataC        101                       
        DataC        32                        
        DataC        108                       
        DataC        101                       
        DataC        115                       
        DataC        115                       
        DataC        32                        
        DataC        116                       
        DataC        104                       
        DataC        97                        
        DataC        110                       
        DataC        32                        
        DataC        106                       
        DataC        0                         
        Label        $$s-index-range           
        PushD        $errors-index-range-invalid 
        Jump         $$general-runtime-error   
        DLabel       $errors-arrays-different-length 
        DataC        97                        %% "arrays in zip operation must be of the same length"
        DataC        114                       
        DataC        114                       
        DataC        97                        
        DataC        121                       
        DataC        115                       
        DataC        32                        
        DataC        105                       
        DataC        110                       
        DataC        32                        
        DataC        122                       
        DataC        105                       
        DataC        112                       
        DataC        32                        
        DataC        111                       
        DataC        112                       
        DataC        101                       
        DataC        114                       
        DataC        97                        
        DataC        116                       
        DataC        105                       
        DataC        111                       
        DataC        110                       
        DataC        32                        
        DataC        109                       
        DataC        117                       
        DataC        115                       
        DataC        116                       
        DataC        32                        
        DataC        98                        
        DataC        101                       
        DataC        32                        
        DataC        111                       
        DataC        102                       
        DataC        32                        
        DataC        116                       
        DataC        104                       
        DataC        101                       
        DataC        32                        
        DataC        115                       
        DataC        97                        
        DataC        109                       
        DataC        101                       
        DataC        32                        
        DataC        108                       
        DataC        101                       
        DataC        110                       
        DataC        103                       
        DataC        116                       
        DataC        104                       
        DataC        0                         
        Label        $$a-different-length      
        PushD        $errors-arrays-different-length 
        Jump         $$general-runtime-error   
        DLabel       $errors-empty-array       
        DataC        97                        %% "array in fold must not be empty"
        DataC        114                       
        DataC        114                       
        DataC        97                        
        DataC        121                       
        DataC        32                        
        DataC        105                       
        DataC        110                       
        DataC        32                        
        DataC        102                       
        DataC        111                       
        DataC        108                       
        DataC        100                       
        DataC        32                        
        DataC        109                       
        DataC        117                       
        DataC        115                       
        DataC        116                       
        DataC        32                        
        DataC        110                       
        DataC        111                       
        DataC        116                       
        DataC        32                        
        DataC        98                        
        DataC        101                       
        DataC        32                        
        DataC        101                       
        DataC        109                       
        DataC        112                       
        DataC        116                       
        DataC        121                       
        DataC        0                         
        Label        $$a-empty                 
        PushD        $errors-empty-array       
        Jump         $$general-runtime-error   
        DLabel       $rational-numerator-temp  
        DataZ        4                         
        DLabel       $rational-denominator-temp 
        DataZ        4                         
        DLabel       $return-address           
        DataZ        4                         
        DLabel       $integer-temp             
        DataZ        4                         
        DLabel       $integer-temp2            
        DataZ        4                         
        DLabel       $char-temp                
        DataZ        4                         
        DLabel       $a-indexing-array         
        DataZ        4                         
        DLabel       $a-indexing-index         
        DataZ        4                         
        DLabel       $record-creation-temp     
        DataZ        4                         
        DLabel       $a-datasize-temp          
        DataZ        4                         
        DLabel       $a-address-original       
        DataZ        4                         
        DLabel       $a-address-original2      
        DataZ        4                         
        DLabel       $a-address-copy           
        DataZ        4                         
        DLabel       $a-subtype-size           
        DataZ        4                         
        DLabel       a-element-loc-new         
        DataZ        4                         
        DLabel       a-element-loc-original    
        DataZ        4                         
        DLabel       $s-indexing-string        
        DataZ        4                         
        DLabel       $s-indexing-string2       
        DataZ        4                         
        DLabel       $s-indexing-index-start   
        DataZ        4                         
        DLabel       $s-indexing-index-end     
        DataZ        4                         
        DLabel       $s-indexing-index-start2  
        DataZ        4                         
        DLabel       $s-indexing-index-end2    
        DataZ        4                         
        DLabel       l-sequence                
        DataZ        4                         
        DLabel       l-identifier              
        DataZ        4                         
        DLabel       l-current-index           
        DataZ        4                         
        DLabel       l-end-index               
        DataZ        4                         
        DLabel       l-current-element         
        DataZ        4                         
        Label        $$lowest-terms            
        PushD        $return-address           
        Exchange                               
        StoreI                                 
        Duplicate                              
        JumpFalse    $$r-divide-by-zero        
        PushD        $rational-denominator-temp 
        Exchange                               
        StoreI                                 
        PushD        $rational-numerator-temp  
        Exchange                               
        StoreI                                 
        PushD        $rational-numerator-temp  
        LoadI                                  
        PushD        $rational-denominator-temp 
        LoadI                                  
        Label        -lowest-terms-6-start     
        Duplicate                              
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        Remainder                              
        PushD        $integer-temp             
        LoadI                                  
        Exchange                               
        Duplicate                              
        JumpTrue     -lowest-terms-6-start     
        Pop                                    
        PushD        $rational-numerator-temp  
        LoadI                                  
        Exchange                               
        Divide                                 
        PushD        $rational-denominator-temp 
        LoadI                                  
        PushD        $integer-temp             
        LoadI                                  
        Divide                                 
        Duplicate                              
        JumpNeg      -lowest-terms-6-neg       
        Jump         -lowest-terms-6-join      
        Label        -lowest-terms-6-neg       
        Exchange                               
        Negate                                 
        Exchange                               
        Negate                                 
        Label        -lowest-terms-6-join      
        PushD        $return-address           
        LoadI                                  
        Return                                 
        Label        $$clear-n-bytes           
        PushD        $return-address           
        Exchange                               
        StoreI                                 
        PushI        1                         
        Subtract                               
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        PushD        $integer-temp             
        LoadI                                  
        Label        -clear-n-bytes-7-start    
        Add                                    
        PushI        0                         
        StoreC                                 
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        16                        
        Add                                    
        PushD        $integer-temp             
        LoadI                                  
        PushI        1                         
        Subtract                               
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        PushD        $integer-temp             
        LoadI                                  
        Duplicate                              
        JumpNeg      -clear-n-bytes-7-join     
        Jump         -clear-n-bytes-7-start    
        Label        -clear-n-bytes-7-join     
        Pop                                    
        Pop                                    
        PushD        $return-address           
        LoadI                                  
        Return                                 
        Label        $$deallocate              
        Exchange                               
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        PushI        4                         
        BTAnd                                  
        JumpTrue     -deallocate-8-skip-deallocate 
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        PushI        8                         
        BTAnd                                  
        JumpTrue     -deallocate-8-skip-deallocate 
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        PushI        2                         
        BTAnd                                  
        JumpFalse    -deallocate-8-false       
        Duplicate                              
        PushI        12                        
        Add                                    
        LoadI                                  
        PushI        1                         
        Subtract                               
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        Label        -deallocate-8-loop        
        Duplicate                              
        PushI        16                        
        Add                                    
        PushD        $integer-temp             
        LoadI                                  
        PushI        4                         
        Multiply                               
        Add                                    
        LoadI                                  
        PushD        $integer-temp             
        LoadI                                  
        Exchange                               
        Call         $$deallocate              
        PushI        1                         
        Subtract                               
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        PushD        $integer-temp             
        LoadI                                  
        JumpNeg      -deallocate-8-false       
        Jump         -deallocate-8-loop        
        Label        -deallocate-8-false       
        Duplicate                              
        PushI        4                         
        Add                                    
        PushI        4                         
        StoreI                                 
        Call         -mem-manager-deallocate   
        Return                                 
        Label        -deallocate-8-skip-deallocate 
        Pop                                    
        Return                                 
        DLabel       $usable-memory-start      
        DLabel       $global-memory-block      
        DataZ        24                        
        Label        $$main                    
        PushD        $global-memory-block      
        PushI        0                         
        Add                                    %% asciiStringFor
        PushD        -function-2-func          
        StoreI                                 
        PushD        $global-memory-block      
        PushI        8                         
        Add                                    %% numbers
        PushI        0                         
        Duplicate                              
        JumpNeg      $$a-neg-length            
        Duplicate                              
        PushI        0                         
        Multiply                               
        Duplicate                              
        PushD        $a-datasize-temp          
        Exchange                               
        StoreI                                 
        PushI        16                        
        Add                                    
        Call         -mem-manager-allocate     
        PushD        $record-creation-temp     
        Exchange                               
        StoreI                                 
        PushI        7                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushI        0                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        4                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        16                        
        Add                                    
        PushD        $a-datasize-temp          
        LoadI                                  
        Call         $$clear-n-bytes           
        PushI        0                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        8                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        12                        
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        StoreI                                 
        PushD        $global-memory-block      
        PushI        12                        
        Add                                    %% max
        PushD        -function-5-func          
        Jump         -function-5-end           
        Label        -function-5-func          
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        PushD        $frame-pointer            
        LoadI                                  
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        8                         
        Subtract                               
        Exchange                               
        StoreI                                 
        PushD        $frame-pointer            
        PushD        $stack-pointer            
        LoadI                                  
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        8                         
        Subtract                               
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        Label        -jump-3-continue          
        Label        -compare-10-arg1          
        PushD        $frame-pointer            
        LoadI                                  
        PushI        4                         
        Add                                    %% x
        LoadI                                  
        Label        -compare-10-arg2          
        PushD        $frame-pointer            
        LoadI                                  
        PushI        0                         
        Add                                    %% y
        LoadI                                  
        Label        -compare-10-sub           
        Subtract                               
        JumpPos      -compare-10-true          
        Jump         -compare-10-false         
        Label        -compare-10-true          
        PushI        1                         
        Jump         -compare-10-join          
        Label        -compare-10-false         
        PushI        0                         
        Jump         -compare-10-join          
        Label        -compare-10-join          
        JumpTrue     -condition-11-true        
        Jump         -condition-11-false       
        Label        -condition-11-true        
        PushD        $frame-pointer            
        LoadI                                  
        PushI        4                         
        Add                                    %% x
        LoadI                                  
        Jump         -lambda-4-handshake       
        Jump         -jump-3-break             
        Label        -condition-11-false       
        PushD        $frame-pointer            
        LoadI                                  
        PushI        0                         
        Add                                    %% y
        LoadI                                  
        Jump         -lambda-4-handshake       
        Jump         -jump-3-break             
        Label        -jump-3-break             
        Jump         $$l-no-return-statement   
        Label        -lambda-4-handshake       
        PushD        $frame-pointer            
        LoadI                                  
        PushI        8                         
        Subtract                               
        LoadI                                  
        PushD        $return-address           
        Exchange                               
        StoreI                                 
        PushD        $frame-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        LoadI                                  
        PushD        $frame-pointer            
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        16                        
        Add                                    
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        Exchange                               
        StoreI                                 
        PushD        $return-address           
        LoadI                                  
        Return                                 
        Label        -function-5-end           
        StoreI                                 
        PushD        $global-memory-block      
        PushI        20                        
        Add                                    %% maxity
        PushI        12                        
        PushD        $global-memory-block      
        PushI        8                         
        Add                                    %% numbers
        LoadI                                  
        Duplicate                              
        PushD        $a-address-original       
        Exchange                               
        StoreI                                 
        PushI        12                        
        Add                                    
        LoadI                                  
        PushD        a-length                  
        Exchange                               
        StoreI                                 
        PushD        a-length                  
        LoadI                                  
        JumpFalse    -fold-base-12-end         
        PushI        0                         
        PushD        l-current-index           
        Exchange                               
        StoreI                                 
        Label        -fold-base-12-loop        
        PushD        a-length                  
        LoadI                                  
        PushD        l-current-index           
        LoadI                                  
        Subtract                               
        JumpFalse    -fold-base-12-end         
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        Duplicate                              
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        Duplicate                              
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushD        $a-address-original       
        LoadI                                  
        PushI        16                        
        Add                                    
        PushD        l-current-index           
        LoadI                                  
        PushI        4                         
        Multiply                               
        Add                                    
        LoadI                                  
        StoreI                                 
        PushD        l-current-index           
        LoadI                                  
        PushD        a-length                  
        LoadI                                  
        PushD        $a-address-original       
        LoadI                                  
        PushD        $global-memory-block      
        PushI        12                        
        Add                                    %% max
        LoadI                                  
        CallV                                  
        PushD        $a-address-original       
        Exchange                               
        StoreI                                 
        PushD        a-length                  
        Exchange                               
        StoreI                                 
        PushD        l-current-index           
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        LoadI                                  
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Add                                    
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushI        1                         
        PushD        l-current-index           
        LoadI                                  
        Add                                    
        PushD        l-current-index           
        Exchange                               
        StoreI                                 
        Jump         -fold-base-12-loop        
        Label        -fold-base-12-end         
        StoreI                                 
        PushD        $global-memory-block      
        PushI        20                        
        Add                                    %% maxity
        LoadI                                  
        PushD        $print-format-integer     
        Printf                                 
        Halt                                   
        Label        -function-2-func          
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        PushD        $frame-pointer            
        LoadI                                  
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        8                         
        Subtract                               
        Exchange                               
        StoreI                                 
        PushD        $frame-pointer            
        PushD        $stack-pointer            
        LoadI                                  
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        9                         
        Subtract                               
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushD        $frame-pointer            
        LoadI                                  
        PushI        -9                        
        Add                                    %% char
        PushD        $frame-pointer            
        LoadI                                  
        PushI        0                         
        Add                                    %% x
        LoadI                                  
        PushI        99                        
        Pop                                    
        PushI        127                       
        BTAnd                                  
        StoreC                                 
        PushI        0                         
        PushD        $s-length-temp            
        Exchange                               
        StoreI                                 
        PushD        $s-length-temp            
        LoadI                                  
        PushI        12                        
        Add                                    
        PushI        1                         
        Add                                    
        Call         -mem-manager-allocate     
        PushD        $record-creation-temp     
        Exchange                               
        StoreI                                 
        PushI        6                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushI        9                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        4                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        8                         
        Add                                    
        PushD        $s-length-temp            
        LoadI                                  
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        Duplicate                              
        PushI        12                        
        Add                                    
        PushI        0                         
        Add                                    
        PushI        0                         
        StoreC                                 
        PushD        $frame-pointer            
        LoadI                                  
        PushI        -9                        
        Add                                    %% char
        LoadC                                  
        PushD        $char-temp                
        Exchange                               
        StoreI                                 
        PushD        $s-indexing-string        
        Exchange                               
        StoreI                                 
        PushD        $s-indexing-string        
        LoadI                                  
        PushI        8                         
        Add                                    
        LoadI                                  
        Duplicate                              
        PushD        $s-indexing-index-end     
        Exchange                               
        StoreI                                 
        PushI        1                         
        Add                                    
        Duplicate                              
        PushD        $s-length-temp            
        Exchange                               
        StoreI                                 
        PushD        $s-length-temp            
        Exchange                               
        StoreI                                 
        PushD        $s-length-temp            
        LoadI                                  
        PushI        12                        
        Add                                    
        PushI        1                         
        Add                                    
        Call         -mem-manager-allocate     
        PushD        $record-creation-temp     
        Exchange                               
        StoreI                                 
        PushI        6                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushI        9                         
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        4                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        PushI        8                         
        Add                                    
        PushD        $s-length-temp            
        LoadI                                  
        StoreI                                 
        PushD        $record-creation-temp     
        LoadI                                  
        Duplicate                              
        PushI        8                         
        Add                                    
        PushD        $s-length-temp            
        LoadI                                  
        StoreI                                 
        PushI        0                         
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        PushI        0                         
        PushD        $s-indexing-index-start   
        Exchange                               
        StoreI                                 
        PushD        $s-indexing-index-end     
        LoadI                                  
        JumpFalse    -string-char-concat-9-empty 
        Label        -string-char-concat-9-loop 
        Duplicate                              
        PushI        12                        
        Add                                    
        PushD        $integer-temp             
        LoadI                                  
        Add                                    
        PushD        $s-indexing-string        
        LoadI                                  
        PushI        12                        
        Add                                    
        PushD        $s-indexing-index-start   
        LoadI                                  
        Add                                    
        LoadC                                  
        StoreC                                 
        PushI        1                         
        PushD        $integer-temp             
        LoadI                                  
        Add                                    
        PushD        $integer-temp             
        Exchange                               
        StoreI                                 
        PushI        1                         
        PushD        $s-indexing-index-start   
        LoadI                                  
        Add                                    
        PushD        $s-indexing-index-start   
        Exchange                               
        StoreI                                 
        PushD        $s-indexing-index-end     
        LoadI                                  
        PushD        $s-indexing-index-start   
        LoadI                                  
        Subtract                               
        JumpPos      -string-char-concat-9-loop 
        Label        -string-char-concat-9-empty 
        Duplicate                              
        PushI        12                        
        Add                                    
        PushD        $integer-temp             
        LoadI                                  
        Add                                    
        PushD        $char-temp                
        LoadI                                  
        StoreC                                 
        Jump         -lambda-1-handshake       
        Jump         $$l-no-return-statement   
        Label        -lambda-1-handshake       
        PushD        $frame-pointer            
        LoadI                                  
        PushI        8                         
        Subtract                               
        LoadI                                  
        PushD        $return-address           
        Exchange                               
        StoreI                                 
        PushD        $frame-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        LoadI                                  
        PushD        $frame-pointer            
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        13                        
        Add                                    
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        PushI        4                         
        Subtract                               
        PushD        $stack-pointer            
        Exchange                               
        StoreI                                 
        PushD        $stack-pointer            
        LoadI                                  
        Exchange                               
        StoreI                                 
        PushD        $return-address           
        LoadI                                  
        Return                                 
        Label        -function-2-end           
        Label        -mem-manager-make-tags    
        DLabel       $mmgr-tags-size           
        DataZ        4                         
        DLabel       $mmgr-tags-start          
        DataZ        4                         
        DLabel       $mmgr-tags-available      
        DataZ        4                         
        DLabel       $mmgr-tags-nextptr        
        DataZ        4                         
        DLabel       $mmgr-tags-prevptr        
        DataZ        4                         
        DLabel       $mmgr-tags-return         
        DataZ        4                         
        PushD        $mmgr-tags-return         
        Exchange                               
        StoreI                                 
        PushD        $mmgr-tags-size           
        Exchange                               
        StoreI                                 
        PushD        $mmgr-tags-start          
        Exchange                               
        StoreI                                 
        PushD        $mmgr-tags-available      
        Exchange                               
        StoreI                                 
        PushD        $mmgr-tags-nextptr        
        Exchange                               
        StoreI                                 
        PushD        $mmgr-tags-prevptr        
        Exchange                               
        StoreI                                 
        PushD        $mmgr-tags-prevptr        
        LoadI                                  
        PushD        $mmgr-tags-size           
        LoadI                                  
        PushD        $mmgr-tags-available      
        LoadI                                  
        PushD        $mmgr-tags-start          
        LoadI                                  
        Call         -mem-manager-one-tag      
        PushD        $mmgr-tags-nextptr        
        LoadI                                  
        PushD        $mmgr-tags-size           
        LoadI                                  
        PushD        $mmgr-tags-available      
        LoadI                                  
        PushD        $mmgr-tags-start          
        LoadI                                  
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        Add                                    
        PushI        9                         
        Subtract                               
        Call         -mem-manager-one-tag      
        PushD        $mmgr-tags-return         
        LoadI                                  
        Return                                 
        Label        -mem-manager-one-tag      
        DLabel       $mmgr-onetag-return       
        DataZ        4                         
        DLabel       $mmgr-onetag-location     
        DataZ        4                         
        DLabel       $mmgr-onetag-available    
        DataZ        4                         
        DLabel       $mmgr-onetag-size         
        DataZ        4                         
        DLabel       $mmgr-onetag-pointer      
        DataZ        4                         
        PushD        $mmgr-onetag-return       
        Exchange                               
        StoreI                                 
        PushD        $mmgr-onetag-location     
        Exchange                               
        StoreI                                 
        PushD        $mmgr-onetag-available    
        Exchange                               
        StoreI                                 
        PushD        $mmgr-onetag-size         
        Exchange                               
        StoreI                                 
        PushD        $mmgr-onetag-location     
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $mmgr-onetag-size         
        LoadI                                  
        PushD        $mmgr-onetag-location     
        LoadI                                  
        PushI        4                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $mmgr-onetag-available    
        LoadI                                  
        PushD        $mmgr-onetag-location     
        LoadI                                  
        PushI        8                         
        Add                                    
        Exchange                               
        StoreC                                 
        PushD        $mmgr-onetag-return       
        LoadI                                  
        Return                                 
        Label        -mem-manager-allocate     
        DLabel       $mmgr-alloc-return        
        DataZ        4                         
        DLabel       $mmgr-alloc-size          
        DataZ        4                         
        DLabel       $mmgr-alloc-current-block 
        DataZ        4                         
        DLabel       $mmgr-alloc-remainder-block 
        DataZ        4                         
        DLabel       $mmgr-alloc-remainder-size 
        DataZ        4                         
        PushD        $mmgr-alloc-return        
        Exchange                               
        StoreI                                 
        PushI        18                        
        Add                                    
        PushD        $mmgr-alloc-size          
        Exchange                               
        StoreI                                 
        PushD        $heap-first-free          
        LoadI                                  
        PushD        $mmgr-alloc-current-block 
        Exchange                               
        StoreI                                 
        Label        -mmgr-alloc-process-current 
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        JumpFalse    -mmgr-alloc-no-block-works 
        Label        -mmgr-alloc-test-block    
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        PushI        4                         
        Add                                    
        LoadI                                  
        PushD        $mmgr-alloc-size          
        LoadI                                  
        Subtract                               
        PushI        1                         
        Add                                    
        JumpPos      -mmgr-alloc-found-block   
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        Add                                    
        PushI        9                         
        Subtract                               
        PushI        0                         
        Add                                    
        LoadI                                  
        PushD        $mmgr-alloc-current-block 
        Exchange                               
        StoreI                                 
        Jump         -mmgr-alloc-process-current 
        Label        -mmgr-alloc-found-block   
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        Call         -mem-manager-remove-block 
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        PushI        4                         
        Add                                    
        LoadI                                  
        PushD        $mmgr-alloc-size          
        LoadI                                  
        Subtract                               
        PushI        26                        
        Subtract                               
        JumpNeg      -mmgr-alloc-return-userblock 
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        PushD        $mmgr-alloc-size          
        LoadI                                  
        Add                                    
        PushD        $mmgr-alloc-remainder-block 
        Exchange                               
        StoreI                                 
        PushD        $mmgr-alloc-size          
        LoadI                                  
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        PushI        4                         
        Add                                    
        LoadI                                  
        Exchange                               
        Subtract                               
        PushD        $mmgr-alloc-remainder-size 
        Exchange                               
        StoreI                                 
        PushI        0                         
        PushI        0                         
        PushI        0                         
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        PushD        $mmgr-alloc-size          
        LoadI                                  
        Call         -mem-manager-make-tags    
        PushI        0                         
        PushI        0                         
        PushI        1                         
        PushD        $mmgr-alloc-remainder-block 
        LoadI                                  
        PushD        $mmgr-alloc-remainder-size 
        LoadI                                  
        Call         -mem-manager-make-tags    
        PushD        $mmgr-alloc-remainder-block 
        LoadI                                  
        PushI        9                         
        Add                                    
        Call         -mem-manager-deallocate   
        Jump         -mmgr-alloc-return-userblock 
        Label        -mmgr-alloc-no-block-works 
        PushD        $mmgr-alloc-size          
        LoadI                                  
        PushD        $mmgr-newblock-size       
        Exchange                               
        StoreI                                 
        PushD        $heap-after-ptr           
        LoadI                                  
        PushD        $mmgr-newblock-block      
        Exchange                               
        StoreI                                 
        PushD        $mmgr-newblock-size       
        LoadI                                  
        PushD        $heap-after-ptr           
        LoadI                                  
        Add                                    
        PushD        $heap-after-ptr           
        Exchange                               
        StoreI                                 
        PushI        0                         
        PushI        0                         
        PushI        0                         
        PushD        $mmgr-newblock-block      
        LoadI                                  
        PushD        $mmgr-newblock-size       
        LoadI                                  
        Call         -mem-manager-make-tags    
        PushD        $mmgr-newblock-block      
        LoadI                                  
        PushD        $mmgr-alloc-current-block 
        Exchange                               
        StoreI                                 
        Label        -mmgr-alloc-return-userblock 
        PushD        $mmgr-alloc-current-block 
        LoadI                                  
        PushI        9                         
        Add                                    
        PushD        $mmgr-alloc-return        
        LoadI                                  
        Return                                 
        Label        -mem-manager-deallocate   
        DLabel       $mmgr-dealloc-return      
        DataZ        4                         
        DLabel       $mmgr-dealloc-block       
        DataZ        4                         
        PushD        $mmgr-dealloc-return      
        Exchange                               
        StoreI                                 
        PushI        9                         
        Subtract                               
        PushD        $mmgr-dealloc-block       
        Exchange                               
        StoreI                                 
        PushD        $heap-first-free          
        LoadI                                  
        JumpFalse    -mmgr-bypass-firstFree    
        PushD        $mmgr-dealloc-block       
        LoadI                                  
        PushD        $heap-first-free          
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        Label        -mmgr-bypass-firstFree    
        PushI        0                         
        PushD        $mmgr-dealloc-block       
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushD        $heap-first-free          
        LoadI                                  
        PushD        $mmgr-dealloc-block       
        LoadI                                  
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        Add                                    
        PushI        9                         
        Subtract                               
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        PushI        1                         
        PushD        $mmgr-dealloc-block       
        LoadI                                  
        PushI        8                         
        Add                                    
        Exchange                               
        StoreC                                 
        PushI        1                         
        PushD        $mmgr-dealloc-block       
        LoadI                                  
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        Add                                    
        PushI        9                         
        Subtract                               
        PushI        8                         
        Add                                    
        Exchange                               
        StoreC                                 
        PushD        $mmgr-dealloc-block       
        LoadI                                  
        PushD        $heap-first-free          
        Exchange                               
        StoreI                                 
        PushD        $mmgr-dealloc-return      
        LoadI                                  
        Return                                 
        Label        -mem-manager-remove-block 
        DLabel       $mmgr-remove-return       
        DataZ        4                         
        DLabel       $mmgr-remove-block        
        DataZ        4                         
        DLabel       $mmgr-remove-prev         
        DataZ        4                         
        DLabel       $mmgr-remove-next         
        DataZ        4                         
        PushD        $mmgr-remove-return       
        Exchange                               
        StoreI                                 
        PushD        $mmgr-remove-block        
        Exchange                               
        StoreI                                 
        PushD        $mmgr-remove-block        
        LoadI                                  
        PushI        0                         
        Add                                    
        LoadI                                  
        PushD        $mmgr-remove-prev         
        Exchange                               
        StoreI                                 
        PushD        $mmgr-remove-block        
        LoadI                                  
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        Add                                    
        PushI        9                         
        Subtract                               
        PushI        0                         
        Add                                    
        LoadI                                  
        PushD        $mmgr-remove-next         
        Exchange                               
        StoreI                                 
        Label        -mmgr-remove-process-prev 
        PushD        $mmgr-remove-prev         
        LoadI                                  
        JumpFalse    -mmgr-remove-no-prev      
        PushD        $mmgr-remove-next         
        LoadI                                  
        PushD        $mmgr-remove-prev         
        LoadI                                  
        Duplicate                              
        PushI        4                         
        Add                                    
        LoadI                                  
        Add                                    
        PushI        9                         
        Subtract                               
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        Jump         -mmgr-remove-process-next 
        Label        -mmgr-remove-no-prev      
        PushD        $mmgr-remove-next         
        LoadI                                  
        PushD        $heap-first-free          
        Exchange                               
        StoreI                                 
        Label        -mmgr-remove-process-next 
        PushD        $mmgr-remove-next         
        LoadI                                  
        JumpFalse    -mmgr-remove-done         
        PushD        $mmgr-remove-prev         
        LoadI                                  
        PushD        $mmgr-remove-next         
        LoadI                                  
        PushI        0                         
        Add                                    
        Exchange                               
        StoreI                                 
        Label        -mmgr-remove-done         
        PushD        $mmgr-remove-return       
        LoadI                                  
        Return                                 
        DLabel       $heap-memory              
