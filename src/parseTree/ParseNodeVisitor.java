package parseTree;

import parseTree.nodeTypes.CreateEmptyArrayNode;
import parseTree.nodeTypes.ArrayNode;
import parseTree.nodeTypes.AssignmentNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallNode;
import parseTree.nodeTypes.CharConstantNode;
import parseTree.nodeTypes.ConditionalNode;
import parseTree.nodeTypes.BlockNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ErrorNode;
import parseTree.nodeTypes.ExpressionListNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.JumpNode;
import parseTree.nodeTypes.LambdaNode;
import parseTree.nodeTypes.MapNode;
import parseTree.nodeTypes.FloatingConstantNode;
import parseTree.nodeTypes.FoldNode;
import parseTree.nodeTypes.ForNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.FunctionNode;
import parseTree.nodeTypes.NewlineNode;
import parseTree.nodeTypes.ParameterListNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProcedureBlockNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReduceNode;
import parseTree.nodeTypes.ReleaseArrayNode;
import parseTree.nodeTypes.ReturnNode;
import parseTree.nodeTypes.SpaceNode;
import parseTree.nodeTypes.StringConstantNode;
import parseTree.nodeTypes.TabNode;
import parseTree.nodeTypes.TrinaryOperatorNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.VoidNode;
import parseTree.nodeTypes.ZipNode;

// Visitor pattern with pre- and post-order visits
public interface ParseNodeVisitor {
	
	// non-leaf nodes: visitEnter and visitLeave
	void visitEnter(BinaryOperatorNode node);
	void visitLeave(BinaryOperatorNode node);
	
	void visitEnter(TrinaryOperatorNode node);
	void visitLeave(TrinaryOperatorNode node);
	
	void visitEnter(UnaryOperatorNode node);
	void visitLeave(UnaryOperatorNode node);
	
	void visitEnter(CreateEmptyArrayNode node);
	void visitLeave(CreateEmptyArrayNode node);
	
	void visitEnter(ReleaseArrayNode node);
	void visitLeave(ReleaseArrayNode node);
	
	void visitEnter(ExpressionListNode node);
	void visitLeave(ExpressionListNode node);
	
	void visitEnter(BlockNode node);
	void visitLeave(BlockNode node);
	
	void visitEnter(ProcedureBlockNode node);
	void visitLeave(ProcedureBlockNode node);
	
	void visitEnter(DeclarationNode node);
	void visitLeave(DeclarationNode node);

	void visitEnter(AssignmentNode node);
	void visitLeave(AssignmentNode node);
	
	void visitEnter(ParseNode node);
	void visitLeave(ParseNode node);
	
	void visitEnter(PrintStatementNode node);
	void visitLeave(PrintStatementNode node);
	
	void visitEnter(ConditionalNode node);
	void visitLeave(ConditionalNode node);
	
	void visitEnter(ProgramNode node);
	void visitLeave(ProgramNode node);
	
	void visitEnter(FunctionNode node);
	void visitLeave(FunctionNode node);
	
	void visitEnter(FunctionInvocationNode node);
	void visitLeave(FunctionInvocationNode node);
	
	void visitEnter(ParameterListNode node);
	void visitLeave(ParameterListNode node);
	
	void visitEnter(LambdaNode node);
	void visitLeave(LambdaNode node);
	
	void visitEnter(ReturnNode node);
	void visitLeave(ReturnNode node);
	
	void visitEnter(CallNode node);
	void visitLeave(CallNode node);
	
	void visitEnter(ForNode node);
	void visitLeave(ForNode node);
	
	void visitEnter(MapNode node);
	void visitLeave(MapNode node);
	
	void visitEnter(ReduceNode node);
	void visitLeave(ReduceNode node);

	void visitEnter(ZipNode node);
	void visitLeave(ZipNode node);
	
	void visitEnter(FoldNode node);
	void visitLeave(FoldNode node);
	
	// leaf nodes: visitLeaf only
	void visit(BooleanConstantNode node);
	void visit(ErrorNode node);
	void visit(IdentifierNode node);
	void visit(IntegerConstantNode node);
	void visit(FloatingConstantNode node);
	void visit(CharConstantNode node);
	void visit(StringConstantNode node);
	void visit(NewlineNode node);
	void visit(SpaceNode node);
	void visit(TabNode node);
	void visit(ArrayNode node);
	void visit(JumpNode node);
	void visit(VoidNode node);

	
	public static class Default implements ParseNodeVisitor
	{
		public void defaultVisit(ParseNode node) {	}
		public void defaultVisitEnter(ParseNode node) {
			defaultVisit(node);
		}
		public void defaultVisitLeave(ParseNode node) {
			defaultVisit(node);
		}		
		public void defaultVisitForLeaf(ParseNode node) {
			defaultVisit(node);
		}
		
		public void visitEnter(BinaryOperatorNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(BinaryOperatorNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(TrinaryOperatorNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(TrinaryOperatorNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(UnaryOperatorNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(UnaryOperatorNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(CreateEmptyArrayNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(CreateEmptyArrayNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ReleaseArrayNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ReleaseArrayNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ExpressionListNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ExpressionListNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(DeclarationNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(DeclarationNode node) {
			defaultVisitLeave(node);
		}				
		public void visitEnter(AssignmentNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(AssignmentNode node) {
			defaultVisitLeave(node);
		}		
		public void visitEnter(BlockNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(BlockNode node) {
			defaultVisitLeave(node);
		}			
		public void visitEnter(ProcedureBlockNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ProcedureBlockNode node) {
			defaultVisitLeave(node);
		}			
		public void visitEnter(ParseNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ParseNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(PrintStatementNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(PrintStatementNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ProgramNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ProgramNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(FunctionNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(FunctionNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(FunctionInvocationNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(FunctionInvocationNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ConditionalNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ConditionalNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ParameterListNode node) {
			defaultVisitEnter(node);
		}
		public void visitLeave(ParameterListNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(LambdaNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(LambdaNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ReturnNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(ReturnNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(CallNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(CallNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ForNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(ForNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(MapNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(MapNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ReduceNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(ReduceNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(ZipNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(ZipNode node) {
			defaultVisitLeave(node);
		}
		public void visitEnter(FoldNode node) {
			defaultVisitLeave(node);
		}
		public void visitLeave(FoldNode node) {
			defaultVisitLeave(node);
		}
		

		public void visit(BooleanConstantNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(ErrorNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(IdentifierNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(IntegerConstantNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(CharConstantNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(StringConstantNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(FloatingConstantNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(NewlineNode node) {
			defaultVisitForLeaf(node);
		}	
		public void visit(SpaceNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(TabNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(ArrayNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(JumpNode node) {
			defaultVisitForLeaf(node);
		}
		public void visit(VoidNode node) {
			defaultVisitForLeaf(node);
		}


	}


	
}
