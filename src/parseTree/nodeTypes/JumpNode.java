package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import lexicalAnalyzer.Keyword;
import tokens.LextantToken;
import tokens.Token;

public class JumpNode extends ParseNode {
	
	String breakLabel;
	String continueLabel;
	
	public JumpNode(Token token) {
		super(token);
		assert(token.isLextant(Keyword.CONTINUE, Keyword.BREAK));
	}
	public JumpNode(ParseNode node) {
		super(node);
	}

////////////////////////////////////////////////////////////
// attributes
	
	public void setBreakLabel(String breakLabel) {
		this.breakLabel = breakLabel;
	}
	
	public void setContinueLabel(String continueLabel) {
		this.continueLabel = continueLabel;
	}
	
	public String getBreakLabel() {
		return breakLabel;
	}
	
	public String getContinueLabel() {
		return continueLabel;
	}


///////////////////////////////////////////////////////////
// accept a visitor
	
	public void accept(ParseNodeVisitor visitor) {
		visitor.visit(this);
	}

}
