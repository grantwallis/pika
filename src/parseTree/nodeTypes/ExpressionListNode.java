package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.signatures.FunctionSignature;

import java.util.ArrayList;

import lexicalAnalyzer.Lextant;
import tokens.LextantToken;
import tokens.Token;

public class ExpressionListNode extends ParseNode {

	public ExpressionListNode(Token token) {
		super(token);
		assert(token instanceof LextantToken);
	}

	public ExpressionListNode(ParseNode node) {
		super(node);
	}
	
	
	
	////////////////////////////////////////////////////////////
	// attributes
	
	public Lextant getOperator() {
		return lextantToken().getLextant();
	}
	public LextantToken lextantToken() {
		return (LextantToken)token;
	}	
	
	private FunctionSignature signature =
			FunctionSignature.nullInstance();
	
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static ExpressionListNode withChildren(Token token, ArrayList<ParseNode> list) {
		ExpressionListNode node = new ExpressionListNode(token);
		for (ParseNode n : list) {
			node.appendChild(n);
		}
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
	
	///////////////////////////////////////////////////////////
	// getters & setters
	
	public final FunctionSignature getSignature() {
		return signature;
	}
	public final void setSignature(FunctionSignature signature) {
		this.signature = signature;
	}
}
