package parseTree.nodeTypes;


import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class CallNode extends ParseNode {

	public CallNode(Token token) {
		super(token);
	}

	public CallNode(ParseNode node) {
		super(node);
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static CallNode withChild(Token token, ParseNode invocation) {
		CallNode node = new CallNode(token);
		node.appendChild(invocation);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
