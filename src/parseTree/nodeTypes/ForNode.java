package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import asmCodeGenerator.Labeller;
import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Lextant;
import tokens.LextantToken;
import tokens.Token;

public class ForNode extends ParseNode {
	
	Labeller labeller = new Labeller("for");
	private String continueLabel = labeller.newLabel("continue");
	private String loopLabel = labeller.newLabel("loop");
	private String endLabel = labeller.newLabel("end");
	
	public ForNode(Token token) {
		super(token);
		assert(token.isLextant(Keyword.ELEM, Keyword.INDEX));
	}

	public ForNode(ParseNode node) {
		super(node);
	}
	
	
	
	////////////////////////////////////////////////////////////
	// attributes
	
	public Lextant getOperator() {
		return lextantToken().getLextant();
	}
	public LextantToken lextantToken() {
		return (LextantToken)token;
	}	
	public String getLoopLabel() {
		return loopLabel;
	}
	public String getEndLabel() {
		return endLabel;
	}
	public String getContinueLabel() {
		return continueLabel;
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static ForNode withChildren(Token token, ParseNode expression, ParseNode identifier, ParseNode body) {
		ForNode node = new ForNode(token);
		node.appendChild(expression);
		node.appendChild(identifier);
		node.appendChild(body);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
