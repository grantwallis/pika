package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import asmCodeGenerator.Labeller;
import lexicalAnalyzer.Keyword;

import tokens.Token;

public class ConditionalNode extends ParseNode {
	Labeller labeller = new Labeller("jump");
	private String breakLabel = labeller.newLabel("break");
	private String continueLabel = labeller.newLabel("continue");

	public ConditionalNode(Token token) {
		super(token);
		assert(token.isLextant(Keyword.IF) || token.isLextant(Keyword.WHILE));
	}

	public ConditionalNode(ParseNode node) {
		super(node);
	}
	
	
	////////////////////////////////////////////////////////////
	// attributes
	
	public String getBreakLabel() {
		return breakLabel;
	}
	
	public String getContinueLabel() {
		return continueLabel;
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static ConditionalNode withChildren(Token token, ParseNode condition, ParseNode ifBranch, ParseNode elseBranch) {
		ConditionalNode node = new ConditionalNode(token);
		node.appendChild(condition);
		node.appendChild(ifBranch);
		node.appendChild(elseBranch);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
