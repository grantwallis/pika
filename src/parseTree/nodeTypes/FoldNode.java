package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import lexicalAnalyzer.Keyword;
import tokens.Token;

public class FoldNode extends ParseNode {

	public FoldNode(Token token) {
		super(token);
		assert(token.isLextant(Keyword.FOLD));
	}

	public FoldNode(ParseNode node) {
		super(node);
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static FoldNode withBase(Token token, ParseNode left, ParseNode right, ParseNode base) {
		FoldNode node = new FoldNode(token);
		node.appendChild(left);
		node.appendChild(right);
		node.appendChild(base);
		return node;
	}
	public static FoldNode withoutBase(Token token, ParseNode left, ParseNode right) {
		FoldNode node = new FoldNode(token);
		node.appendChild(left);
		node.appendChild(right);
		return node;
	}
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
	

}
