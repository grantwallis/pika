package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import lexicalAnalyzer.Keyword;
import tokens.Token;

public class ZipNode extends ParseNode {

	public ZipNode(Token token) {
		super(token);
		assert(token.isLextant(Keyword.ZIP));
	}

	public ZipNode(ParseNode node) {
		super(node);
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static ZipNode withChildren(Token token, ParseNode left, ParseNode right, ParseNode lambda) {
		ZipNode node = new ZipNode(token);
		node.appendChild(left);
		node.appendChild(right);
		node.appendChild(lambda);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
	

}
