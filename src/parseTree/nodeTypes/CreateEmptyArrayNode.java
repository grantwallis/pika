package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.signatures.FunctionSignature;
import lexicalAnalyzer.Lextant;
import tokens.LextantToken;
import tokens.Token;

public class CreateEmptyArrayNode extends ParseNode {

	public CreateEmptyArrayNode(Token token) {
		super(token);
		assert(token instanceof LextantToken);
	}

	public CreateEmptyArrayNode(ParseNode node) {
		super(node);
	}
	
	
	
	////////////////////////////////////////////////////////////
	// attributes
	
	public Lextant getOperator() {
		return lextantToken().getLextant();
	}
	public LextantToken lextantToken() {
		return (LextantToken)token;
	}	
	
	private FunctionSignature signature =
			FunctionSignature.nullInstance();
	
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static CreateEmptyArrayNode withChildren(Token token, ParseNode left, ParseNode right) {
		CreateEmptyArrayNode node = new CreateEmptyArrayNode(token);
		node.appendChild(left);
		node.appendChild(right);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
	
	///////////////////////////////////////////////////////////
	// getters & setters
	
	public final FunctionSignature getSignature() {
		return signature;
	}
	public final void setSignature(FunctionSignature signature) {
		this.signature = signature;
	}
}
