package parseTree.nodeTypes;

import asmCodeGenerator.Labeller;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import tokens.Token;

public class FunctionNode extends ParseNode {
	
	Labeller labeller = new Labeller("function");
	private String funcLabel = labeller.newLabel("func");
	private String endLabel = labeller.newLabel("end");

	public FunctionNode(Token token) {
		super(token);
	}

	public FunctionNode(ParseNode node) {
		super(node);
	}
	
	////////////////////////////////////////////////////////////
	// attributes

	public String getFunctionLabel() {
		return funcLabel;
	}
	
	public String getEndLabel() {
		return endLabel;
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static FunctionNode withChildren(Token token, ParseNode declaredName, ParseNode lambda) {
		FunctionNode node = new FunctionNode(token);
		node.appendChild(declaredName);
		node.appendChild(lambda);
		return node;
	}
	
	public static FunctionNode withChild(Token token, ParseNode lambda) {
		FunctionNode node = new FunctionNode(token);
		node.appendChild(lambda);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
