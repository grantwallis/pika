package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.signatures.FunctionSignature;
import asmCodeGenerator.Labeller;
import lexicalAnalyzer.Lextant;
import tokens.LextantToken;
import tokens.Token;

public class LambdaNode extends ParseNode {
	
	Labeller labeller = new Labeller("lambda");
	private String handshakeLabel = labeller.newLabel("handshake");

	public LambdaNode(Token token) {
		super(token);
	}

	public LambdaNode(ParseNode node) {
		super(node);
	}
	
	
	
	////////////////////////////////////////////////////////////
	// attributes
	
	public Lextant getOperator() {
		return lextantToken().getLextant();
	}
	public LextantToken lextantToken() {
		return (LextantToken)token;
	}	
	public String getHandshakeLabel() {
		return handshakeLabel;
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static LambdaNode withChildren(Token token, ParseNode parameters, ParseNode returnType, ParseNode body) {
		LambdaNode node = new LambdaNode(token);
		node.appendChild(parameters);
		node.appendChild(returnType);
		node.appendChild(body);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
}
