package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import semanticAnalyzer.signatures.FunctionSignature;
import lexicalAnalyzer.Lextant;
import tokens.LextantToken;
import tokens.Token;

public class ReleaseArrayNode extends ParseNode {

	public ReleaseArrayNode(Token token) {
		super(token);
		assert(token instanceof LextantToken);
	}

	public ReleaseArrayNode(ParseNode node) {
		super(node);
	}
	
	
	
	////////////////////////////////////////////////////////////
	// attributes
	
	public Lextant getOperator() {
		return lextantToken().getLextant();
	}
	public LextantToken lextantToken() {
		return (LextantToken)token;
	}	
	
	private FunctionSignature signature =
			FunctionSignature.nullInstance();
	
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static ReleaseArrayNode withChild(Token token, ParseNode left) {
		ReleaseArrayNode node = new ReleaseArrayNode(token);
		node.appendChild(left);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
	
	///////////////////////////////////////////////////////////
	// getters & setters
	
	public final FunctionSignature getSignature() {
		return signature;
	}
	public final void setSignature(FunctionSignature signature) {
		this.signature = signature;
	}
}
