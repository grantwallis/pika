package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import lexicalAnalyzer.Keyword;
import tokens.Token;

public class ReduceNode extends ParseNode {

	public ReduceNode(Token token) {
		super(token);
		assert(token.isLextant(Keyword.REDUCE));
	}

	public ReduceNode(ParseNode node) {
		super(node);
	}
	
	////////////////////////////////////////////////////////////
	// convenience factory
	
	public static ReduceNode withChildren(Token token, ParseNode left, ParseNode right) {
		ReduceNode node = new ReduceNode(token);
		node.appendChild(left);
		node.appendChild(right);
		return node;
	}
	
	
	///////////////////////////////////////////////////////////
	// boilerplate for visitors
			
	public void accept(ParseNodeVisitor visitor) {
		visitor.visitEnter(this);
		visitChildren(visitor);
		visitor.visitLeave(this);
	}
	

}
