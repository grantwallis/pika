package parseTree.nodeTypes;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import lexicalAnalyzer.Keyword;
import tokens.LextantToken;
import tokens.Token;

public class ArrayNode extends ParseNode {
	public ArrayNode(Token token) {
		super(token);
	}
	public ArrayNode(ParseNode node) {
		super(node);
	}

////////////////////////////////////////////////////////////
// attributes
	
//	public String getValue() {
//		return lextantToken().getValue();
//	}
//
//	public LextantToken lextantToken() {
//		return (LextantToken)token;
//	}	

///////////////////////////////////////////////////////////
// accept a visitor
	
	public void accept(ParseNodeVisitor visitor) {
		visitor.visit(this);
	}

}
