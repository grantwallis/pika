package semanticAnalyzer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Lextant;
import lexicalAnalyzer.Punctuator;
import logging.PikaLogger;
import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import parseTree.nodeTypes.CreateEmptyArrayNode;
import parseTree.nodeTypes.ArrayNode;
import parseTree.nodeTypes.AssignmentNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallNode;
import parseTree.nodeTypes.BlockNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ErrorNode;
import parseTree.nodeTypes.ExpressionListNode;
import parseTree.nodeTypes.FloatingConstantNode;
import parseTree.nodeTypes.FoldNode;
import parseTree.nodeTypes.ForNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.FunctionNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.JumpNode;
import parseTree.nodeTypes.LambdaNode;
import parseTree.nodeTypes.MapNode;
import parseTree.nodeTypes.CharConstantNode;
import parseTree.nodeTypes.ConditionalNode;
import parseTree.nodeTypes.NewlineNode;
import parseTree.nodeTypes.ParameterListNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProcedureBlockNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReduceNode;
import parseTree.nodeTypes.ReleaseArrayNode;
import parseTree.nodeTypes.ReturnNode;
import parseTree.nodeTypes.SpaceNode;
import parseTree.nodeTypes.StringConstantNode;
import parseTree.nodeTypes.TabNode;
import parseTree.nodeTypes.TrinaryOperatorNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.ZipNode;
import semanticAnalyzer.signatures.FunctionSignature;
import semanticAnalyzer.signatures.FunctionSignatures;
import semanticAnalyzer.types.Array;
import semanticAnalyzer.types.Lambda;
import semanticAnalyzer.types.PrimitiveType;
import semanticAnalyzer.types.Type;
import symbolTable.Binding;
import symbolTable.Scope;
import tokens.FloatingConstantToken;
import tokens.IntegerConstantToken;
import tokens.LextantToken;
import tokens.Token;

class SemanticAnalysisVisitor extends ParseNodeVisitor.Default {
	@Override
	public void visitLeave(ParseNode node) {
		throw new RuntimeException("Node class unimplemented in SemanticAnalysisVisitor: " + node.getClass());
	}
	
	///////////////////////////////////////////////////////////////////////////
	// constructs larger than statements
	@Override
	public void visitEnter(ProgramNode node) {
		enterProgramScope(node);
	}
	public void visitLeave(ProgramNode node) {
		leaveScope(node);
	}
	public void visitEnter(BlockNode node) {
		enterSubscope(node);
	}
	public void visitLeave(BlockNode node) {
		leaveScope(node);
	}
	public void visitEnter(FunctionNode node) {
		if(node.child(0) instanceof LambdaNode) {
			ArrayList<Type> types = new ArrayList<Type>();
			LambdaNode lambdaNode = (LambdaNode)node.child(0);
			for(ParseNode p : lambdaNode.child(0).getChildren()) {
				types.add(p.getType());
			}
			Type returnType = lambdaNode.child(1).getType();
			Lambda lambda = new Lambda(types, returnType);
			node.setType(lambda);
			
			if(node.getParent() instanceof DeclarationNode) {
				IdentifierNode identifier = (IdentifierNode)node.getParent().child(0);
				boolean isMutable = false;
				if(node.getParent().getToken().isLextant(Keyword.VAR)) isMutable = true;
				addBinding(identifier, lambda, isMutable);
			}
		}
		enterParameterScope(node);
	}
	public void visitLeave(FunctionNode node) {
		leaveScope(node);
		
		
	}
	public void visitEnter(ProcedureBlockNode node) {
		enterProcedureScope(node);
	}
	public void visitLeave(ProcedureBlockNode node) {
		leaveScope(node);
	}

	
	
	///////////////////////////////////////////////////////////////////////////
	// helper methods for scoping.
	private void enterProgramScope(ParseNode node) {
		node.getScope().enter();
	}	
	private void enterSubscope(ParseNode node) {
		Scope baseScope = node.getLocalScope();
		Scope scope = baseScope.createSubscope();
		scope.enter();
		node.setScope(scope);
	}		
	private void enterParameterScope(ParseNode node) {
		Scope scope = Scope.createParameterScope();
		scope.enter();
		node.setScope(scope);
	}
	private void enterProcedureScope(ParseNode node) {
		Scope baseScope = node.getLocalScope();
		Scope scope = baseScope.createProcedureScope();
		scope.enter();
		node.setScope(scope);	
	}
	private void leaveScope(ParseNode node) {
		node.getScope().leave();
	}
	
	///////////////////////////////////////////////////////////////////////////
	// conditionals
	@Override
	public void visitLeave(ConditionalNode node) {
		if(node.child(0).getType() != PrimitiveType.BOOLEAN) {
			logError("If statements and while loops must have a boolean as their condition. At " + node.getToken().getLocation());
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// statements and declarations
	
	@Override
	public void visitLeave(PrintStatementNode node) {
	}
	@Override
	public void visitLeave(DeclarationNode node) {
		IdentifierNode identifier = (IdentifierNode) node.child(0);
		ParseNode initializer = node.child(1);
		
		Type declarationType = initializer.getType();
		node.setType(declarationType);
		
		if(node.child(1) instanceof FunctionNode) return;
		
		identifier.setType(declarationType);
		boolean isMutable = node.lextantToken().isLextant(Keyword.VAR);
		addBinding(identifier, declarationType, isMutable);
	}
	@Override
	public void visitLeave(AssignmentNode node) {

		//array element assignment
		if(node.child(0) instanceof FunctionInvocationNode) {
			logError("Function invocations are not a targetable type. At " + node.getToken().getLocation());
		}
		
		if(node.child(0).getType() instanceof Array || node.child(0) instanceof BinaryOperatorNode) {
			IdentifierNode identifier = null;
			if(node.child(0) instanceof BinaryOperatorNode) {	
				BinaryOperatorNode bin = (BinaryOperatorNode) node.child(0);
				if(bin.child(0).getType() == PrimitiveType.STRING) {
					logError("String indeces are not a targetable type. At " + node.getToken().getLocation());
					return;
				}
				while(bin.child(0) instanceof BinaryOperatorNode) {
					bin = (BinaryOperatorNode) bin.child(0);
				}	
				identifier = (IdentifierNode)bin.child(0);
			} else {
				identifier = (IdentifierNode) node.child(0);
			}
			ParseNode initializer = node.child(1);
			Type assignmentType = initializer.getType();
			Array arr = (Array) identifier.getType();
			
			while(arr.getSubtype() instanceof Array) {
				arr = (Array) arr.getSubtype();
			}
			
			if(assignmentType instanceof Array == false) {
				if(arr.getSubtype() != assignmentType) {
					if(assignmentType == PrimitiveType.CHAR) {
						if(arr.getSubtype() == PrimitiveType.INTEGER) {
							//promote to int
							BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(1));									
							node.replaceChild(node.child(1), b);			
							
						} else if(arr.getSubtype() == PrimitiveType.RATIONAL) {
							//promote to int, then to rational
							BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(1));
							BinaryOperatorNode b2 = createCastNode(PrimitiveType.RATIONAL, b);
							node.replaceChild(node.child(1), b2);
							assignmentType = PrimitiveType.RATIONAL;
							
						} else if(arr.getSubtype() == PrimitiveType.FLOATING) {
							//promote to int, then to floating
							BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(1));
							BinaryOperatorNode b2 = createCastNode(PrimitiveType.FLOATING, b);
							node.replaceChild(node.child(1), b2);
							assignmentType = PrimitiveType.FLOATING;
							
						} else {
							logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + identifier.getType() +
									") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());	
						}
					} else if(assignmentType == PrimitiveType.INTEGER) {
						if(arr.getSubtype() == PrimitiveType.RATIONAL) {
							BinaryOperatorNode b = createCastNode(PrimitiveType.RATIONAL, node.child(1));									
							node.replaceChild(node.child(1), b);
							assignmentType = PrimitiveType.RATIONAL;
						} else if(arr.getSubtype() == PrimitiveType.FLOATING) {
							BinaryOperatorNode b = createCastNode(PrimitiveType.FLOATING, node.child(1));									
							node.replaceChild(node.child(1), b);
							assignmentType = PrimitiveType.FLOATING;
						} else {
							logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + identifier.getType() +
									") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());	
						}
					} else {
						logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + identifier.getType() +
								") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());
					}									
				}
			} else {
				Array assignArray = (Array)assignmentType;
				while(assignArray.getSubtype() instanceof Array) {
					assignArray = (Array) assignArray.getSubtype();
				}
				if(arr.getSubtype() != assignArray.getSubtype()) {
					logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + arr.getSubtype() +
							") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());	
				}
			}

			node.setType(assignmentType);
		
		} else {
		//normal assignment
			IdentifierNode identifier = (IdentifierNode) node.child(0);
		
			Binding binding = identifier.findVariableBinding();
			if(!binding.getIsMutable()) {
				logError("Attempted to change value of constant identifier " + identifier.getToken().getLexeme() +
						" at " + node.getToken().getLocation());	
			}
				
			ParseNode initializer = node.child(1);
			
			if(identifier.getType() instanceof Lambda && initializer instanceof FunctionNode) {
				if(identifier.getType().equivalent(initializer.getType())) {
					node.setType(initializer.getType());
					return;
				} else {
					logError("Declared and assigned function signatures do not match. At" + node.getToken().getLocation());
					return;
				}
			}			
			//TextLocation loc = null;

			Type assignmentType = initializer.getType();
			if(identifier.getType() != assignmentType) {
				if(assignmentType == PrimitiveType.CHAR) {
					if(identifier.getType() == PrimitiveType.INTEGER) {
						//promote to int
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(1));									
						node.replaceChild(node.child(1), b);			
						
					} else if(identifier.getType() == PrimitiveType.RATIONAL) {
						//promote to int, then to rational
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(1));
						BinaryOperatorNode b2 = createCastNode(PrimitiveType.RATIONAL, b);
						node.replaceChild(node.child(1), b2);
						assignmentType = PrimitiveType.RATIONAL;
						
					} else if(identifier.getType() == PrimitiveType.FLOATING) {
						//promote to int, then to floating
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(1));
						BinaryOperatorNode b2 = createCastNode(PrimitiveType.FLOATING, b);
						node.replaceChild(node.child(1), b2);
						assignmentType = PrimitiveType.FLOATING;
						
					} else {
						logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + identifier.getType() +
								") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());	
					}
				} else if(assignmentType == PrimitiveType.INTEGER) {
					if(identifier.getType() == PrimitiveType.RATIONAL) {
						BinaryOperatorNode b = createCastNode(PrimitiveType.RATIONAL, node.child(1));									
						node.replaceChild(node.child(1), b);
						assignmentType = PrimitiveType.RATIONAL;
					} else if(identifier.getType() == PrimitiveType.FLOATING) {
						BinaryOperatorNode b = createCastNode(PrimitiveType.FLOATING, node.child(1));									
						node.replaceChild(node.child(1), b);
						assignmentType = PrimitiveType.FLOATING;
					} else {
						logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + identifier.getType() +
								") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());	
					}
				} else {
					logError("Type mismatch: identifier " + identifier.getToken().getLexeme() + " (" + identifier.getType() +
							") assigned " + initializer.getToken().getLexeme() + " (" + assignmentType + ") at " + node.getToken().getLocation());
				}
				
			}
			
			node.setType(assignmentType);
		}
	}
	
	private BinaryOperatorNode createCastNode(Type type, ParseNode node) {
		ParseNode castType = null;
		if(type == PrimitiveType.INTEGER || type == PrimitiveType.RATIONAL) {
			IntegerConstantToken t = IntegerConstantToken.make(null, "0");
			castType = new IntegerConstantNode(t);
			castType.setType(type);
		} else if(type == PrimitiveType.FLOATING) {
			FloatingConstantToken t = FloatingConstantToken.make(null, "0.0");
			castType = new FloatingConstantNode(t);
			castType.setType(type);
		} else {
			//error
		}
		LextantToken t2 = LextantToken.make(null, Punctuator.BAR.getLexeme(), Punctuator.BAR);
		BinaryOperatorNode b = BinaryOperatorNode.withChildren(t2, node, castType);
		visitLeave(b);	
		return b;
	}

	public void visitEnter(ForNode node) {	
		enterSubscope(node);
	}
	
	public void visitLeave(ForNode node) {
		ParseNode expressionNode = node.child(0);
		if(expressionNode.getType() != PrimitiveType.STRING && !(expressionNode.getType() instanceof Array)) {
			logError("Expression of for loop must be of array or string type. At " + node.getToken().getLocation());
		}
		leaveScope(node);
	}
	///////////////////////////////////////////////////////////////////////////
	// Functions
	public void visitLeave(ParameterListNode node) {
		for(ParseNode n : node.getChildren()) {
			IdentifierNode identifier = (IdentifierNode)n;
			addBinding(identifier, n.getType(), false);
		}
	}
	
	public void visitLeave(CallNode node) {
		if(!(node.child(0) instanceof FunctionInvocationNode)) {
			logError("Not a valid function invocation. At "+ node.getToken().getLocation());
		}
	}
	
	public void visitLeave(FunctionInvocationNode node) {
		IdentifierNode identifier = (IdentifierNode) node.child(0);
		Binding binding = identifier.findVariableBinding();
		if(binding.getType() instanceof Lambda) {
			Lambda otherLambda = (Lambda) binding.getType();
			if(otherLambda.getReturnType() == PrimitiveType.VOID) {
				if(!(node.getParent() instanceof CallNode)) {
					logError("Void functions can only be called using the \"call\" command. At "+ node.getToken().getLocation());
				}
			}
			ArrayList<Type> params = new ArrayList<Type>();
			
			//skip identifier node (child 0)
			for(int i = 1; i < node.nChildren(); i++) {
				params.add(node.child(i).getType());
			}
			Lambda lambda = new Lambda(params, otherLambda.getReturnType());
			if(lambda.equivalent(otherLambda)) {			
				node.setType(otherLambda.getReturnType());
			} else {
				logError("Argument mismatch. At " + node.getToken().getLocation());
			}
		} else {
			logError(node.child(0).toString() + "is not a declared function. At" + node.getToken().getLocation());
		}
	}
	
	public void visitLeave(ReturnNode node) {
		ParseNode invocation = node.getParent();
		while(!(invocation instanceof LambdaNode) && invocation != null) {
			invocation = invocation.getParent();
		}
		
		if(invocation instanceof LambdaNode) {
			//return.
			if(node.nChildren() == 0) {
				if(invocation.child(1).getType() != PrimitiveType.VOID) {
					logError("Return value mismatch. At "+ node.getToken().getLocation());
				}
				
			//return x.
			} else {
				if(!node.child(0).getType().equivalent(invocation.child(1).getType())) {
					logError("Return value mismatch. At " + node.getToken().getLocation());
				}
			}
		} else {
			logError("Return statement has no parent function. At " + node.getToken().getLocation());
		}
	}
	
	///////////////////////////////////////////////////////////////////////////
	// expressions
	@Override
	public void visitLeave(BinaryOperatorNode node) {
		assert node.nChildren() == 2;
		ParseNode left  = node.child(0);
		ParseNode right = node.child(1);
		List<Type> childTypes = Arrays.asList(left.getType(), right.getType());
		
		Lextant operator = operatorFor(node);
		FunctionSignatures signatures = FunctionSignatures.signaturesOf(operator);
		FunctionSignature signature = signatures.acceptingSignature(childTypes);
		if(signature.accepts(childTypes)) {
			node.setType(signature.resultType());
			node.setSignature(signature);
		} else {
			if(!promoteLeft(node, left, right)) {
				if(!promoteRight(node, right, left)) {
					if(!promoteBothOperands(node, left, right)) {
						typeCheckError(node, childTypes);
						node.setType(PrimitiveType.ERROR);
					}
				}
			}
		}
	}
	private Lextant operatorFor(BinaryOperatorNode node) {
		LextantToken token = (LextantToken) node.getToken();
		return token.getLextant();
	}
	
	@Override
	public void visitLeave(UnaryOperatorNode node) {
		assert node.nChildren() == 1;
		ParseNode right = node.child(0);
		List<Type> childTypes = Arrays.asList(right.getType());
		
		Lextant operator = operatorFor(node);
		FunctionSignatures signatures = 
				FunctionSignatures.signaturesOf(operator);
		FunctionSignature signature = 
				signatures.acceptingSignature(childTypes);
		if(signature.accepts(childTypes)) {
			node.setType(signature.resultType());
			node.setSignature(signature);
		}
		else {
			typeCheckError(node, childTypes);
			node.setType(PrimitiveType.ERROR);
		}
	}
	
	private Lextant operatorFor(UnaryOperatorNode node) {
		LextantToken token = (LextantToken) node.getToken();
		return token.getLextant();
	}
	
	@Override
	public void visitLeave(TrinaryOperatorNode node) {
		assert node.nChildren() == 3;
		ParseNode first = node.child(0);
		ParseNode second = node.child(1);
		ParseNode third = node.child(2);
		List<Type> childTypes = Arrays.asList(first.getType(), second.getType(), third.getType()); 
		
		Lextant operator = operatorFor(node);
		FunctionSignatures signatures = 
				FunctionSignatures.signaturesOf(operator);
		FunctionSignature signature = 
				signatures.acceptingSignature(childTypes);
		if(signature.accepts(childTypes)) {
			node.setType(signature.resultType());
			node.setSignature(signature);
		}
		else {
			typeCheckError(node, childTypes);
			node.setType(PrimitiveType.ERROR);
		}
	}
	
	private Lextant operatorFor(TrinaryOperatorNode node) {
		LextantToken token = (LextantToken) node.getToken();
		return token.getLextant();
	}

	
	@Override
	public void visitLeave(CreateEmptyArrayNode node) {
		
		node.setType(node.child(0).getType());
	}

	@Override
	public void visitLeave(ReleaseArrayNode node) {
		if(node.child(0).getType() instanceof Array) {
			node.setType(node.child(0).getType());
		}
		else {
			node.setType(PrimitiveType.ERROR);
			logError("Release expression must be of array type. At " + node.getToken().getLocation());
		}
		
	}
	
	@Override
	public void visitLeave(ExpressionListNode node) {
		
		if(node.getChildren().size() == 0) {
			node.setType(new Array(PrimitiveType.INTEGER)); //doesnt matter the type, no elements will be created
			return;
		}
		
		boolean containsChar = false;
		boolean containsInt = false;
		boolean containsRat = false;
		boolean containsFloat = false;
		boolean containsOther = false;
		for(ParseNode n : node.getChildren()) {
			if(n.getType() == PrimitiveType.CHAR) {
				containsChar = true;
			} else if(n.getType() == PrimitiveType.INTEGER) {
				containsInt = true;
			} else if(n.getType() == PrimitiveType.RATIONAL) {
				containsRat = true;
			} else if(n.getType() == PrimitiveType.FLOATING) {
				containsFloat = true;
			} else {
				containsOther = true;
			}
		}
		
		if(containsOther && (containsChar || containsInt || containsFloat || containsRat)) {
			logError("Array contains unpromotable types" + " at " + node.getToken().getLocation());
		}
		
		if(containsFloat && containsRat) {
			logError("Array contains both rationals and floating types" + " at " + node.getToken().getLocation());
		}
		
		if(containsRat) {
			for (int i = 0; i < node.nChildren(); i++) {
				if(node.child(i).getType() == PrimitiveType.CHAR) {
					BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(i));
					BinaryOperatorNode b2 = createCastNode(PrimitiveType.RATIONAL, b);
					node.replaceChild(node.child(i), b2);
				} else {
					BinaryOperatorNode b = createCastNode(PrimitiveType.RATIONAL, node.child(i));
					node.replaceChild(node.child(i), b);
				}
			}
			node.setType(new Array(PrimitiveType.RATIONAL));
				
		} else if(containsFloat) {
			for (int i = 0; i < node.nChildren(); i++) {
				if(node.child(i).getType() == PrimitiveType.CHAR) {
					BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(i));
					BinaryOperatorNode b2 = createCastNode(PrimitiveType.FLOATING, b);
					node.replaceChild(node.child(i), b2);
				} else {
					BinaryOperatorNode b = createCastNode(PrimitiveType.FLOATING, node.child(i));
					node.replaceChild(node.child(i), b);
				}
			}
			node.setType(new Array(PrimitiveType.FLOATING));
		} else if(containsInt) {
			for (int i = 0; i < node.nChildren(); i++) {
				BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, node.child(i));
				node.replaceChild(node.child(i), b);
			}
			node.setType(new Array(PrimitiveType.INTEGER));
		} else {
			node.setType(new Array(node.child(0).getType()));
		}	
	}
	
	
	///////////////////////////////////////////////////////////////////////////
	// higher order functions
	
	public void visitLeave(MapNode node) {
		if(!(node.child(0).getType() instanceof Array)) {
			logError("Argument 1 of map operation must be of Array type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!(node.child(1).getType() instanceof Lambda)) {
			logError("Argument 2 of map operation must be of Lambda type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		Array arr = (Array)node.child(0).getType();
		Type subtype = arr.getSubtype();
		Lambda lambda = (Lambda)node.child(1).getType();
		List<Type> params = lambda.getParamTypes();
		Type returnType = lambda.getReturnType();
		if(params.size() != 1) {
			logError("Lambda must have exactly one argument. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!params.get(0).equivalent(subtype)) {
			logError("Argument of lambda must match the subtype of the array. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		
		if(returnType == PrimitiveType.VOID) {
			logError("Return type of lambda must not be void. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		node.setType(new Array(returnType));
	}
	
	public void visitLeave(ReduceNode node) {
		if(!(node.child(0).getType() instanceof Array)) {
			logError("Argument 1 of reduce operation must be of Array type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!(node.child(1).getType() instanceof Lambda)) {
			logError("Argument 2 of reduce operation must be of Lambda type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		Array arr = (Array)node.child(0).getType();
		Type subtype = arr.getSubtype();
		Lambda lambda = (Lambda)node.child(1).getType();
		List<Type> params = lambda.getParamTypes();
		Type returnType = lambda.getReturnType();
		if(params.size() != 1) {
			logError("Lambda must have exactly one argument. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!params.get(0).equivalent(subtype)) {
			logError("Argument of lambda must match the subtype of the array. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		
		if(returnType != PrimitiveType.BOOLEAN) {
			logError("Return type of lambda must be boolean. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		node.setType(new Array(subtype));
	}
	
	public void visitLeave(ZipNode node) {
		if(!(node.child(0).getType() instanceof Array)) {
			logError("Argument 1 of zip operation must be of Array type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!(node.child(1).getType() instanceof Array)) {
			logError("Argument 2 of zip operation must be of Array type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!(node.child(2).getType() instanceof Lambda)) {
			logError("Argument 3 of zip operation must be of Lambda type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		Array arr = (Array)node.child(0).getType();
		Type subtype = arr.getSubtype();
		Array arr2 = (Array)node.child(1).getType();
		Type subtype2 = arr2.getSubtype();
		Lambda lambda = (Lambda)node.child(2).getType();
		List<Type> params = lambda.getParamTypes();
		Type returnType = lambda.getReturnType();
		
		if(params.size() != 2) {
			logError("Lambda must have exactly two arguments. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!params.get(0).equivalent(subtype) || !params.get(1).equivalent(subtype2)) {
			logError("Arguments of lambda must match the subtypes of the arrays. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(returnType == PrimitiveType.VOID) {
			logError("Return type of lambda must not be void. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		node.setType(new Array(returnType));
	}
	
	public void visitLeave(FoldNode node) {
		if(!(node.child(0).getType() instanceof Array)) {
			logError("Argument 1 of fold operation must be of Array type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		if(!(node.child(1).getType() instanceof Lambda)) {
			logError("Argument 2 of fold operation must be of Lambda type. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		
		Array arr = (Array)node.child(0).getType();
		Type subtype = arr.getSubtype();
		Lambda lambda = (Lambda)node.child(1).getType();
		List<Type> params = lambda.getParamTypes();
		Type returnType = lambda.getReturnType();
		
		if(params.size() != 2) {
			logError("Lambda must have exactly two arguments. At " + node.getToken().getLocation());
			node.setType(PrimitiveType.NO_TYPE);
			return;
		}
		
		//without base
		if(node.nChildren() == 2) {
			if(params.get(0) != params.get(1) || params.get(0) != subtype || params.get(0) != returnType) {
				logError("Lambda's arguments and return type must match that of the array's subtype. At " + node.getToken().getLocation());
				node.setType(PrimitiveType.NO_TYPE);
				return;
			}
		//with base
		} else {
			assert(node.nChildren() == 3);
			Type baseType = node.child(2).getType();
			if(params.get(0) != baseType || returnType != baseType) {
				logError("Lambda's argument 1 and return type must match the base type. At " + node.getToken().getLocation());
				node.setType(PrimitiveType.NO_TYPE);
				return;
			}
			if(params.get(1) != subtype) {
				logError("Lambda argument 2 must match the array's subtype. At " + node.getToken().getLocation());
				node.setType(PrimitiveType.NO_TYPE);
				return;
			}	
		}
		node.setType(returnType);
	}
	
	///////////////////////////////////////////////////////////////////////////
	// simple leaf nodes
	@Override
	public void visit(BooleanConstantNode node) {
		node.setType(PrimitiveType.BOOLEAN);
	}
	@Override
	public void visit(ErrorNode node) {
		node.setType(PrimitiveType.ERROR);
	}
	@Override
	public void visit(IntegerConstantNode node) {
		//to avoid creating a node for rational
		if(node.getType() != PrimitiveType.RATIONAL) { 
			node.setType(PrimitiveType.INTEGER);
		}
	}
	@Override
	public void visit(FloatingConstantNode node) {
		node.setType(PrimitiveType.FLOATING);
	}
	@Override
	public void visit(CharConstantNode node) {
		node.setType(PrimitiveType.CHAR);
	}
	
	@Override
	public void visit(StringConstantNode node) {
		node.setType(PrimitiveType.STRING);
	}
	
	@Override
	public void visit(ArrayNode node) {
		//type should already be set
	}
	
	@Override
	public void visit(NewlineNode node) {
	}
	@Override
	public void visit(TabNode node) {
	}
	@Override
	public void visit(SpaceNode node) {
	}
	@Override
	public void visit(JumpNode node) {
		ParseNode conditionalParent = node.getParent();
		while(true) {
			if(conditionalParent instanceof ConditionalNode) {
				if(conditionalParent.getToken().isLextant(Keyword.WHILE)) {
					node.setBreakLabel(((ConditionalNode) conditionalParent).getBreakLabel());
					node.setContinueLabel(((ConditionalNode) conditionalParent).getContinueLabel());
					break;
				} else {
					conditionalParent = conditionalParent.getParent();
				}
			} else if(conditionalParent instanceof ForNode) {
				node.setBreakLabel(((ForNode) conditionalParent).getEndLabel());
				node.setContinueLabel(((ForNode) conditionalParent).getContinueLabel());
				break;
			} else if(conditionalParent == null) {
				logError("break or continue statements must be contained within a while loop. At: " + node.getToken().getLocation());
				break;
			} else {
				conditionalParent = conditionalParent.getParent();
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// IdentifierNodes, with helper methods
	@Override
	public void visit(IdentifierNode node) {
		ParseNode parent = node.getParent();
		if(parent instanceof ForNode && parent.child(1) == node) {
			Type type = null;
			if(parent.getToken().isLextant(Keyword.INDEX)) {
				type = PrimitiveType.INTEGER;
			} else { //elem
				if(parent.child(0).getType() instanceof Array) {
					Array arr = (Array) parent.child(0).getType();
					type = arr.getSubtype();
				} else if(parent.child(0).getType() == PrimitiveType.STRING) {
					type = PrimitiveType.CHAR;
				}
			}
			node.setType(type);
			addBinding(node, type, false);
		}
		if(!isBeingDeclared(node)) {		
			Binding binding = node.findVariableBinding();
			
			node.setType(binding.getType());
			node.setBinding(binding);
		}
		// else parent DeclarationNode or ParameterListNode does the processing.
	}
	private boolean isBeingDeclared(IdentifierNode node) {
		ParseNode parent = node.getParent();
		return ((parent instanceof DeclarationNode) && (node == parent.child(0))) || (parent instanceof ParameterListNode);
	}
	private void addBinding(IdentifierNode identifierNode, Type type, boolean isMutable) {
		
		Scope scope = identifierNode.getLocalScope();
		Binding binding = scope.createBinding(identifierNode, type, isMutable);
		identifierNode.setBinding(binding);
		
	}
	
	///////////////////////////////////////////////////////////////////////////
	// error logging/printing

	private void typeCheckError(ParseNode node, List<Type> operandTypes) {
		Token token = node.getToken();
		
		logError("operator " + token.getLexeme() + " not defined for types " 
				 + operandTypes  + " at " + token.getLocation());	
	}
	private void logError(String message) {
		PikaLogger log = PikaLogger.getLogger("compiler.semanticAnalyzer");
		log.severe(message);
	}
	
	///////////////////////////////////////////////////////////////////////////
	// promotion logic
	public boolean promoteLeft(BinaryOperatorNode node, ParseNode op1, ParseNode op2) {
		List<Type> childTypes = Arrays.asList(op2.getType(), op2.getType());
		
		Lextant operator = operatorFor(node);
		FunctionSignatures signatures = FunctionSignatures.signaturesOf(operator);
		FunctionSignature signature = signatures.acceptingSignature(childTypes);
		
		if(op1.getType() == PrimitiveType.CHAR) {
			//try promotion to int
			childTypes = Arrays.asList(PrimitiveType.INTEGER, op2.getType());
			signature = signatures.acceptingSignature(childTypes);
			if(signature.accepts(childTypes)) {
				BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, op1);
				node.replaceChild(op1, b);
				node.setType(signature.resultType());
				node.setSignature(signature);
				return true;
			}
			else {
				//try promotion to float
				childTypes = Arrays.asList(PrimitiveType.FLOATING, op2.getType());
				signature = signatures.acceptingSignature(childTypes);
				if(signature.accepts(childTypes)) {
					//error if it also accepts a rational
					childTypes = Arrays.asList(PrimitiveType.RATIONAL, op2.getType());
					FunctionSignature ratSig = signatures.acceptingSignature(childTypes);
					if(ratSig.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					} else {
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, op1);
						BinaryOperatorNode b2 = createCastNode(PrimitiveType.FLOATING, b);
						node.replaceChild(op1, b2);						
						node.setType(signature.resultType());
						node.setSignature(signature);
						return true;
					}
				} else {
					//try promotion to rat
					childTypes = Arrays.asList(PrimitiveType.RATIONAL, op2.getType());
					signature = signatures.acceptingSignature(childTypes);
					if(signature.accepts(childTypes)) { 
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, op1);
						BinaryOperatorNode b2 = createCastNode(PrimitiveType.RATIONAL, b);
						node.replaceChild(op1, b2);						
						node.setType(signature.resultType());
						node.setSignature(signature);
						return true;
					}
				}
			}
		} else if(op1.getType() == PrimitiveType.INTEGER) {
			childTypes = Arrays.asList(PrimitiveType.FLOATING, op2.getType());
			signature = signatures.acceptingSignature(childTypes);
			if(signature.accepts(childTypes)) {
				//error if it also accepts a rational
				childTypes = Arrays.asList(PrimitiveType.RATIONAL, op2.getType());
				FunctionSignature ratSig = signatures.acceptingSignature(childTypes);
				if(ratSig.accepts(childTypes)) {
					logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
					node.setType(PrimitiveType.ERROR);
					return true;
				} else {
					BinaryOperatorNode b = createCastNode(PrimitiveType.FLOATING, op1);
					node.replaceChild(op1, b);
					node.setType(signature.resultType());
					node.setSignature(signature);
					return true;
				}
			} else {
				//try promotion to rat
				childTypes = Arrays.asList(PrimitiveType.RATIONAL, op2.getType());
				signature = signatures.acceptingSignature(childTypes);
				if(signature.accepts(childTypes)) { 
					BinaryOperatorNode b = createCastNode(PrimitiveType.RATIONAL, op1);
					node.replaceChild(op1, b);
					node.setType(signature.resultType());
					node.setSignature(signature);
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean promoteRight(BinaryOperatorNode node, ParseNode op1, ParseNode op2) {
		List<Type> childTypes = Arrays.asList(op2.getType(), op2.getType());
		
		Lextant operator = operatorFor(node);
		FunctionSignatures signatures = FunctionSignatures.signaturesOf(operator);
		FunctionSignature signature = signatures.acceptingSignature(childTypes);
		
		if(op1.getType() == PrimitiveType.CHAR) {
			//try promotion to int
			childTypes = Arrays.asList(op2.getType(), PrimitiveType.INTEGER);
			signature = signatures.acceptingSignature(childTypes);
			if(signature.accepts(childTypes)) {
				BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, op1);
				node.replaceChild(op1, b);
				node.setType(signature.resultType());
				node.setSignature(signature);
				return true;
			}
			else {
				//try promotion to float
				childTypes = Arrays.asList(op2.getType(), PrimitiveType.FLOATING);
				signature = signatures.acceptingSignature(childTypes);
				if(signature.accepts(childTypes)) {
					//error if it also accepts a rational
					childTypes = Arrays.asList(op2.getType(), PrimitiveType.RATIONAL);
					FunctionSignature ratSig = signatures.acceptingSignature(childTypes);
					if(ratSig.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					} else {
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, op1);
						BinaryOperatorNode b2 = createCastNode(PrimitiveType.FLOATING, b);
						node.replaceChild(op1, b2);						
						node.setType(signature.resultType());
						node.setSignature(signature);
						return true;
					}
				} else {
					//try promotion to rat
					childTypes = Arrays.asList(op2.getType(), PrimitiveType.RATIONAL);
					signature = signatures.acceptingSignature(childTypes);
					if(signature.accepts(childTypes)) { 
						BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, op1);
						BinaryOperatorNode b2 = createCastNode(PrimitiveType.RATIONAL, b);
						node.replaceChild(op1, b2);						
						node.setType(signature.resultType());
						node.setSignature(signature);
						return true;
					}
				}
			}
		} else if(op1.getType() == PrimitiveType.INTEGER) {
			childTypes = Arrays.asList(op2.getType(), PrimitiveType.FLOATING);
			signature = signatures.acceptingSignature(childTypes);
			if(signature.accepts(childTypes)) {
				//error if it also accepts a rational
				childTypes = Arrays.asList(op2.getType(), PrimitiveType.RATIONAL);
				FunctionSignature ratSig = signatures.acceptingSignature(childTypes);
				if(ratSig.accepts(childTypes)) {
					logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
					node.setType(PrimitiveType.ERROR);
					return true;
				} else {
					BinaryOperatorNode b = createCastNode(PrimitiveType.FLOATING, op1);
					node.replaceChild(op1, b);
					node.setType(signature.resultType());
					node.setSignature(signature);
					return true;
				}
			} else {
				//try promotion to rat
				childTypes = Arrays.asList(op2.getType(), PrimitiveType.RATIONAL);
				signature = signatures.acceptingSignature(childTypes);
				if(signature.accepts(childTypes)) { 
					BinaryOperatorNode b = createCastNode(PrimitiveType.RATIONAL, op1);
					node.replaceChild(op1, b);
					node.setType(signature.resultType());
					node.setSignature(signature);
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean promoteBothOperands(BinaryOperatorNode node, ParseNode op1, ParseNode op2) {
		//first promote each operator
		if(!promote(node, op1) || !promote(node, op2)) {
			return false;
		}
		ParseNode newOp1 = node.child(0);
		ParseNode newOp2 = node.child(1);
		
		//operators got promoted to the same type
		if(newOp1.getType() == newOp2.getType()) {
			List<Type> childTypes = Arrays.asList(newOp1.getType(), newOp2.getType());
			
			Lextant operator = operatorFor(node);
			FunctionSignatures signatures = FunctionSignatures.signaturesOf(operator);
			FunctionSignature signature = signatures.acceptingSignature(childTypes);
			if(signature.accepts(childTypes)) {
				//ambiguous if (rat rat) (rat float) or (float rat) passes as well
				if(newOp1.getType() == PrimitiveType.FLOATING) {
					childTypes = Arrays.asList(PrimitiveType.RATIONAL, PrimitiveType.RATIONAL);
					FunctionSignature ratSignature = signatures.acceptingSignature(childTypes);
					if(ratSignature.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					}
					
					childTypes = Arrays.asList(PrimitiveType.FLOATING, PrimitiveType.RATIONAL);
					ratSignature = signatures.acceptingSignature(childTypes);
					if(ratSignature.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					}
					
					childTypes = Arrays.asList(PrimitiveType.RATIONAL, PrimitiveType.FLOATING);
					ratSignature = signatures.acceptingSignature(childTypes);
					if(ratSignature.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					}
				}
				
				node.setType(signature.resultType());
				node.setSignature(signature);
				return true;
			}
			
			if(promoteLeft(node, newOp1, newOp2)) {
				//if both are valid then it is ambiguous
				if(promoteRight(node, newOp2, newOp1)) {
					logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
					node.setType(PrimitiveType.ERROR);
				}
				return true;
			} else if(promoteRight(node, newOp2, newOp1)) {
				return true;
			} else {
				return false;
			}
			
		//operators got promoted to different types	
		} else {
			List<Type> childTypes = Arrays.asList(newOp1.getType(), newOp2.getType());
			Lextant operator = operatorFor(node);
			FunctionSignatures signatures = FunctionSignatures.signaturesOf(operator);
			FunctionSignature signature = signatures.acceptingSignature(childTypes);
			if(signature.accepts(childTypes)) {
				if(newOp1.getType() == PrimitiveType.INTEGER) {
					//ambiguous if (int rat) also matches
					childTypes = Arrays.asList(newOp1.getType(), PrimitiveType.RATIONAL);
					FunctionSignature ratSignature = signatures.acceptingSignature(childTypes);
					if(ratSignature.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					}
					node.setType(signature.resultType());
					node.setSignature(signature);
				} else {
					//ambiguous if (rat int) also matches
					childTypes = Arrays.asList(PrimitiveType.RATIONAL, newOp2.getType());
					FunctionSignature ratSignature = signatures.acceptingSignature(childTypes);
					if(ratSignature.accepts(childTypes)) {
						logError("Ambiguous implicit promotion at " + node.getToken().getLocation());
						node.setType(PrimitiveType.ERROR);
						return true;
					}
					node.setType(signature.resultType());
					node.setSignature(signature);
				}
			} else {
				if(newOp1.getType() == PrimitiveType.INTEGER) { 
					if(promoteLeft(node, newOp1, newOp2)) {
						return true;
					}
					return false;
				} else {
					if(promoteRight(node, newOp2, newOp1)) {
						return true;
					}
					return false;
				}
			}
		}
		
		return false;
	}
	
	public boolean promote(BinaryOperatorNode node, ParseNode subNode) {
		if(subNode.getType() == PrimitiveType.CHAR) {
			BinaryOperatorNode b = createCastNode(PrimitiveType.INTEGER, subNode);
			b.setType(PrimitiveType.INTEGER);
			node.replaceChild(subNode, b);
			return true;
		} else if(subNode.getType() == PrimitiveType.INTEGER) {
			BinaryOperatorNode b = createCastNode(PrimitiveType.FLOATING, subNode);
			b.setType(PrimitiveType.FLOATING);
			node.replaceChild(subNode, b);
			return true;
		} 
		//cannot promote, must fail
		return false;
	}



}