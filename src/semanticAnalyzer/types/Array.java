package semanticAnalyzer.types;

public class Array implements Type {
	private Type subtype;
	
	public Array(Type subtype) {
		this.subtype = subtype;
	}
	
	public Type getSubtype() {
		return subtype;
	}
	
	private void setSubtype(Type subType) {
		this.subtype = subType;
	}

	public int getSize() {
		return 4;
	}

	public String infoString() {
		return null;
	}

	public boolean equivalent(Type otherType) {
		if(otherType instanceof Array) {
			Array otherArray = (Array)otherType;
			return subtype.equivalent(otherArray.getSubtype());
		}
		return false;
	}

	public Type getConcreteType() {
		Type concreteSubtype = subtype.getConcreteType();
		return new Array(concreteSubtype);
	}
	
}
