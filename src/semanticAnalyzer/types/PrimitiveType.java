package semanticAnalyzer.types;


public enum PrimitiveType implements Type {
	BOOLEAN(1),
	CHAR(1),
	INTEGER(4),
	STRING(4),
	FLOATING(8),
	RATIONAL(8),
	STRING_LITERAL(4),  // use for printing strings directly
	VOID(0),			// not an actual type, used when function does not return a value
	ERROR(0),			// use as a value when a syntax error has occurred
	NO_TYPE(0, "");		// use as a value when no type has been assigned.
	
	private int sizeInBytes;
	private String infoString;
	
	private PrimitiveType(int size) {
		this.sizeInBytes = size;
		this.infoString = toString();
	}
	private PrimitiveType(int size, String infoString) {
		this.sizeInBytes = size;
		this.infoString = infoString;
	}
	public int getSize() {
		return sizeInBytes;
	}
	public String infoString() {
		return infoString;
	}
	public boolean equivalent(Type type) {
		return this == type;
	}

	public Type getConcreteType() {
		return this;
	}
}
