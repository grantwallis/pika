package semanticAnalyzer.types;

import java.util.ArrayList;


public class Lambda implements Type {

	private ArrayList<Type> paramTypes;
	private Type returnType;
	
	public Lambda(ArrayList<Type> paramTypes, Type returnType) {
		this.paramTypes = paramTypes;
		this.returnType = returnType;
	}
	
//	public Lambda(ArrayList<Type> paramTypes) {
//		this.paramTypes = paramTypes;
//		this.returnType = PrimitiveType.VOID;
//	}

	public ArrayList<Type> getParamTypes() {
		return paramTypes;
	}
	
	public Type getReturnType() {
		return returnType;
	}

	public int getSize() {
		return 8;
	}

	public String infoString() {
		return null;
	}

	public boolean equivalent(Type otherType) {
		if(otherType instanceof Lambda) {
			Lambda lambda = (Lambda) otherType;
			if(paramTypes.size() != lambda.getParamTypes().size()) {
				return false;
			}
			for(int i = 0; i < paramTypes.size(); i++) {
				if(!paramTypes.get(i).equivalent(lambda.getParamTypes().get(i))) {
					return false;
				}
			}
			if(!(returnType.equivalent(lambda.getReturnType()))) {
				return false;
			}
			return true;
		}
		return false;
	}

	public Type getConcreteType() {
		return this;
	}
//	
}
