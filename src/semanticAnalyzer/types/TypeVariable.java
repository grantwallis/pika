package semanticAnalyzer.types;

public class TypeVariable implements Type {
	private String name; //debugging
	private Type typeConstraint; //type variable is set to

	public TypeVariable(String name) {
		this.name = name;
		this.typeConstraint = PrimitiveType.NO_TYPE;
	}
	
	public Type getType() {
		return typeConstraint;
	}
	
	public String getName() {
		return name;
	}
	
	private void setType(Type type) {
		typeConstraint = type;
	}
	
	public int getSize() {
		return 0;
	}

	public String infoString() {
		return toString();
	}
	
	public String toString() {
		return "<" + getName() + ">";
	}
	
	public void reset() {
		typeConstraint = PrimitiveType.NO_TYPE;
	}

	public boolean equivalent(Type otherType) {
		if(otherType instanceof TypeVariable) {
			throw new RuntimeException("equals attempted on two types containing type variables.");
		}
		if(this.getType() == PrimitiveType.NO_TYPE) {
			setType(otherType);
			return true;
		}
		return this.getType().equivalent(otherType);
	}

	public Type getConcreteType() {
		return getType().getConcreteType();
	}
	
}