package semanticAnalyzer.signatures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import asmCodeGenerator.ArrayCloneCodeGenerator;
import asmCodeGenerator.ArrayIndexingCodeGenerator;
import asmCodeGenerator.ArrayLengthCodeGenerator;
import asmCodeGenerator.ArrayReverseCodeGenerator;
import asmCodeGenerator.CastCodeGenerator;
import asmCodeGenerator.CharStringConcatCodeGenerator;
import asmCodeGenerator.DivideCodeGenerator;
import asmCodeGenerator.FloatingExpressOverCodeGenerator;
import asmCodeGenerator.FloatingRationalizeCodeGenerator;
import asmCodeGenerator.RationalAddCodeGenerator;
import asmCodeGenerator.RationalCodeGenerator;
import asmCodeGenerator.RationalDivideCodeGenerator;
import asmCodeGenerator.RationalExpressOverCodeGenerator;
import asmCodeGenerator.RationalMultiplyCodeGenerator;
import asmCodeGenerator.RationalRationalizeCodeGenerator;
import asmCodeGenerator.RationalSubtractCodeGenerator;
import asmCodeGenerator.ShortCircuitAndCodeGenerator;
import asmCodeGenerator.ShortCircuitOrCodeGenerator;
import asmCodeGenerator.StringCharConcatCodeGenerator;
import asmCodeGenerator.StringIndexingCodeGenerator;
import asmCodeGenerator.StringLengthCodeGenerator;
import asmCodeGenerator.StringRangeIndexingCodeGenerator;
import asmCodeGenerator.StringReverseCodeGenerator;
import asmCodeGenerator.StringStringConcatCodeGenerator;
import asmCodeGenerator.codeStorage.ASMOpcode;
import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Punctuator;
import semanticAnalyzer.types.Array;
import semanticAnalyzer.types.Type;
import semanticAnalyzer.types.TypeVariable;

import static semanticAnalyzer.types.PrimitiveType.*;

public class FunctionSignatures extends ArrayList<FunctionSignature> {
	private static final long serialVersionUID = -4907792488209670697L;
	private static Map<Object, FunctionSignatures> signaturesForKey = new HashMap<Object, FunctionSignatures>();
	
	Object key;
	
	public FunctionSignatures(Object key, FunctionSignature ...functionSignatures) {
		this.key = key;
		for(FunctionSignature functionSignature: functionSignatures) {
			add(functionSignature);
		}
		signaturesForKey.put(key, this);
	}
	
	public Object getKey() {
		return key;
	}
	public boolean hasKey(Object key) {
		return this.key.equals(key);
	}
	
	public FunctionSignature acceptingSignature(List<Type> types) {
		for(FunctionSignature functionSignature: this) {
			if(functionSignature.accepts(types)) {
				return functionSignature;
			}
		}
		return FunctionSignature.nullInstance();
	}
	public boolean accepts(List<Type> types) {
		return !acceptingSignature(types).isNull();
	}

	
	/////////////////////////////////////////////////////////////////////////////////
	// access to FunctionSignatures by key object.
	
	public static FunctionSignatures nullSignatures = new FunctionSignatures(0, FunctionSignature.nullInstance());

	public static FunctionSignatures signaturesOf(Object key) {
		if(signaturesForKey.containsKey(key)) {
			return signaturesForKey.get(key);
		}
		return nullSignatures;
	}
	public static FunctionSignature signature(Object key, List<Type> types) {
		FunctionSignatures signatures = FunctionSignatures.signaturesOf(key);
		return signatures.acceptingSignature(types);
	}

	
	
	/////////////////////////////////////////////////////////////////////////////////
	// Put the signatures for operators in the following static block.
	
	static {
		// here's one example to get you started with FunctionSignatures: the signatures for addition.		
		// for this to work, you should statically import PrimitiveType.*
		
		TypeVariable S = new TypeVariable("S");
		List<TypeVariable> setS = Arrays.asList(S);

		new FunctionSignatures(Punctuator.ADD,
		    new FunctionSignature(ASMOpcode.Add, INTEGER, INTEGER, INTEGER),
		    new FunctionSignature(ASMOpcode.FAdd, FLOATING, FLOATING, FLOATING),
		    new FunctionSignature(new RationalAddCodeGenerator(), RATIONAL, RATIONAL, RATIONAL),
		    
		    new FunctionSignature(new StringStringConcatCodeGenerator(), STRING, STRING, STRING),
		    new FunctionSignature(new StringCharConcatCodeGenerator(), STRING, CHAR, STRING),
		    new FunctionSignature(new CharStringConcatCodeGenerator(), CHAR, STRING, STRING)
		);
		new FunctionSignatures(Punctuator.SUBTRACT,
			new FunctionSignature(ASMOpcode.Subtract, INTEGER, INTEGER, INTEGER),
			new FunctionSignature(ASMOpcode.FSubtract, FLOATING, FLOATING, FLOATING),
			new FunctionSignature(new RationalSubtractCodeGenerator(), RATIONAL, RATIONAL, RATIONAL)
		);
		new FunctionSignatures(Punctuator.MULTIPLY,
			new FunctionSignature(ASMOpcode.Multiply, INTEGER, INTEGER, INTEGER),
			new FunctionSignature(ASMOpcode.FMultiply, FLOATING, FLOATING, FLOATING),
			new FunctionSignature(new RationalMultiplyCodeGenerator(), RATIONAL, RATIONAL, RATIONAL)
		);
		new FunctionSignatures(Punctuator.DIVIDE,
				new FunctionSignature(new DivideCodeGenerator(), INTEGER, INTEGER, INTEGER),
				new FunctionSignature(new DivideCodeGenerator(), FLOATING, FLOATING, FLOATING),
				new FunctionSignature(new RationalDivideCodeGenerator(), RATIONAL, RATIONAL, RATIONAL)
		);
		new FunctionSignatures(Punctuator.OVER,
				new FunctionSignature(new RationalCodeGenerator(), INTEGER, INTEGER, RATIONAL)
		);
		new FunctionSignatures(Punctuator.EXPRESS_OVER,
				new FunctionSignature(new RationalExpressOverCodeGenerator(), RATIONAL, INTEGER, INTEGER),
				new FunctionSignature(new FloatingExpressOverCodeGenerator(), FLOATING, INTEGER, INTEGER)
		);
		
		new FunctionSignatures(Punctuator.RATIONALIZE, 
				new FunctionSignature(new RationalRationalizeCodeGenerator(), RATIONAL, INTEGER, RATIONAL),
				new FunctionSignature(new FloatingRationalizeCodeGenerator(), FLOATING, INTEGER, RATIONAL)
		);
		
		//Cast signatures
		new FunctionSignatures(Punctuator.BAR,
				new FunctionSignature(new CastCodeGenerator(), FLOATING, FLOATING, FLOATING),
				new FunctionSignature(new CastCodeGenerator(), FLOATING, INTEGER, INTEGER),
				new FunctionSignature(new CastCodeGenerator(), FLOATING, RATIONAL, RATIONAL),
				
				new FunctionSignature(new CastCodeGenerator(), INTEGER, INTEGER, INTEGER),
				new FunctionSignature(new CastCodeGenerator(), INTEGER, FLOATING, FLOATING),
				new FunctionSignature(new CastCodeGenerator(), INTEGER, BOOLEAN, BOOLEAN),
				new FunctionSignature(new CastCodeGenerator(), INTEGER, CHAR, CHAR),
				new FunctionSignature(new CastCodeGenerator(), INTEGER, RATIONAL, RATIONAL),
				
				new FunctionSignature(new CastCodeGenerator(), CHAR, CHAR, CHAR),
				new FunctionSignature(new CastCodeGenerator(), CHAR, INTEGER, INTEGER),
				new FunctionSignature(new CastCodeGenerator(), CHAR, BOOLEAN, BOOLEAN),
				new FunctionSignature(new CastCodeGenerator(), CHAR, RATIONAL, RATIONAL),
				
				new FunctionSignature(new CastCodeGenerator(), BOOLEAN, BOOLEAN, BOOLEAN),
				
				new FunctionSignature(new CastCodeGenerator(), STRING, STRING, STRING),
				
				new FunctionSignature(new CastCodeGenerator(), RATIONAL, RATIONAL, RATIONAL),
				new FunctionSignature(new CastCodeGenerator(), RATIONAL, INTEGER, INTEGER),
				new FunctionSignature(new CastCodeGenerator(), RATIONAL, FLOATING, FLOATING),
				
				new FunctionSignature(new CastCodeGenerator(), new Array(S), new Array(S), new Array(S))
				
		);
		
		new FunctionSignatures(Punctuator.OR, 
				new FunctionSignature(new ShortCircuitOrCodeGenerator(), BOOLEAN, BOOLEAN, BOOLEAN)	
		);
		
		new FunctionSignatures(Punctuator.AND, 
				new FunctionSignature(new ShortCircuitAndCodeGenerator(), BOOLEAN, BOOLEAN, BOOLEAN)	
		);
		
		new FunctionSignatures(Punctuator.NOT, 
				new FunctionSignature(ASMOpcode.BNegate, BOOLEAN, BOOLEAN)
		);
		
		
		new FunctionSignatures(Keyword.LENGTH,
				new FunctionSignature(new ArrayLengthCodeGenerator(), setS, new Array(S), INTEGER),
				new FunctionSignature(new StringLengthCodeGenerator(), STRING, INTEGER)
		);
		
		new FunctionSignatures(Keyword.CLONE,
				new FunctionSignature(new ArrayCloneCodeGenerator(), setS, new Array(S), new Array(S))
		);
		
		new FunctionSignatures(Keyword.REVERSE,
				new FunctionSignature(new StringReverseCodeGenerator(), STRING, STRING),
				new FunctionSignature(new ArrayReverseCodeGenerator(), setS, new Array(S), new Array(S))
		);
		
		
		
		new FunctionSignatures(Punctuator.ARRAY_INDEXING, 
				new FunctionSignature(new ArrayIndexingCodeGenerator(), setS, new Array(S), INTEGER, S),
				new FunctionSignature(new StringIndexingCodeGenerator(), STRING, INTEGER, CHAR),
				new FunctionSignature(new StringRangeIndexingCodeGenerator(), STRING, INTEGER, INTEGER, STRING)
		);
		
		Punctuator []comparisons = { Punctuator.GREATER, Punctuator.LESS, Punctuator.GREATER_EQ, Punctuator.LESS_EQ,
									 Punctuator.EQUAL, Punctuator.NOT_EQUAL};
		for(Punctuator comparison: comparisons) {
			FunctionSignature iSignature = new FunctionSignature(1,
					INTEGER, INTEGER, BOOLEAN);
			FunctionSignature cSignature = new FunctionSignature(1,
					CHAR, CHAR, BOOLEAN);
			FunctionSignature fSignature = new FunctionSignature(1,
					FLOATING, FLOATING, BOOLEAN);
			FunctionSignature bSignature = new FunctionSignature(1,
					BOOLEAN, BOOLEAN, BOOLEAN);
			FunctionSignature sSignature = new FunctionSignature(1,
					STRING, STRING, BOOLEAN);
			FunctionSignature rSignature = new FunctionSignature(1,
					RATIONAL, RATIONAL, BOOLEAN);
			FunctionSignature aSignature = new FunctionSignature(1,
					new Array(S), new Array(S), BOOLEAN);
			
			if(comparison == Punctuator.EQUAL || comparison == Punctuator.NOT_EQUAL) {
				new FunctionSignatures(comparison, iSignature, cSignature, fSignature, rSignature, bSignature, sSignature, aSignature);
			} else {
				new FunctionSignatures(comparison, iSignature, cSignature, fSignature, rSignature);
			}
		}

		
		// First, we use the operator itself (in this case the Punctuator ADD) as the key.
		// Then, we give that key two signatures: one an (INT x INT -> INT) and the other
		// a (FLOAT x FLOAT -> FLOAT).  Each signature has a "whichVariant" parameter where
		// I'm placing the instruction (ASMOpcode) that needs to be executed.
		//
		// I'll follow the convention that if a signature has an ASMOpcode for its whichVariant,
		// then to generate code for the operation, one only needs to generate the code for
		// the operands (in order) and then add to that the Opcode.  For instance, the code for
		// floating addition should look like:
		//
		//		(generate argument 1)	: may be many instructions
		//		(generate argument 2)   : ditto
		//		FAdd					: just one instruction
		//
		// If the code that an operator should generate is more complicated than this, then
		// I will not use an ASMOpcode for the whichVariant.  In these cases I typically use
		// a small object with one method (the "Command" design pattern) that generates the
		// required code.

	}

}
