package semanticAnalyzer;

import java.util.ArrayList;

import parseTree.ParseNode;
import parseTree.ParseNodeVisitor;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.FunctionNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.LambdaNode;
import parseTree.nodeTypes.ProgramNode;
import semanticAnalyzer.types.Lambda;
import semanticAnalyzer.types.Type;
import symbolTable.Binding;
import symbolTable.Scope;

class FunctionDeclarationVisitor extends ParseNodeVisitor.Default {
	@Override
	public void visitEnter(ProgramNode node) {
		createProgramScope(node);
		node = (ProgramNode) moveGlobals(node);
	}
	
	public void visitEnter(FunctionNode node) {
		addToSymbolTable(node);
	}
	
	private void createProgramScope(ParseNode node) {
		Scope scope = Scope.createProgramScope();
		node.setScope(scope);
	}
	
	private ParseNode moveGlobals(ProgramNode node) {
		ArrayList<ParseNode> nodes = new ArrayList<ParseNode>();
		for(ParseNode n : node.getChildren()) {
			if(n instanceof DeclarationNode) {
				nodes.add(n);
			}
		}
		for(ParseNode n : node.getChildren()) {
			if(!(n instanceof DeclarationNode)) {
				nodes.add(n);
			}
		}
		
		node.removeChildren();
		for(ParseNode n : nodes) {
			node.appendChild(n);
		}
		
		return node;
	}
	
	private void addToSymbolTable(FunctionNode node) {
		if(!(node.child(0) instanceof LambdaNode)) {
			Scope scope = node.getParent().getScope();
			IdentifierNode identifier = (IdentifierNode) node.child(0);
			LambdaNode lambdaNode = (LambdaNode) node.child(1);
			ArrayList<Type> paramTypes = new ArrayList<Type>();
			for(int i = 0; i < lambdaNode.child(0).nChildren(); i++) {
				Type type = lambdaNode.child(0).child(i).getType();
				paramTypes.add(type);
			}
			Type returnType = lambdaNode.child(1).getType();
			Lambda lambda = new Lambda(paramTypes, returnType);
			
			Binding binding = scope.createBinding(identifier, lambda, true);
			identifier.setBinding(binding);
		}
	}
	
	public void visitEnter(LambdaNode node) {
		node.setType(node.child(1).getType());
	}
}