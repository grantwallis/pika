package tokens;

import inputHandler.TextLocation;

public class CharConstantToken extends TokenImp {
	protected char value;
	
	protected CharConstantToken(TextLocation location, String lexeme) {
		super(location, lexeme);
	}
	protected void setValue(char value) {
		this.value = value;
	}
	public char getValue() {
		return value;
	}
	
	public static CharConstantToken make(TextLocation location, String lexeme) {
		CharConstantToken result = new CharConstantToken(location, lexeme);
		result.setValue(lexeme.charAt(0));
		return result;
	}
	
	@Override
	protected String rawString() {
		return "charConst, " + value;
	}
}
