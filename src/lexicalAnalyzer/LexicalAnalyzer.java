package lexicalAnalyzer;


import logging.PikaLogger;

import inputHandler.InputHandler;
import inputHandler.LocatedChar;
import inputHandler.LocatedCharStream;
import inputHandler.PushbackCharStream;
import inputHandler.TextLocation;
import tokens.CharConstantToken;
import tokens.FloatingConstantToken;
import tokens.IdentifierToken;
import tokens.LextantToken;
import tokens.NullToken;
import tokens.StringConstantToken;
import tokens.IntegerConstantToken;
import tokens.Token;

import static lexicalAnalyzer.PunctuatorScanningAids.*;

public class LexicalAnalyzer extends ScannerImp implements Scanner {
	public static LexicalAnalyzer make(String filename) {
		InputHandler handler = InputHandler.fromFilename(filename);
		PushbackCharStream charStream = PushbackCharStream.make(handler);
		return new LexicalAnalyzer(charStream);
	}

	public LexicalAnalyzer(PushbackCharStream input) {
		super(input);
	}

	
	//////////////////////////////////////////////////////////////////////////////
	// Token-finding main dispatch	

	@Override
	protected Token findNextToken() {
		LocatedChar ch = nextValidChar();
		
		if(ch.isDigit() || (ch.isDecimal() && input.peek().isDigit())) {
			return scanNumber(ch);
		}
		else if(ch.isSign()) {
			//check to see if punctuator or sign
			LocatedChar temp = input.peek();
			if (temp.isDigit() || temp.isDecimal()) {
				return scanNumber(ch);
			}	
			return PunctuatorScanner.scan(ch, input);
		}
		else if(ch.isIdentifierStart()) {
			return scanIdentifier(ch);
		}
		else if(ch.isStringStart()) {
			return scanString();
		}
		else if(ch.isCarot()) {
			return scanChar();
		}
		else if(isPunctuatorStart(ch)) {
			return PunctuatorScanner.scan(ch, input);
		}
		else if(isEndOfInput(ch)) {
			return NullToken.make(ch.getLocation());
		}
		else {
			lexicalError(ch);
			return findNextToken();
		}
	}

	//recursively checks until non-whitespace, non-comment char
	private LocatedChar nextValidChar() {
		LocatedChar ch = input.next();
		while(ch.isWhitespace()) {
			ch = input.next();
		}
		if(ch.isComment()) {
			ch = input.next();
			while(!ch.isCommentEnd()) {
				ch = input.next();
			}
			ch = nextValidChar();
		}
		return ch;
	}
	
	
	//////////////////////////////////////////////////////////////////////////////
	// Number lexical analysis	

	private Token scanNumber(LocatedChar firstChar) {
		StringBuffer buffer = new StringBuffer();
		if(firstChar.isSign()) {
			buffer.append(firstChar.getCharacter());
			firstChar = input.next();
		}
		boolean isInt = appendSubsequentDigits(buffer, firstChar);
		
		if(isInt) {
			return IntegerConstantToken.make(firstChar.getLocation(), buffer.toString());
		} else {
			return FloatingConstantToken.make(firstChar.getLocation(), buffer.toString());
		}
	}
	private boolean appendSubsequentDigits(StringBuffer buffer, LocatedChar c) {
		boolean isInt = true;
		while(c.isDigit()) {
			buffer.append(c.getCharacter());
			c = input.next();
		}
		//Check for floating type
		if(c.isDecimal()) {
			LocatedChar temp = input.peek();
			if(temp.isDigit()) {
				isInt = false;
				buffer.append(c.getCharacter());
				c = input.next();
				while(c.isDigit()) {
					buffer.append(c.getCharacter());
					c = input.next();
				}
				//optional E
				if(c.isE()) {
					buffer.append(c.getCharacter());
					c = input.next();
					if(c.isSign()) {
						buffer.append(c.getCharacter());
						c = input.next();
					}
					while(c.isDigit()) {
						buffer.append(c.getCharacter());
						c = input.next();
					}
				}
			}
		}
		input.pushback(c);
		return isInt;
	}
	
	
	//////////////////////////////////////////////////////////////////////////////
	// Identifier and keyword lexical analysis	

	private Token scanIdentifier(LocatedChar firstChar) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(firstChar.getCharacter());
		appendSubsequent(buffer);

		String lexeme = buffer.toString();
		if(Keyword.isAKeyword(lexeme)) {
			return LextantToken.make(firstChar.getLocation(), lexeme, Keyword.forLexeme(lexeme));
		}
		else {
			return IdentifierToken.make(firstChar.getLocation(), lexeme);
		}
	}
	private void appendSubsequent(StringBuffer buffer) {
		int count = 1;
		LocatedChar c = input.next();
		while(c.isValidIDChar()) {
			buffer.append(c.getCharacter());
			c = input.next();
			count++;
		}
		if(count > 32) {
			identifierError(buffer.toString());
		}
		input.pushback(c);
	}
	
	//////////////////////////////////////////////////////////////////////////////
	// Char analysis
	
	private Token scanChar() {
		StringBuffer buffer = new StringBuffer();
		LocatedChar c = input.next();
		buffer.append(c.getCharacter());
		TextLocation location = c.getLocation();
		c = input.next();
		if(c.isCarot()) {
			return CharConstantToken.make(location, buffer.toString());
		} else {
			lexicalError(c);
			return findNextToken();
		}
		
	}
	
	//////////////////////////////////////////////////////////////////////////////
	// String analysis
	
	private Token scanString() {
		StringBuffer buffer = new StringBuffer();
		LocatedChar c = input.next();
		TextLocation location = c.getLocation();
		while(!c.isStringStart()) {
			if(c.isEndLine()) {
				lexicalError(c);
				return findNextToken();
			}
			buffer.append(c.getCharacter());
			c = input.next();
		}
		
		return StringConstantToken.make(location, buffer.toString());
	}
	
	
	//////////////////////////////////////////////////////////////////////////////
	// Punctuator lexical analysis	
	// old method left in to show a simple scanning method.
	// current method is the algorithm object PunctuatorScanner.java

	@SuppressWarnings("unused")
	private Token oldScanPunctuator(LocatedChar ch) {
		TextLocation location = ch.getLocation();
		
		switch(ch.getCharacter()) {
		case '*':
			return LextantToken.make(location, "*", Punctuator.MULTIPLY);
		case '+':
			return LextantToken.make(location, "+", Punctuator.ADD);
		case '>':
			return LextantToken.make(location, ">", Punctuator.GREATER);
		case ':':
			if(ch.getCharacter()=='=') {
				return LextantToken.make(location, ":=", Punctuator.ASSIGN);
			}
			else {
				throw new IllegalArgumentException("found : not followed by = in scanOperator");
			}
		case ',':
			return LextantToken.make(location, ",", Punctuator.SEPARATOR);
		case ';':
			return LextantToken.make(location, ";", Punctuator.TERMINATOR);
		default:
			throw new IllegalArgumentException("bad LocatedChar " + ch + "in scanOperator");
		}
	}

	

	//////////////////////////////////////////////////////////////////////////////
	// Character-classification routines specific to Pika scanning.	

	private boolean isPunctuatorStart(LocatedChar lc) {
		char c = lc.getCharacter();
		return isPunctuatorStartingCharacter(c);
	}

	private boolean isEndOfInput(LocatedChar lc) {
		return lc == LocatedCharStream.FLAG_END_OF_INPUT;
	}
	
	
	//////////////////////////////////////////////////////////////////////////////
	// Error-reporting	

	private void lexicalError(LocatedChar ch) {
		PikaLogger log = PikaLogger.getLogger("compiler.lexicalAnalyzer");
		log.severe("Lexical error: invalid character " + ch);
	}
	
	private void identifierError(String s) {
		PikaLogger log = PikaLogger.getLogger("compiler.lexicalAnalyzer");
		log.severe("Lexical error: identifer " + s + " is too long.");
	}

	
}
