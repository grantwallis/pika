package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;

public class StringStringConcatCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("string-string-concat");
		String loopLabel = labeller.newLabel("loop");
		String loopLabel2 = labeller.newLabel("loop2");
		String firstEmptyLabel = labeller.newLabel("first-empty");
		String secondEmptyLabel = labeller.newLabel("second-empty");
		String endLabel = labeller.newLabel("end");
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_VALUE);				
		Macros.storeITo(frag, RunTime.STRING_INDEXING_STRING2);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_STRING);
		
		//calc and store lengths
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING2);
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_END2);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_END);
		
		//check if either lengths are zero
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		frag.add(JumpFalse, firstEmptyLabel);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END2);
		frag.add(JumpFalse, secondEmptyLabel);
		
		//create new string record
		frag.add(Add);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_LENGTH_TEMPORARY);
		RunTime.CreateEmptyString(frag, 9);
		
		//new length
		frag.add(Duplicate);
		
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_LENGTH_TEMPORARY);
		frag.add(StoreI);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_START);
		
		
		//copy first string
		frag.add(Label, loopLabel);
		
		//new string
		frag.add(Duplicate);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old string
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Add);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//loop check
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP);
		Macros.incrementInteger(frag, RunTime.STRING_INDEXING_INDEX_START);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Subtract);
		frag.add(JumpPos, loopLabel);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_START2);
		
		//copy second string
		frag.add(Label, loopLabel2);
		
		//new string
		frag.add(Duplicate);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old string
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING2);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START2);
		frag.add(Add);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//loop check
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP);
		Macros.incrementInteger(frag, RunTime.STRING_INDEXING_INDEX_START2);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END2);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START2);
		frag.add(Subtract);
		frag.add(JumpPos, loopLabel2);
		frag.add(Jump, endLabel);
		
		//first string is empty, return second string
		frag.add(Label, firstEmptyLabel);
		frag.add(Pop);
		frag.add(Pop);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING2);
		frag.add(Jump, endLabel);
		
		//second string is empty, return first string
		frag.add(Label, secondEmptyLabel);
		frag.add(Pop);
		frag.add(Pop);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		
		frag.add(Label, endLabel);
		
		return frag;
	}
	
	
}