package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.*;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import parseTree.ParseNode;

public class FloatingRationalizeCodeGenerator implements SimpleCodeGenerator {
	public ASMCodeFragment generate(ParseNode node) {
		ASMCodeFragment fragment = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		fragment.add(Duplicate);
		fragment.add(JumpFalse, RunTime.RATIONAL_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		
		fragment.add(PushD, RunTime.INTEGER_TEMP);
		fragment.add(Exchange);
		fragment.add(StoreI);
		
		fragment.add(PushD, RunTime.INTEGER_TEMP);
		fragment.add(LoadI);
		fragment.add(ConvertF);
		fragment.add(FMultiply);
		fragment.add(ConvertI);
		
		fragment.add(PushD, RunTime.INTEGER_TEMP);
		fragment.add(LoadI);
		fragment.add(Call, RunTime.LOWEST_TERMS);
		return fragment;
	}
}