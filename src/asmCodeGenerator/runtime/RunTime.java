package asmCodeGenerator.runtime;
import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.*;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;

import asmCodeGenerator.Labeller;
import asmCodeGenerator.Macros;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
public class RunTime {
	public static final String EAT_LOCATION_ZERO      		 	 = "$eat-location-zero";		// helps us distinguish null pointers from real ones.
	public static final String INTEGER_PRINT_FORMAT   		 	 = "$print-format-integer";
	public static final String FLOATING_PRINT_FORMAT  		 	 = "$print-format-floating";
	public static final String BOOLEAN_PRINT_FORMAT   		 	 = "$print-format-boolean";
	public static final String CHAR_PRINT_FORMAT      		 	 = "$print-format-char";
	public static final String STRING_PRINT_FORMAT   		  	 = "$print-format-string";
	public static final String MIXED_NUMBER_PRINT_FORMAT      	 = "$print-format-mixed";
	public static final String PROPER_FRACTION_PRINT_FORMAT  	 = "$print-format-fraction";
	public static final String NEG_PROPER_FRACTION_PRINT_FORMAT  = "$print-format-neg-fraction";
	public static final String NEWLINE_PRINT_FORMAT   			 = "$print-format-newline";
	public static final String ARRAY_START_PRINT_FORMAT			 = "$print-format-array-start";
	public static final String ARRAY_END_PRINT_FORMAT			 = "$print-format-array-end";
	public static final String ARRAY_SEPARATOR_FORMAT			 = "$print-format-array-separator";
	public static final String SPACE_PRINT_FORMAT     		 	 = "$print-format-space";
	public static final String TAB_PRINT_FORMAT      			 = "$print-format-tab";
	public static final String BOOLEAN_TRUE_STRING   			 = "$boolean-true-string";
	public static final String BOOLEAN_FALSE_STRING  			 = "$boolean-false-string";
	public static final String GLOBAL_MEMORY_BLOCK   			 = "$global-memory-block";
	public static final String FRAME_POINTER				 	 = "$frame-pointer";
	public static final String STACK_POINTER				 	 = "$stack-pointer";
	public static final String USABLE_MEMORY_START  			 = "$usable-memory-start";
	public static final String MAIN_PROGRAM_LABEL    			 = "$$main";
	
	public static final String LOWEST_TERMS = "$$lowest-terms";
	public static final String CLEAR_N_BYTES = "$$clear-n-bytes";
	public static final String DEALLOCATE = "$$deallocate";
	public static final String PRINT_ARRAY = "$$print-array";
	public static final String PRINT_RAT = "$$print-rat";
	
	public static final String RATIONAL_NUMERATOR_TEMP = "$rational-numerator-temp";
	public static final String RATIONAL_DENOMINATOR_TEMP = "$rational-denominator-temp";
	public static final String INTEGER_TEMP = "$integer-temp";
	public static final String INTEGER_TEMP2 = "$integer-temp2";
	public static final String CHAR_TEMP = "$char-temp";
	public static final String RETURN_ADDRESS = "$return-address";
	
	public static final String ARRAY_INDEXING_ARRAY = "$a-indexing-array";
	public static final String ARRAY_INDEXING_INDEX = "$a-indexing-index";
	public static final String ARRAY_DATASIZE_TEMPORARY = "$a-datasize-temp";
	public static final String ARRAY_ADDRESS_ORIGINAL = "$a-address-original";
	public static final String ARRAY_ADDRESS_ORIGINAL2 = "$a-address-original2";
	public static final String ARRAY_ADDRESS_COPY = "$a-address-copy";
	public static final String ARRAY_SUBTYPE_SIZE = "$a-subtype-size";
	public static final String ARRAY_ELEMENT_LOC_NEW = "a-element-loc-new";
	public static final String ARRAY_ELEMENT_LOC_ORIGINAL = "a-element-loc-original";
	public static final String ARRAY_LENGTH = "a-length";
	
	public static final String STRING_LENGTH_TEMPORARY = "$s-length-temp";
	public static final String STRING_INDEXING_STRING = "$s-indexing-string";
	public static final String STRING_INDEXING_STRING2 = "$s-indexing-string2";
	public static final String STRING_INDEXING_INDEX_START = "$s-indexing-index-start";
	public static final String STRING_INDEXING_INDEX_END = "$s-indexing-index-end";
	public static final String STRING_INDEXING_INDEX_START2 = "$s-indexing-index-start2";
	public static final String STRING_INDEXING_INDEX_END2 = "$s-indexing-index-end2";
	
	public static final String LOOP_SEQUENCE = "l-sequence";
	public static final String LOOP_IDENTIFIER = "l-identifier";
	public static final String LOOP_CURRENT_INDEX = "l-current-index";
	public static final String LOOP_END_INDEX = "l-end-index";
	public static final String LOOP_CURRENT_ELEMENT = "l-current-element";
	
	public static final int ARRAY_RECORD_LENGTH_OFFSET = 12;
	public static final int ARRAY_HEADER_SIZE = 16;
	public static final int ARRAY_SUBTYPE_OFFSET = 8;
	public static final int ARRAY_TYPE_ID = 7;
	
	public static final int STRING_TYPE_ID = 6;
	public static final int STRING_HEADER_SIZE = 12;
	public static final int STRING_RECORD_LENGTH_OFFSET = 8;
	
	public static final int RECORD_TYPEID_OFFSET = 0;
	public static final int RECORD_STATUS_OFFSET = 4;
	
	public static final String RECORD_CREATION_TEMP = "$record-creation-temp";


	
	public static final String GENERAL_RUNTIME_ERROR = "$$general-runtime-error";
	public static final String INTEGER_DIVIDE_BY_ZERO_RUNTIME_ERROR = "$$i-divide-by-zero";
	public static final String FLOATING_DIVIDE_BY_ZERO_RUNTIME_ERROR = "$$f-divide-by-zero";
	public static final String RATIONAL_DIVIDE_BY_ZERO_RUNTIME_ERROR = "$$r-divide-by-zero";
	public static final String NULL_ARRAY_RUNTIME_ERROR = "$$a-null-array";
	public static final String INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR = "$$a-out-of-bounds";
	public static final String NEGATIVE_ARRAY_LENGTH_RUNTIME_ERROR = "$$a-neg-length";
	public static final String DELETED_RECORD_ACCESS_RUNTIME_ERROR = "$$rec-deleted-access";
	public static final String NO_RETURN_STATEMENT_RUNTIME_ERROR = "$$l-no-return-statement";
	public static final String STRING_INDEXING_RANGE_RUNTIME_ERROR = "$$s-index-range";
	public static final String ARRAYS_DIFFERENT_LENGTH_RUNTIME_ERROR = "$$a-different-length";
	public static final String FOLD_EMPTY_ARRAY_RUNTIME_ERROR = "$$a-empty";


	private ASMCodeFragment environmentASM() {
		ASMCodeFragment result = new ASMCodeFragment(GENERATES_VOID);
		result.append(initializePointers());
		result.append(jumpToMain());
		result.append(stringsForPrintf());
		result.append(runtimeErrors());
		result.append(tempVariables());
		result.append(lowestTerms());
		result.append(clearNBytes());
		result.append(deallocate());
		result.add(DLabel, USABLE_MEMORY_START);
		return result;
	}
	
	private ASMCodeFragment tempVariables() {
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		Macros.declareI(frag, RATIONAL_NUMERATOR_TEMP);
		Macros.declareI(frag, RATIONAL_DENOMINATOR_TEMP);
		Macros.declareI(frag, RETURN_ADDRESS);
		Macros.declareI(frag, INTEGER_TEMP);
		Macros.declareI(frag, INTEGER_TEMP2);
		Macros.declareI(frag, CHAR_TEMP);
		
		Macros.declareI(frag, ARRAY_INDEXING_ARRAY);
		Macros.declareI(frag, ARRAY_INDEXING_INDEX);
		Macros.declareI(frag, RECORD_CREATION_TEMP);
		Macros.declareI(frag, ARRAY_DATASIZE_TEMPORARY);
		Macros.declareI(frag, ARRAY_ADDRESS_ORIGINAL);
		Macros.declareI(frag, ARRAY_ADDRESS_ORIGINAL2);
		Macros.declareI(frag, ARRAY_ADDRESS_COPY);
		Macros.declareI(frag, ARRAY_SUBTYPE_SIZE);
		Macros.declareI(frag, ARRAY_ELEMENT_LOC_NEW);
		Macros.declareI(frag, ARRAY_ELEMENT_LOC_ORIGINAL);
		
		Macros.declareI(frag, STRING_INDEXING_STRING);
		Macros.declareI(frag, STRING_INDEXING_STRING2);
		Macros.declareI(frag, STRING_INDEXING_INDEX_START);
		Macros.declareI(frag, STRING_INDEXING_INDEX_END);
		Macros.declareI(frag, STRING_INDEXING_INDEX_START2);
		Macros.declareI(frag, STRING_INDEXING_INDEX_END2);
		
		Macros.declareI(frag, LOOP_SEQUENCE);
		Macros.declareI(frag, LOOP_IDENTIFIER);
		Macros.declareI(frag, LOOP_CURRENT_INDEX);
		Macros.declareI(frag, LOOP_END_INDEX);
		Macros.declareI(frag, LOOP_CURRENT_ELEMENT);
		return frag;
	}
	
	private ASMCodeFragment initializePointers() {
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(DLabel, FRAME_POINTER);
		frag.add(DataI, 0);
		frag.add(PushD, FRAME_POINTER);
		frag.add(Memtop);
		frag.add(StoreI);
		
		frag.add(DLabel, STACK_POINTER);
		frag.add(DataI, 0);
		frag.add(PushD, STACK_POINTER);
		frag.add(Memtop);
		frag.add(StoreI);
		
		return frag;
	}
	
	private ASMCodeFragment jumpToMain() {
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(Jump, MAIN_PROGRAM_LABEL);
		return frag;
	}

	private ASMCodeFragment stringsForPrintf() {
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(DLabel, EAT_LOCATION_ZERO);
		frag.add(DataZ, 8);
		frag.add(DLabel, INTEGER_PRINT_FORMAT);
		frag.add(DataS, "%d");
		frag.add(DLabel, FLOATING_PRINT_FORMAT);
		frag.add(DataS, "%g");
		frag.add(DLabel, BOOLEAN_PRINT_FORMAT);
		frag.add(DataS, "%s");
		frag.add(DLabel, STRING_PRINT_FORMAT);
		frag.add(DataS, "%s");
		frag.add(DLabel, MIXED_NUMBER_PRINT_FORMAT);
		frag.add(DataS, "%d_%d/%d");
		frag.add(DLabel, PROPER_FRACTION_PRINT_FORMAT);
		frag.add(DataS, "_%d/%d");
		frag.add(DLabel, NEG_PROPER_FRACTION_PRINT_FORMAT);
		frag.add(DataS, "-_%d/%d");
		frag.add(DLabel, CHAR_PRINT_FORMAT);
		frag.add(DataS, "%c");
		frag.add(DLabel, NEWLINE_PRINT_FORMAT);
		frag.add(DataS, "\n");
		frag.add(DLabel, ARRAY_START_PRINT_FORMAT);
		frag.add(DataS, "[");
		frag.add(DLabel, ARRAY_END_PRINT_FORMAT);
		frag.add(DataS, "]");
		frag.add(DLabel, ARRAY_SEPARATOR_FORMAT);
		frag.add(DataS, ", ");
		frag.add(DLabel, SPACE_PRINT_FORMAT);
		frag.add(DataS, " ");
		frag.add(DLabel, TAB_PRINT_FORMAT);
		frag.add(DataS, "\t");
		frag.add(DLabel, BOOLEAN_TRUE_STRING);
		frag.add(DataS, "true");
		frag.add(DLabel, BOOLEAN_FALSE_STRING);
		frag.add(DataS, "false");
		
		return frag;
	}
	
	
	private ASMCodeFragment runtimeErrors() {
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		
		generalRuntimeError(frag);
		integerDivideByZeroError(frag);
		floatingDivideByZeroError(frag);
		rationalDivideByZeroError(frag);
		nullArrayError(frag);
		indexOutOfBoundsError(frag);
		negativeArrayLengthError(frag);
		deletedRecordAccessError(frag);
		noReturnStatementError(frag);
		stringIndexRangeError(frag);
		arrayDifferentLengthError(frag);
		foldEmptyArrayError(frag);
		return frag;
	}
	private ASMCodeFragment generalRuntimeError(ASMCodeFragment frag) {
		String generalErrorMessage = "$errors-general-message";

		frag.add(DLabel, generalErrorMessage);
		frag.add(DataS, "Runtime error: %s\n");
		
		frag.add(Label, GENERAL_RUNTIME_ERROR);
		frag.add(PushD, generalErrorMessage);
		frag.add(Printf);
		frag.add(Halt);
		return frag;
	}
	private void integerDivideByZeroError(ASMCodeFragment frag) {
		String intDivideByZeroMessage = "$errors-int-divide-by-zero";
		
		frag.add(DLabel, intDivideByZeroMessage);
		frag.add(DataS, "integer divide by zero");
		
		frag.add(Label, INTEGER_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		frag.add(PushD, intDivideByZeroMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	private void floatingDivideByZeroError(ASMCodeFragment frag) {
		String floatDivideByZeroMessage = "$errors-float-divide-by-zero";
		
		frag.add(DLabel, floatDivideByZeroMessage);
		frag.add(DataS, "floating divide by zero");
		
		frag.add(Label, FLOATING_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		frag.add(PushD, floatDivideByZeroMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	private void rationalDivideByZeroError(ASMCodeFragment frag) {
		String ratDivideByZeroMessage = "$errors-rat-divide-by-zero";
		
		frag.add(DLabel, ratDivideByZeroMessage);
		frag.add(DataS, "rational divide by zero");
		
		frag.add(Label, RATIONAL_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		frag.add(PushD, ratDivideByZeroMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void nullArrayError(ASMCodeFragment frag) {
		String nullArrayMessage = "$errors-null-array";
		
		frag.add(DLabel, nullArrayMessage);
		frag.add(DataS, "null array");
		
		frag.add(Label, NULL_ARRAY_RUNTIME_ERROR);
		frag.add(PushD, nullArrayMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void indexOutOfBoundsError(ASMCodeFragment frag) {
		String outOfBoundsMessage = "$errors-index-out-of-bounds";
		
		frag.add(DLabel, outOfBoundsMessage);
		frag.add(DataS, "index out of bounds");
		
		frag.add(Label, INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		frag.add(PushD, outOfBoundsMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void negativeArrayLengthError(ASMCodeFragment frag) {
		String negArrayLengthMessage = "$errors-negative-array-length";
		
		frag.add(DLabel, negArrayLengthMessage);
		frag.add(DataS, "negative array length");
		
		frag.add(Label, NEGATIVE_ARRAY_LENGTH_RUNTIME_ERROR);
		frag.add(PushD, negArrayLengthMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void deletedRecordAccessError(ASMCodeFragment frag) {
		String DeletedRecordAccessMessage = "$errors-deleted-record-access";
		
		frag.add(DLabel, DeletedRecordAccessMessage);
		frag.add(DataS, "deleted record access");
		
		frag.add(Label, DELETED_RECORD_ACCESS_RUNTIME_ERROR);
		frag.add(PushD, DeletedRecordAccessMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void noReturnStatementError(ASMCodeFragment frag) {
		String noReturnStatementMessage = "$errors-no-return-statement";
		
		frag.add(DLabel, noReturnStatementMessage);
		frag.add(DataS, "function ended with no return");
		
		frag.add(Label, NO_RETURN_STATEMENT_RUNTIME_ERROR);
		frag.add(PushD, noReturnStatementMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void stringIndexRangeError(ASMCodeFragment frag) {
		String stringIndexingRangeMessage = "$errors-index-range-invalid";
		
		frag.add(DLabel, stringIndexingRangeMessage);
		frag.add(DataS, "invalid index range. In [i,j] i must be less than j");
		
		frag.add(Label, STRING_INDEXING_RANGE_RUNTIME_ERROR);
		frag.add(PushD, stringIndexingRangeMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void arrayDifferentLengthError(ASMCodeFragment frag) {
		String arrayDifferentLengthMessage = "$errors-arrays-different-length";
		
		frag.add(DLabel, arrayDifferentLengthMessage);
		frag.add(DataS, "arrays in zip operation must be of the same length");
		
		frag.add(Label, ARRAYS_DIFFERENT_LENGTH_RUNTIME_ERROR);
		frag.add(PushD, arrayDifferentLengthMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	private void foldEmptyArrayError(ASMCodeFragment frag) {
		String foldEmptyArrayMessage = "$errors-empty-array";
		
		frag.add(DLabel, foldEmptyArrayMessage);
		frag.add(DataS, "array in fold must not be empty");
		
		frag.add(Label, FOLD_EMPTY_ARRAY_RUNTIME_ERROR);
		frag.add(PushD, foldEmptyArrayMessage);
		frag.add(Jump, GENERAL_RUNTIME_ERROR);
	}
	
	
	
	private ASMCodeFragment lowestTerms() {
		
		Labeller labeller = new Labeller("lowest-terms");
		String startLabel = labeller.newLabel("start");
		String negativeLabel = labeller.newLabel("neg");
		String join = labeller.newLabel("join");
		
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(Label, LOWEST_TERMS);
			
		//stash return
		frag.add(PushD, RETURN_ADDRESS);
		frag.add(Exchange);
		frag.add(StoreI);
		
		//check divide by 0
		frag.add(Duplicate);
		frag.add(JumpFalse, RATIONAL_DIVIDE_BY_ZERO_RUNTIME_ERROR);
		
		//stash numerator and denominator
		frag.add(PushD, RATIONAL_DENOMINATOR_TEMP);
		frag.add(Exchange);
		frag.add(StoreI);
		frag.add(PushD, RATIONAL_NUMERATOR_TEMP);
		frag.add(Exchange);
		frag.add(StoreI);	
		
		frag.add(PushD, RATIONAL_NUMERATOR_TEMP);
		frag.add(LoadI);
		frag.add(PushD, RATIONAL_DENOMINATOR_TEMP);
		frag.add(LoadI);
		
		//find GCD using Euclidean algorithm
		frag.add(Label, startLabel);
		frag.add(Duplicate);
		frag.add(PushD, INTEGER_TEMP);
		frag.add(Exchange);
		frag.add(StoreI);
		frag.add(Remainder);
		frag.add(PushD, INTEGER_TEMP);
		frag.add(LoadI);
		frag.add(Exchange);
		frag.add(Duplicate);
		frag.add(JumpTrue, startLabel);
		
		//divide numerator by gcd
		frag.add(Pop);
		frag.add(PushD, RATIONAL_NUMERATOR_TEMP);
		frag.add(LoadI);
		frag.add(Exchange);
		frag.add(Divide);
		
		//divide denominator by gcd
		frag.add(PushD, RATIONAL_DENOMINATOR_TEMP);
		frag.add(LoadI);
		frag.add(PushD, INTEGER_TEMP);
		frag.add(LoadI);
		frag.add(Divide);
		
		frag.add(Duplicate);
		frag.add(JumpNeg, negativeLabel);
		frag.add(Jump, join);
		
		frag.add(Label, negativeLabel);
		frag.add(Exchange);
		frag.add(Negate);
		frag.add(Exchange);
		frag.add(Negate);
		
		//load return address
		frag.add(Label, join);
		frag.add(PushD, RETURN_ADDRESS);
		frag.add(LoadI);
		frag.add(Return);
		return frag;
	}
	
	public static void createRecord(ASMCodeFragment code, int typeCode, int statusFlags) {
		code.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE);
		Macros.storeITo(code, RECORD_CREATION_TEMP);
		code.add(PushI, typeCode);
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
		Macros.writeIOffset(code, RECORD_TYPEID_OFFSET);
		
		code.add(PushI, statusFlags);
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
		Macros.writeIOffset(code, RECORD_STATUS_OFFSET);
	}
	
	public static void CreateEmptyString(ASMCodeFragment code, int statusFlags) {
		final int typecode = STRING_TYPE_ID;
	
		Macros.storeITo(code, STRING_LENGTH_TEMPORARY);
		Macros.loadIFrom(code, STRING_LENGTH_TEMPORARY);
		code.add(PushI, STRING_HEADER_SIZE);
		code.add(Add);
		code.add(PushI, 1);
		code.add(Add); //for null character
		createRecord(code, typecode, statusFlags);
		
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
		code.add(PushI, STRING_RECORD_LENGTH_OFFSET);
		code.add(Add);
		Macros.loadIFrom(code, STRING_LENGTH_TEMPORARY);
		code.add(StoreI);
		
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
	}
	
	public static void createEmptyArrayRecord(ASMCodeFragment code, int statusFlags, int subtypeSize) {
		final int typecode = ARRAY_TYPE_ID;
		code.add(Duplicate);
		code.add(JumpNeg, NEGATIVE_ARRAY_LENGTH_RUNTIME_ERROR);
		
		code.add(Duplicate);
		code.add(PushI, subtypeSize);
		code.add(Multiply);
		code.add(Duplicate);
		Macros.storeITo(code, ARRAY_DATASIZE_TEMPORARY);
		code.add(PushI, ARRAY_HEADER_SIZE);
		code.add(Add);
		
		createRecord(code, typecode, statusFlags);
		
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
		code.add(PushI, ARRAY_HEADER_SIZE);
		code.add(Add);
		Macros.loadIFrom(code, ARRAY_DATASIZE_TEMPORARY);
		code.add(Call, CLEAR_N_BYTES);
		
		
		code.add(PushI, subtypeSize);
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
		Macros.writeIOffset(code, ARRAY_SUBTYPE_OFFSET);
		
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
		code.add(PushI, ARRAY_RECORD_LENGTH_OFFSET);
		code.add(Add);
		code.add(Exchange);
		code.add(StoreI);
		
		Macros.loadIFrom(code, RECORD_CREATION_TEMP);
	}
	
	public static void deallocateRecord(ASMCodeFragment code) {
		code.add(Call, DEALLOCATE);
	}
	
	private ASMCodeFragment deallocate() {
		Labeller labeller = new Labeller("deallocate");
		String falseLabel = labeller.newLabel("false");
		String loopLabel = labeller.newLabel("loop");
		String skipDeallocateLabel = labeller.newLabel("skip-deallocate");
		
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
		code.add(Label, DEALLOCATE);
		code.add(Exchange);
		
		
		//skip if already deallocated
		code.add(Duplicate);
		code.add(PushI, RECORD_STATUS_OFFSET);
		code.add(Add);
		code.add(LoadI);
		code.add(PushI, 4); 
		code.add(BTAnd);
		code.add(JumpTrue, skipDeallocateLabel);
		
		//skip if permanent
		code.add(Duplicate);
		code.add(PushI, RECORD_STATUS_OFFSET);
		code.add(Add);
		code.add(LoadI);
		code.add(PushI, 8); 
		code.add(BTAnd);
		code.add(JumpTrue, skipDeallocateLabel);
		
		code.add(Duplicate);
		code.add(PushI, RECORD_STATUS_OFFSET);
		code.add(Add);
		code.add(LoadI);
		code.add(PushI, 2);
		code.add(BTAnd);
		code.add(JumpFalse, falseLabel);
		
		code.add(Duplicate);
		code.add(PushI, ARRAY_RECORD_LENGTH_OFFSET);
		code.add(Add);
		code.add(LoadI);
		code.add(PushI, 1);
		code.add(Subtract);
		Macros.storeITo(code, INTEGER_TEMP); //length
		
		code.add(Label, loopLabel);
		code.add(Duplicate);
		code.add(PushI, ARRAY_HEADER_SIZE);
		code.add(Add);
		Macros.loadIFrom(code, INTEGER_TEMP);
		code.add(PushI, 4); //address will always be size 4
		code.add(Multiply);
		code.add(Add);
		code.add(LoadI);
		Macros.loadIFrom(code, INTEGER_TEMP);
		code.add(Exchange);
		code.add(Call, DEALLOCATE);
		
		code.add(PushI, 1);
		code.add(Subtract);
		Macros.storeITo(code, INTEGER_TEMP);
		Macros.loadIFrom(code, INTEGER_TEMP);
		code.add(JumpNeg, falseLabel);
		code.add(Jump, loopLabel);
		
		code.add(Label, falseLabel);	
		//deallocate
		code.add(Duplicate);
		code.add(PushI, RECORD_STATUS_OFFSET);
		code.add(Add);
		code.add(PushI, 4); // status: 0100 - deleted
		code.add(StoreI);
		code.add(Call, MemoryManager.MEM_MANAGER_DEALLOCATE);
		code.add(Return);
		
		code.add(Label, skipDeallocateLabel);
		code.add(Pop);
		code.add(Return);
		return code;
	}
	
	private ASMCodeFragment clearNBytes() {
		Labeller labeller = new Labeller("clear-n-bytes");
		String startLabel = labeller.newLabel("start");
		String join = labeller.newLabel("join");
		
		ASMCodeFragment frag = new ASMCodeFragment(GENERATES_VOID);
		frag.add(Label, CLEAR_N_BYTES);
		Macros.storeITo(frag, RETURN_ADDRESS);
		frag.add(PushI, 1);
		frag.add(Subtract); // we will be clearing from address +  to address + datasize - 1
		Macros.storeITo(frag, INTEGER_TEMP);
		Macros.loadIFrom(frag, INTEGER_TEMP);
		
		frag.add(Label, startLabel);
		frag.add(Add);
		frag.add(PushI, 0);
		frag.add(StoreC);
		Macros.loadIFrom(frag, RECORD_CREATION_TEMP);
		frag.add(PushI, ARRAY_HEADER_SIZE);
		frag.add(Add);
		
		//decrement
		Macros.loadIFrom(frag, INTEGER_TEMP);
		frag.add(PushI, 1);
		frag.add(Subtract);
		Macros.storeITo(frag, INTEGER_TEMP);
		Macros.loadIFrom(frag, INTEGER_TEMP);
		frag.add(Duplicate);
		frag.add(JumpNeg, join);
		frag.add(Jump, startLabel);
		
		
		frag.add(Label, join);
		frag.add(Pop);
		frag.add(Pop);
		Macros.loadIFrom(frag, RETURN_ADDRESS);
		frag.add(Return);
		return frag;
	}
	
	
	
	
	public static ASMCodeFragment getEnvironment() {
		RunTime rt = new RunTime();
		return rt.environmentASM();
	}
}
