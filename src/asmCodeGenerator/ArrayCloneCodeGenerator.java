package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.MemoryManager;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;
import semanticAnalyzer.types.*;

public class ArrayCloneCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("clone");
		String startLabel = labeller.newLabel("start");
		String join = labeller.newLabel("join");
		
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		Macros.storeITo(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		frag.add(PushI, RunTime.ARRAY_SUBTYPE_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		frag.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Multiply);
		
		frag.add(PushI, RunTime.ARRAY_HEADER_SIZE);
		frag.add(Add);
		Macros.storeITo(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		
		frag.add(PushI, 1);
		frag.add(Subtract);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP); //#bytes to copy
		
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		frag.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE);
		Macros.storeITo(frag, RunTime.ARRAY_ADDRESS_COPY);
		
		frag.add(Label, startLabel);
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_COPY);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//decrement
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(PushI, 1);
		frag.add(Subtract);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(JumpNeg, join);
		frag.add(Jump, startLabel);
		
		frag.add(Label, join);
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_COPY);
		return frag;
	}
}