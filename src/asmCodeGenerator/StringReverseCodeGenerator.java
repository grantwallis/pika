package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;

public class StringReverseCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("string-reverse");
		String loopLabel = labeller.newLabel("loop");
		String firstEmptyLabel = labeller.newLabel("first-empty");
		String endLabel = labeller.newLabel("end");
		
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_VALUE);				
		Macros.storeITo(frag, RunTime.STRING_INDEXING_STRING);
		
		//store length
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_END);
		
		//if length is zero then just return the same string
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		frag.add(JumpFalse, firstEmptyLabel);
		
		//create new string record
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_LENGTH_TEMPORARY);
		RunTime.CreateEmptyString(frag, 9);
		
		//new length
		frag.add(Duplicate);
		
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_LENGTH_TEMPORARY);
		frag.add(StoreI);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		
		//start at length - 1, work down to 0
		Macros.loadIFrom(frag, RunTime.STRING_LENGTH_TEMPORARY);
		frag.add(PushI, 1);
		frag.add(Subtract);
		Macros.storeITo(frag, RunTime.STRING_LENGTH_TEMPORARY);
		
		//copy first string
		frag.add(Label, loopLabel);
		
		//new string
		frag.add(Duplicate);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old string
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_LENGTH_TEMPORARY);
		frag.add(Add);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//loop check
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP);
		Macros.decrementInteger(frag, RunTime.STRING_LENGTH_TEMPORARY);
		Macros.loadIFrom(frag, RunTime.STRING_LENGTH_TEMPORARY);
		frag.add(JumpNeg, endLabel);
		frag.add(Jump, loopLabel);		
		
		//first string is empty, return second string
		frag.add(Label, firstEmptyLabel);
		frag.add(Pop);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(Jump, endLabel);
		
		frag.add(Label, endLabel);
		
		return frag;
	}
	
	
}