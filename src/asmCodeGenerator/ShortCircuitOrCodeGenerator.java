package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.*;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import parseTree.ParseNode;

public class ShortCircuitOrCodeGenerator implements FullCodeGenerator {
	@Override
	public ASMCodeFragment generate(ParseNode node, ASMCodeFragment...args) {
		ASMCodeFragment fragment = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		Labeller labeller = new Labeller("SC-Or");
		final String trueLabel = labeller.newLabel("Short-circuit-true");
		final String endLabel = labeller.newLabel("end");
		
		fragment.append(args[0]);
		
		fragment.add(Duplicate);
		fragment.add(JumpTrue, trueLabel);
		fragment.add(Pop);
		
		fragment.append(args[1]);
		fragment.add(Jump, endLabel);
		
		fragment.add(Label, trueLabel);
		fragment.add(Label, endLabel);
		return fragment;
	
	}
}