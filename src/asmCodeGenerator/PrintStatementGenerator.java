package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;
import parseTree.nodeTypes.NewlineNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.SpaceNode;
import parseTree.nodeTypes.TabNode;
import semanticAnalyzer.types.Array;
import semanticAnalyzer.types.Lambda;
import semanticAnalyzer.types.PrimitiveType;
import semanticAnalyzer.types.Type;
import asmCodeGenerator.ASMCodeGenerator.CodeVisitor;
import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.runtime.RunTime;

public class PrintStatementGenerator {
	ASMCodeFragment code;
	ASMCodeGenerator.CodeVisitor visitor;
	
	
	public PrintStatementGenerator(ASMCodeFragment code, CodeVisitor visitor) {
		super();
		this.code = code;
		this.visitor = visitor;
	}

	public void generate(PrintStatementNode node) {
		for(ParseNode child : node.getChildren()) {
			if(child instanceof NewlineNode || child instanceof SpaceNode || child instanceof TabNode) {
				ASMCodeFragment childCode = visitor.removeVoidCode(child);
				code.append(childCode);
			}
			else {
				appendPrintCode(child);
			}
		}
	}
	
	private void appendPrintCodeForType(Type type) {
		if(type instanceof Array) {
			Labeller labeller = new Labeller("print-array");
			String loopStart = labeller.newLabel("loop");
			String separatorLabel = labeller.newLabel("separator-label");
			String doneLabel = labeller.newLabel("done-label");
			Array arr = (Array)type;
			
			code.add(Duplicate);
			code.add(PushI, RunTime.RECORD_STATUS_OFFSET);
			code.add(Add);
			code.add(LoadI);
			code.add(PushI, 4);
			code.add(BTAnd);
			code.add(JumpTrue, RunTime.DELETED_RECORD_ACCESS_RUNTIME_ERROR);
			
			code.add(PushD, RunTime.ARRAY_START_PRINT_FORMAT);
			code.add(Printf);
			
			//if length is 0, skip printing elements
			code.add(Duplicate);
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			code.add(JumpFalse, doneLabel);
		
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.INTEGER_TEMP);	
			code.add(Jump, loopStart);
			
			code.add(Label, separatorLabel);
			code.add(PushD, RunTime.ARRAY_SEPARATOR_FORMAT);
			code.add(Printf);
			
			code.add(Label, loopStart);
			code.add(Duplicate);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP);
			code.add(PushI, arr.getSubtype().getSize());
			code.add(Multiply);
			code.add(Add);	
			
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP);
			code.add(Exchange);
			
			if(arr.getSubtype().getSize() == 1) {
				code.add(LoadC);
			} else if(arr.getSubtype().getSize() == 4) {
				code.add(LoadI);
			} else {
				if(arr.getSubtype() == PrimitiveType.RATIONAL) {
					code.add(Duplicate);
					code.add(PushI, 4);
					code.add(Add);
					code.add(Exchange);
					code.add(LoadI);
					code.add(Exchange);
					code.add(LoadI);
				} else {
					code.add(LoadF);
				}
			}		
			
			appendPrintCodeForType(arr.getSubtype());
			Macros.storeITo(code, RunTime.INTEGER_TEMP);
			Macros.incrementInteger(code, RunTime.INTEGER_TEMP);
			
			code.add(Duplicate);
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP);
			code.add(Subtract);
			
			code.add(JumpTrue, separatorLabel);
			
			code.add(Label, doneLabel);
			code.add(PushD, RunTime.ARRAY_END_PRINT_FORMAT);
			code.add(Printf);
			
			code.add(Pop);			
		}
		else if(type == PrimitiveType.RATIONAL) {
			appendMixedNumber();
			code.add(Printf);
			
		} else if(type == PrimitiveType.STRING) {
			String format = printFormat(type);
			code.add(PushI, RunTime.STRING_HEADER_SIZE);
			code.add(Add);
			code.add(PushD, format); 
			code.add(Printf);
			
		} else if(type == PrimitiveType.STRING_LITERAL) {
			code.add(PushD, printFormat(PrimitiveType.STRING));
			code.add(Printf);
		} else {
			String format = printFormat(type);
			convertToStringIfBoolean(type);
			
			code.add(PushD, format); 
			code.add(Printf);
		}
	}
	
	private void appendPrintCode(ParseNode node) {
		code.append(visitor.removeValueCode(node));
		appendPrintCodeForType(node.getType());
	}
	private void convertToStringIfBoolean(Type type) {
		if(type != PrimitiveType.BOOLEAN) {
			return;
		}
		
		Labeller labeller = new Labeller("print-boolean");
		String trueLabel = labeller.newLabel("true");
		String endLabel = labeller.newLabel("join");

		code.add(JumpTrue, trueLabel);
		code.add(PushD, RunTime.BOOLEAN_FALSE_STRING);
		code.add(Jump, endLabel);
		code.add(Label, trueLabel);
		code.add(PushD, RunTime.BOOLEAN_TRUE_STRING);
		code.add(Label, endLabel);
	}
	
	private void appendMixedNumber() {
		Labeller labeller = new Labeller("rationals");
		String wholeNumber = labeller.newLabel("whole-number");
		String properFraction = labeller.newLabel("proper-fraction");
		String positiveMixed = labeller.newLabel("positive-mixed");
		String positiveProper = labeller.newLabel("positive-proper");
		String printZero = labeller.newLabel("print-zero");
		String join = labeller.newLabel("join");
		
		
		
		Macros.storeITo(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
		Macros.storeITo(code, RunTime.RATIONAL_NUMERATOR_TEMP);
		
		Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
		code.add(JumpFalse, printZero);
	
		//jump to whole number if denominator is 1
		Macros.loadIFrom(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
		code.add(Duplicate);
		code.add(PushI, 1);
		code.add(Subtract);
		code.add(JumpFalse, wholeNumber);
	
		//numerator
		Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
		Macros.loadIFrom(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
		code.add(Remainder);
	
		//whole number
		Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
		Macros.loadIFrom(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
		code.add(Divide);
		
		//check for improper fraction
		code.add(Duplicate); 
		code.add(JumpFalse, properFraction);
		
		//case 1: print mixed number
		
		//get rid of negative in the fractional part
		code.add(Exchange);
		code.add(Duplicate);
		code.add(JumpPos, positiveMixed);
		code.add(Negate);
		code.add(Label, positiveMixed);
		code.add(Exchange);
		code.add(PushD, RunTime.MIXED_NUMBER_PRINT_FORMAT);
		code.add(Jump, join);
		
		//case 2: print integer
		code.add(Label, wholeNumber);
		code.add(Pop);
		Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
		code.add(PushD, RunTime.INTEGER_PRINT_FORMAT);
		code.add(Jump, join);
		
		//case 3: print proper fraction
		code.add(Label, properFraction);
		code.add(Pop);
		
		//get rid of negative in fractional part
		code.add(Duplicate);
		code.add(JumpPos, positiveProper);
		code.add(Negate);
		code.add(PushD, RunTime.NEG_PROPER_FRACTION_PRINT_FORMAT);
		code.add(Jump, join);
		
		code.add(Label, positiveProper);
		code.add(PushD, RunTime.PROPER_FRACTION_PRINT_FORMAT);
		code.add(Jump, join);
		
		code.add(Label, printZero);
		Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
		code.add(PushD, RunTime.INTEGER_PRINT_FORMAT);
		code.add(Jump, join);
		
		code.add(Label, join);
	}


	private static String printFormat(Type type) {
		assert type instanceof PrimitiveType;
		
		switch((PrimitiveType)type) {
		case INTEGER:	return RunTime.INTEGER_PRINT_FORMAT;
		case FLOATING:	return RunTime.FLOATING_PRINT_FORMAT;
		case BOOLEAN:	return RunTime.BOOLEAN_PRINT_FORMAT;
		case CHAR: 		return RunTime.CHAR_PRINT_FORMAT;
		case STRING:	return RunTime.STRING_PRINT_FORMAT;
		case RATIONAL:  return RunTime.MIXED_NUMBER_PRINT_FORMAT;
		default:		
			assert false : "Type " + type + " unimplemented in PrintStatementGenerator.printFormat()";
			return "";
		}
	}
}
