package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.*;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import parseTree.ParseNode;

public class ShortCircuitAndCodeGenerator implements FullCodeGenerator {
	@Override
	public ASMCodeFragment generate(ParseNode node, ASMCodeFragment...args) {
		ASMCodeFragment fragment = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		Labeller labeller = new Labeller("SC-And");
		final String falseLabel = labeller.newLabel("Short-circuit-false");
		final String endLabel = labeller.newLabel("end");
		
		fragment.append(args[0]);
		
		fragment.add(Duplicate);
		fragment.add(JumpFalse, falseLabel);
		fragment.add(Pop);
		
		fragment.append(args[1]);
		fragment.add(Jump, endLabel);
		
		fragment.add(Label, falseLabel);
		fragment.add(Label, endLabel);
		return fragment;
	
	}
}