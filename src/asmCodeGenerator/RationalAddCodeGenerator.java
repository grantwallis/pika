package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.*;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import parseTree.ParseNode;

public class RationalAddCodeGenerator implements SimpleCodeGenerator {
	public ASMCodeFragment generate(ParseNode node) {
		ASMCodeFragment fragment = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		fragment.add(PushD, RunTime.RATIONAL_DENOMINATOR_TEMP);
		fragment.add(Exchange);
		fragment.add(StoreI);
		
		fragment.add(PushD, RunTime.RATIONAL_NUMERATOR_TEMP);
		fragment.add(Exchange);
		fragment.add(StoreI);
		
		fragment.add(PushD, RunTime.INTEGER_TEMP);
		fragment.add(Exchange);
		fragment.add(StoreI);
		
		fragment.add(PushD, RunTime.RATIONAL_DENOMINATOR_TEMP);
		fragment.add(LoadI);
		fragment.add(Multiply);
		
		fragment.add(PushD, RunTime.RATIONAL_NUMERATOR_TEMP);
		fragment.add(LoadI);
		fragment.add(PushD, RunTime.INTEGER_TEMP);
		fragment.add(LoadI);
		fragment.add(Multiply);
		
		fragment.add(Add);
		fragment.add(PushD, RunTime.RATIONAL_DENOMINATOR_TEMP);
		fragment.add(LoadI);
		fragment.add(PushD, RunTime.INTEGER_TEMP);
		fragment.add(LoadI);
		fragment.add(Multiply);
		
		fragment.add(Call, RunTime.LOWEST_TERMS);
		return fragment;
	}
}