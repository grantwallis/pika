package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;

public class StringRangeIndexingCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("string-range-indexing");
		String label = labeller.newLabel("in-bounds");
		String loopLabel = labeller.newLabel("loop-label");
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_END);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_START);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_STRING);
		
		//start >= 0
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(JumpNeg, RunTime.INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		
		//end < length
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Subtract);
		frag.add(JumpNeg, label);
		frag.add(Jump, RunTime.INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		frag.add(Label, label);
		
		//end >= start
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Subtract);
		frag.add(Duplicate);
		frag.add(Duplicate);
		frag.add(JumpNeg, RunTime.STRING_INDEXING_RANGE_RUNTIME_ERROR);
		frag.add(JumpFalse, RunTime.STRING_INDEXING_RANGE_RUNTIME_ERROR);
		
		RunTime.CreateEmptyString(frag, 9);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
	
		//copy elements
		frag.add(Label, loopLabel);
		
		//new string
		frag.add(Duplicate);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old string
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Add);
		
		frag.add(LoadC);
		frag.add(StoreC);
		
		//loop check
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP);
		Macros.incrementInteger(frag, RunTime.STRING_INDEXING_INDEX_START);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Subtract);
		frag.add(JumpPos, loopLabel);
		
		return frag;
	}
	
	
}