package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.IdentifierNode;
import semanticAnalyzer.types.*;
import symbolTable.Binding;

public class ArrayIndexingCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("array-indexing");
		String label = labeller.newLabel("in-bounds");
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_ADDRESS);
		Macros.storeITo(frag, RunTime.ARRAY_INDEXING_INDEX);
		Macros.storeITo(frag, RunTime.ARRAY_INDEXING_ARRAY);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_ARRAY);
		frag.add(JumpFalse, RunTime.NULL_ARRAY_RUNTIME_ERROR);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_INDEX);
		frag.add(JumpNeg, RunTime.INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_INDEX);
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_ARRAY);
		frag.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Subtract);
		frag.add(JumpNeg, label);
		frag.add(Jump, RunTime.INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		frag.add(Label, label);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_ARRAY);
		frag.add(PushI, RunTime.RECORD_STATUS_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(PushI, 4);
		frag.add(BTAnd);
		frag.add(JumpTrue, RunTime.DELETED_RECORD_ACCESS_RUNTIME_ERROR);
		
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_ARRAY);
		frag.add(PushI, RunTime.ARRAY_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.ARRAY_INDEXING_INDEX);
		Array arrayType = (Array)(node.child(0).getType());
		Type subtype = arrayType.getSubtype();
		frag.add(PushI, subtype.getSize());
		frag.add(Multiply);
		frag.add(Add);
		return frag;
	}
	
	
}