package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;

public class StringCharConcatCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("string-char-concat");
		String loopLabel = labeller.newLabel("loop");
		String emptyLabel = labeller.newLabel("empty");
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_VALUE);				
		Macros.storeITo(frag, RunTime.CHAR_TEMP);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_STRING);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_END);
		
		//create new string record
		frag.add(PushI, 1);
		frag.add(Add);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.STRING_LENGTH_TEMPORARY);
		RunTime.CreateEmptyString(frag, 9);
		
		//new length
		frag.add(Duplicate);
		
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_LENGTH_TEMPORARY);
		frag.add(StoreI);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_START);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		frag.add(JumpFalse, emptyLabel);
		
		//copy first string
		frag.add(Label, loopLabel);
		
		//new string
		frag.add(Duplicate);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old string
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Add);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//loop check
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP);
		Macros.incrementInteger(frag, RunTime.STRING_INDEXING_INDEX_START);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_END);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Subtract);
		frag.add(JumpPos, loopLabel);
		
		
		frag.add(Label, emptyLabel);
		frag.add(Duplicate);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.CHAR_TEMP);
		frag.add(StoreC);
		
		

		return frag;
	}
	
	
}