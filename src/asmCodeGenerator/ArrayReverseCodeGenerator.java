package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.MemoryManager;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;

public class ArrayReverseCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("array-reverse");
		String loopLabel = labeller.newLabel("loop");
		String loopLabel2 = labeller.newLabel("loop2");
		String continueLabel = labeller.newLabel("continue");
		String doneLabel = labeller.newLabel("done");
		String byteCopyLabel = labeller.newLabel("byte-copy");
		
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		Macros.storeITo(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		
		//subtype size
		frag.add(PushI, RunTime.ARRAY_SUBTYPE_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.ARRAY_SUBTYPE_SIZE);
		
		//calculate total data size
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		frag.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Multiply);
		frag.add(Duplicate);
		Macros.storeITo(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		
		//allocate memory for data and header
		frag.add(PushI, RunTime.ARRAY_HEADER_SIZE);
		frag.add(Add);
		frag.add(Call, MemoryManager.MEM_MANAGER_ALLOCATE);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		
		//copy the header bytewise
		frag.add(Label, loopLabel);
		
		//new array
		frag.add(Duplicate);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old array
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//loop check
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP);
		frag.add(PushI, RunTime.ARRAY_HEADER_SIZE);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Subtract);
		frag.add(JumpFalse, continueLabel);
		frag.add(Jump, loopLabel);
		
		//if length is zero, skip copying the elements
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		frag.add(JumpFalse, doneLabel);
		
		//copy the elements in reverse order
		frag.add(Label, continueLabel);
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		
		//start at (length - 1) * subtypeSize, decrement until end of the header
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		Macros.loadIFrom(frag, RunTime.ARRAY_SUBTYPE_SIZE);
		frag.add(Subtract);
		Macros.storeITo(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		
		frag.add(Label, loopLabel2);
		
		//new array
		frag.add(Duplicate);
		frag.add(PushI, RunTime.ARRAY_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		frag.add(Add);
		
		//old array
		Macros.loadIFrom(frag, RunTime.ARRAY_ADDRESS_ORIGINAL);
		frag.add(PushI, RunTime.ARRAY_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		frag.add(Add);
		
		//store the element addresses
		Macros.storeITo(frag, RunTime.ARRAY_ELEMENT_LOC_ORIGINAL);
		Macros.storeITo(frag, RunTime.ARRAY_ELEMENT_LOC_NEW);
		
		frag.add(PushI, 0);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP2);
		
		//bytewise copy
		frag.add(Label, byteCopyLabel);
		Macros.loadIFrom(frag, RunTime.ARRAY_ELEMENT_LOC_NEW);
		Macros.loadIFrom(frag, RunTime.ARRAY_ELEMENT_LOC_ORIGINAL);
		frag.add(LoadC);
		frag.add(StoreC);
		
		//increment
		Macros.incrementInteger(frag, RunTime.INTEGER_TEMP2);
		Macros.incrementInteger(frag, RunTime.ARRAY_ELEMENT_LOC_ORIGINAL);
		Macros.incrementInteger(frag, RunTime.ARRAY_ELEMENT_LOC_NEW);
		
		//loop check
		Macros.loadIFrom(frag, RunTime.ARRAY_SUBTYPE_SIZE);
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP2);
		frag.add(Subtract);
		frag.add(JumpPos, byteCopyLabel);
		
		
		//increment new array counter by subtype size
		Macros.loadIFrom(frag, RunTime.INTEGER_TEMP);
		Macros.loadIFrom(frag, RunTime.ARRAY_SUBTYPE_SIZE);
		frag.add(Add);
		Macros.storeITo(frag, RunTime.INTEGER_TEMP);
		
		//decrement old array counter by subtype size
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		Macros.loadIFrom(frag, RunTime.ARRAY_SUBTYPE_SIZE);
		frag.add(Subtract);
		Macros.storeITo(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		
		//loop check
		Macros.loadIFrom(frag, RunTime.ARRAY_DATASIZE_TEMPORARY);
		frag.add(JumpNeg, doneLabel);
		frag.add(Jump, loopLabel2);
		
		frag.add(Label, doneLabel);
		return frag;
	}
	
	
}