package asmCodeGenerator;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;
import parseTree.ParseNode;

public class StringIndexingCodeGenerator implements SimpleCodeGenerator {

	@Override
	public ASMCodeFragment generate(ParseNode node) {
		Labeller labeller = new Labeller("string-indexing");
		String label = labeller.newLabel("in-bounds");
		ASMCodeFragment frag = new ASMCodeFragment(CodeType.GENERATES_ADDRESS);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_INDEX_START);
		Macros.storeITo(frag, RunTime.STRING_INDEXING_STRING);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		frag.add(JumpNeg, RunTime.INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_INDEX_START);
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(Subtract);
		frag.add(JumpNeg, label);
		frag.add(Jump, RunTime.INDEX_OUT_OF_BOUNDS_RUNTIME_ERROR);
		frag.add(Label, label);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.RECORD_STATUS_OFFSET);
		frag.add(Add);
		frag.add(LoadI);
		frag.add(PushI, 4);
		frag.add(BTAnd);
		frag.add(JumpTrue, RunTime.DELETED_RECORD_ACCESS_RUNTIME_ERROR);
		
		Macros.loadIFrom(frag, RunTime.STRING_INDEXING_STRING);
		frag.add(PushI, RunTime.STRING_HEADER_SIZE);
		frag.add(Add);
		Macros.loadIFrom(frag,RunTime.STRING_INDEXING_INDEX_START);
		frag.add(Add);
		return frag;
	}
	
	
}