package asmCodeGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMOpcode;
import asmCodeGenerator.runtime.MemoryManager;
import asmCodeGenerator.runtime.RunTime;
import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Lextant;
import lexicalAnalyzer.Punctuator;
import parseTree.*;
import parseTree.nodeTypes.CreateEmptyArrayNode;
import parseTree.nodeTypes.ArrayNode;
import parseTree.nodeTypes.AssignmentNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallNode;
import parseTree.nodeTypes.CharConstantNode;
import parseTree.nodeTypes.ConditionalNode;
import parseTree.nodeTypes.BlockNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ExpressionListNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.JumpNode;
import parseTree.nodeTypes.LambdaNode;
import parseTree.nodeTypes.MapNode;
import parseTree.nodeTypes.FloatingConstantNode;
import parseTree.nodeTypes.FoldNode;
import parseTree.nodeTypes.ForNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.FunctionNode;
import parseTree.nodeTypes.NewlineNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProcedureBlockNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReduceNode;
import parseTree.nodeTypes.ReleaseArrayNode;
import parseTree.nodeTypes.ReturnNode;
import parseTree.nodeTypes.SpaceNode;
import parseTree.nodeTypes.StringConstantNode;
import parseTree.nodeTypes.TabNode;
import parseTree.nodeTypes.TrinaryOperatorNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.ZipNode;
import semanticAnalyzer.types.Array;
import semanticAnalyzer.types.Lambda;
import semanticAnalyzer.types.PrimitiveType;
import semanticAnalyzer.types.Type;
import symbolTable.Binding;
import symbolTable.Scope;
import static asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType.*;
import static asmCodeGenerator.codeStorage.ASMOpcode.*;

// do not call the code generator if any errors have occurred during analysis.
public class ASMCodeGenerator {
	ParseNode root;
	ArrayList<FunctionNode> functions = new ArrayList<FunctionNode>();

	public static ASMCodeFragment generate(ParseNode syntaxTree) {
		ASMCodeGenerator codeGenerator = new ASMCodeGenerator(syntaxTree);
		return codeGenerator.makeASM();
	}
	public ASMCodeGenerator(ParseNode root) {
		super();
		this.root = root;
	}
	
	public ASMCodeFragment makeASM() {
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
		code.append(MemoryManager.codeForInitialization());
		code.append( RunTime.getEnvironment() );
		code.append( globalVariableBlockASM() );
		code.append( programASM() );
		code.append( MemoryManager.codeForAfterApplication() );
		
		return code;
	}
	private ASMCodeFragment globalVariableBlockASM() {
		assert root.hasScope();
		Scope scope = root.getScope();
		int globalBlockSize = scope.getAllocatedSize();
		
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
		code.add(DLabel, RunTime.GLOBAL_MEMORY_BLOCK);
		code.add(DataZ, globalBlockSize);
		return code;
	}
	private ASMCodeFragment programASM() {
		ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
		code.add(    Label, RunTime.MAIN_PROGRAM_LABEL);
		code.append( programCode());
		
		return code;
	}
	
	private ASMCodeFragment programCode() {
		CodeVisitor visitor = new CodeVisitor();
		root.accept(visitor);
		return visitor.removeRootCode(root);
	}


	protected class CodeVisitor extends ParseNodeVisitor.Default {
		private Map<ParseNode, ASMCodeFragment> codeMap;
		ASMCodeFragment code;
		
		public CodeVisitor() {
			codeMap = new HashMap<ParseNode, ASMCodeFragment>();
		}


		////////////////////////////////////////////////////////////////////
        // Make the field "code" refer to a new fragment of different sorts.
		private void newAddressCode(ParseNode node) {
			code = new ASMCodeFragment(GENERATES_ADDRESS);
			codeMap.put(node, code);
		}
		private void newValueCode(ParseNode node) {
			code = new ASMCodeFragment(GENERATES_VALUE);
			codeMap.put(node, code);
		}
		private void newVoidCode(ParseNode node) {
			code = new ASMCodeFragment(GENERATES_VOID);
			codeMap.put(node, code);
		}

	    ////////////////////////////////////////////////////////////////////
        // Get code from the map.
		private ASMCodeFragment getAndRemoveCode(ParseNode node) {
			ASMCodeFragment result = codeMap.get(node);
			codeMap.remove(result);
			return result;
		}
	    public  ASMCodeFragment removeRootCode(ParseNode tree) {
			return getAndRemoveCode(tree);
		}		
		ASMCodeFragment removeValueCode(ParseNode node) {
			ASMCodeFragment frag = getAndRemoveCode(node);
			makeFragmentValueCode(frag, node);
			return frag;
		}		
		private ASMCodeFragment removeAddressCode(ParseNode node) {
			ASMCodeFragment frag = getAndRemoveCode(node);
			assert frag.isAddress();
			return frag;
		}		
		ASMCodeFragment removeVoidCode(ParseNode node) {
			ASMCodeFragment frag = getAndRemoveCode(node);
			assert frag.isVoid();
			return frag;
		}
		
	    ////////////////////////////////////////////////////////////////////
        // convert code to value-generating code.
		private void makeFragmentValueCode(ASMCodeFragment code, ParseNode node) {
			assert !code.isVoid();
			
			if(code.isAddress()) {
				turnAddressIntoValue(code, node.getType());
			}	
		}
		private void turnAddressIntoValue(ASMCodeFragment code, Type type) {
			if(type == PrimitiveType.INTEGER) {
				code.add(LoadI);
			}	
			else if(type == PrimitiveType.FLOATING) {
				code.add(LoadF);
			}
			else if(type == PrimitiveType.CHAR) {
				code.add(LoadC);
			}
			else if(type == PrimitiveType.BOOLEAN) {
				code.add(LoadC);
			}	
			else if(type == PrimitiveType.STRING) {
				code.add(LoadI);
			}
			else if(type == PrimitiveType.RATIONAL) {
				code.add(Duplicate);
				code.add(PushI, 4);
				code.add(Add);
				code.add(Exchange);
				code.add(LoadI);
				code.add(Exchange);
				code.add(LoadI);
			} 
			else if(type instanceof Array) {
				code.add(LoadI);
			}
			else if(type instanceof Lambda) {
				code.add(LoadI);
			} 
			else if(type == PrimitiveType.VOID) {
				
			}
			else {
				assert false : "node " + type;
			}
			code.markAsValue();
		}
		
		private void opcodeForLoad(ASMCodeFragment code, Type type) {
			if(type == PrimitiveType.INTEGER) {
				code.add(LoadI);
			}	
			else if(type == PrimitiveType.FLOATING) {
				code.add(LoadF);
			}
			else if(type == PrimitiveType.CHAR) {
				code.add(LoadC);
			}
			else if(type == PrimitiveType.BOOLEAN) {
				code.add(LoadC);
			}	
			else if(type == PrimitiveType.STRING) {
				code.add(LoadI);
			}
			else if(type == PrimitiveType.RATIONAL) {
				code.add(Duplicate);
				code.add(PushI, 4);
				code.add(Add);
				code.add(Exchange);
				code.add(LoadI);
				code.add(Exchange);
				code.add(LoadI);
			} 
			else if(type instanceof Array) {
				code.add(LoadI);
			}
			else if(type instanceof Lambda) {
				code.add(LoadI);
			} 
			else if(type == PrimitiveType.VOID) {
				
			}
			else {
				assert false : "node " + type;
			}
		}
		
		//private void turnAddressIntoValue(ASMCodeFragment code, Type type) {
		
	    ////////////////////////////////////////////////////////////////////
        // ensures all types of ParseNode in given AST have at least a visitLeave	
		public void visitLeave(ParseNode node) {
			assert false : "node " + node + " not handled in ASMCodeGenerator";
		}
		
		
		
		///////////////////////////////////////////////////////////////////////////
		// constructs larger than statements
		public void visitLeave(ProgramNode node) {
			newVoidCode(node);
			code.append(storeFunctionDefinitions());
			List<ParseNode> children = node.getChildren();
			
			//add declarations
			for(int i = 0; i < children.size(); i++) {
				if(children.get(i) instanceof DeclarationNode) {
					ASMCodeFragment childCode = removeVoidCode(children.get(i));
					code.append(childCode);
				}
			}
			//add exec
			for(int i = 0; i < children.size(); i++) {
				if(children.get(i) instanceof BlockNode) {
					ASMCodeFragment childCode = removeVoidCode(children.get(i));
					code.append(childCode);
					code.add(Halt);
				}
			}
			
			for(int i = 0; i < children.size(); i++) {
				if(children.get(i) instanceof FunctionNode) {
					ASMCodeFragment childCode = removeVoidCode(children.get(i));
					code.append(childCode);
				}
			}
				
				
			
		}
		public void visitLeave(BlockNode node) {
			newVoidCode(node);
			for(ParseNode child : node.getChildren()) {
				ASMCodeFragment childCode = removeVoidCode(child);
				code.append(childCode);
			}
		}
		public void visitLeave(ProcedureBlockNode node) {
			newVoidCode(node);
			for(ParseNode child : node.getChildren()) {
				ASMCodeFragment childCode = removeVoidCode(child);
				code.append(childCode);
			}
		}
		
		
		///////////////////////////////////////////////////////////////////////////
		// Functions
		
		public void visitLeave(FunctionNode node) {
			newVoidCode(node);
			if(node.child(0) instanceof IdentifierNode) {
				code.append(removeValueCode(node.child(1)));
			
			
			//add function definitions as global variables
			functions.add(node);	
			}
		}
		
		private ASMCodeFragment storeFunctionDefinitions() {
			ASMCodeFragment code = new ASMCodeFragment(GENERATES_VOID);
			for(FunctionNode func : functions) {
				code.append(removeAddressCode(func.child(0)));
				String label = func.getFunctionLabel();
				code.add(PushD, label);
				code.add(StoreI);
			}
			return code;
		}
		
		public void visitLeave(CallNode node) {
			newVoidCode(node);
			code.append(removeValueCode(node.child(0)));
			Lambda lambda = (Lambda) node.child(0).child(0).getType();
			if(lambda.getReturnType() == PrimitiveType.VOID) {
				code.add(Pop);
			}
			//double pop if rational, single pop if any non-void type
			if(node.child(0).getType() == PrimitiveType.RATIONAL) {
				code.add(Pop);
				code.add(Pop);
			} else if(node.child(0).getType() != PrimitiveType.VOID){
				code.add(Pop);
			}
		}
		
		public void visitLeave(FunctionInvocationNode node) {
			newValueCode(node);

			// Move stack pointer and store for each parameter
			for(int i = 1; i < node.nChildren(); i++) {
				Type type = node.child(i).getType();
				Macros.loadIFrom(code, RunTime.STACK_POINTER);
				code.add(PushI, type.getSize());
				code.add(Subtract);
				Macros.storeITo(code, RunTime.STACK_POINTER);
				
				Macros.loadIFrom(code, RunTime.STACK_POINTER);
				code.append(removeValueCode(node.child(i)));
				opcodeForStore(code, type);
			}
			
			//call
			code.append(removeAddressCode(node.child(0)));
			code.add(LoadI);
			code.add(CallV);
			
			//grab return value
			Type returnType = node.getType();
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			turnAddressIntoValue(code, returnType);
			

			//move stack pointer back up
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, returnType.getSize());
			code.add(Add);
			Macros.storeITo(code, RunTime.STACK_POINTER);
		}
		
		public void visitLeave(LambdaNode node) {
			newValueCode(node);
			//store dynamic link, current frame pointer.
			FunctionNode fn = (FunctionNode) node.getParent();
			
			//skip over lambda implementation if inline
			if(fn.getParent() instanceof DeclarationNode || fn.getParent() instanceof AssignmentNode) {
				String label = fn.getFunctionLabel();
				code.add(PushD, label);
				code.add(Jump, fn.getEndLabel());
			}
			
			code.add(Label, fn.getFunctionLabel());
			
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, 4);
			code.add(Subtract);
			Macros.loadIFrom(code, RunTime.FRAME_POINTER);
			code.add(StoreI);
			
			//store return address, top of stack
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, 8);
			code.add(Subtract);
			code.add(Exchange);
			code.add(StoreI);
			
			//frame pointer = stack pointer
			code.add(PushD, RunTime.FRAME_POINTER);
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(StoreI);
			
			//stack pointer -= (dynamic link + return address + local variables) 
			int frameSize = 8;
			for(ParseNode p : node.child(2).getChildren()) {
				frameSize += p.getType().getSize();
			}
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, frameSize);
			code.add(Subtract);
			Macros.storeITo(code, RunTime.STACK_POINTER);

			//execute function body
			code.append(removeVoidCode(node.child(2)));
			
			//error if no return statement
			code.add(Jump, RunTime.NO_RETURN_STATEMENT_RUNTIME_ERROR);
			
			//handshake exit (return value is on top of stack)
			code.add(Label, node.getHandshakeLabel());
			//push return address (frame pointer - 8)
			Macros.loadIFrom(code, RunTime.FRAME_POINTER);
			code.add(PushI, 8);
			code.add(Subtract);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.RETURN_ADDRESS);
			//code.add(Exchange); // [returnaddress returnvalue] does not work for rats
			
			//replace frame pointer with dynamic link
			Macros.loadIFrom(code, RunTime.FRAME_POINTER);
			code.add(PushI, 4);
			code.add(Subtract);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.FRAME_POINTER);
			
			//stack pointer += frame + args
			int argSize = 0;
			for(ParseNode p : node.child(0).getChildren()) {
				argSize += p.getType().getSize();
			}
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, frameSize + argSize);
			code.add(Add);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//stack pointer -= return value size
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			int returnSize = node.child(1).getType().getSize();
			code.add(PushI, returnSize);
			code.add(Subtract);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//store return value
			if(node.child(1).getType() == PrimitiveType.RATIONAL) {
				Macros.storeITo(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
				Macros.loadIFrom(code, RunTime.STACK_POINTER);
				code.add(Exchange);
				Macros.loadIFrom(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
			} else {
				Macros.loadIFrom(code, RunTime.STACK_POINTER);
				code.add(Exchange);
			}
			
			opcodeForStore(code, node.child(1).getType());	
			Macros.loadIFrom(code, RunTime.RETURN_ADDRESS);
			code.add(Return);
			code.add(Label, fn.getEndLabel());
		}
		
		public void visitLeave(ReturnNode node) {
			newVoidCode(node);
			if(node.nChildren() != 0 && node.getType() != PrimitiveType.VOID) {
				code.append(removeValueCode(node.child(0)));
			} else {
				code.add(PushI, 0);
			}
			ParseNode p = node.getParent();
			while(!(p instanceof LambdaNode)) {
				p = p.getParent();
			}
			LambdaNode lambda = (LambdaNode)p;
			code.add(Jump, lambda.getHandshakeLabel());
		}
		

		///////////////////////////////////////////////////////////////////////////
		// statements and declarations
		
		

		public void visitLeave(PrintStatementNode node) {
			newVoidCode(node);
			new PrintStatementGenerator(code, this).generate(node);	
		}
		public void visit(NewlineNode node) {
			newVoidCode(node);
			code.add(PushD, RunTime.NEWLINE_PRINT_FORMAT);
			code.add(Printf);
		}
		public void visit(TabNode node) {
			newVoidCode(node);
			code.add(PushD, RunTime.TAB_PRINT_FORMAT);
			code.add(Printf);
		}
		public void visit(SpaceNode node) {
			newVoidCode(node);
			code.add(PushD, RunTime.SPACE_PRINT_FORMAT);
			code.add(Printf);
		}
		
		public void visitLeave(AssignmentNode node) {
			newVoidCode(node);
			ASMCodeFragment lvalue = removeAddressCode(node.child(0));	
			ASMCodeFragment rvalue;
			if(node.child(1) instanceof FunctionNode) {
				rvalue = removeValueCode(node.child(1).child(0));
			} else {
				rvalue = removeValueCode(node.child(1));
			}
			Type type = node.getType();
			
			code.append(lvalue);			
			code.append(rvalue);			
			opcodeForStore(code, type);			
		}

		public void visitLeave(DeclarationNode node) {
			newVoidCode(node);
			ASMCodeFragment lvalue = removeAddressCode(node.child(0));	
			ASMCodeFragment rvalue;
			if(node.child(1) instanceof FunctionNode) {
				rvalue = removeValueCode(node.child(1).child(0));
			} else {
				rvalue = removeValueCode(node.child(1));
			}
				
			Type type = node.getType();
			
			code.append(lvalue);
			code.append(rvalue);
			opcodeForStore(code, type);
		}
		private void opcodeForStore(ASMCodeFragment code, Type type) {
			if(type == PrimitiveType.INTEGER) {
				code.add(StoreI);
			}
			else if(type == PrimitiveType.FLOATING) {
				code.add(StoreF);
			}
			else if(type == PrimitiveType.BOOLEAN) {
				code.add(StoreC);
			}
			else if(type == PrimitiveType.CHAR) {
				code.add(StoreC);
			}
			else if(type == PrimitiveType.STRING) {
				code.add(StoreI);
			}
			else if(type == PrimitiveType.RATIONAL) {
				Macros.storeITo(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
				Macros.storeITo(code, RunTime.RATIONAL_NUMERATOR_TEMP);
				code.add(Duplicate);
				code.add(PushI, 4);
				code.add(Add);
				Macros.loadIFrom(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
				code.add(StoreI);
				Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
				code.add(StoreI);

			}
			else if(type == PrimitiveType.VOID) {
				code.add(StoreI);
			}
			else if (type instanceof Array) {
				code.add(StoreI);
			}
			else if(type instanceof Lambda) {
				code.add(StoreI);
			}
			else {
				assert false: "Type " + type + " unimplemented in opcodeForStore()";
			}
			
		}
		
		public void visitLeave(ConditionalNode node) {
			newVoidCode(node);
			ASMCodeFragment condition = removeValueCode(node.child(0));
			ASMCodeFragment ifBlock = removeVoidCode(node.child(1));
			ASMCodeFragment elseBlock = removeVoidCode(node.child(2));

			Labeller labeller = new Labeller("condition");
			
			String trueLabel = labeller.newLabel("true");
			String falseLabel = labeller.newLabel("false");
			
			code.add(Label, node.getContinueLabel());
			code.append(condition);
			code.add(JumpTrue, trueLabel);
			code.add(Jump, falseLabel);
			
			code.add(Label, trueLabel);
			code.append(ifBlock);
			if(node.getToken().isLextant(Keyword.WHILE)) {
				code.add(Jump, node.getContinueLabel());
			}
			code.add(Jump, node.getBreakLabel());
			
			code.add(Label, falseLabel);
			code.append(elseBlock);
			code.add(Jump, node.getBreakLabel());
			
			code.add(Label, node.getBreakLabel());
		}
		
		//for loops
		public void visitLeave(ForNode node) {
			newVoidCode(node);
			if(node.getToken().isLextant(Keyword.INDEX)) {
				indexLoop(node);
			} else {
				elemLoop(node);
			}
		}

		public void indexLoop(ForNode node) {
			//put loop info on stack for nested loops
			Macros.loadIFrom(code, RunTime.LOOP_END_INDEX);
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			Macros.loadIFrom(code, RunTime.LOOP_SEQUENCE);
			
			//initialize sequence
			code.append(removeValueCode(node.child(0)));
			Macros.storeITo(code, RunTime.LOOP_SEQUENCE);
			
			//initialize ending index
			Macros.loadIFrom(code, RunTime.LOOP_SEQUENCE);
			if(node.child(0).getType() instanceof Array) {
				code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			} else {
				code.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
			}
			code.add(Add);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.LOOP_END_INDEX);
			
			//initialize identifier
			code.append(removeAddressCode(node.child(1)));
			Macros.storeITo(code, RunTime.LOOP_IDENTIFIER);
			
			//start at 0
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			code.add(PushI, 0);
			code.add(StoreI);
			
			//loop
			code.add(Label, node.getLoopLabel());
			Macros.loadIFrom(code, RunTime.LOOP_END_INDEX);
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			code.add(LoadI);
			code.add(Subtract);
			code.add(JumpFalse, node.getEndLabel());
			
			//body
			code.append(removeVoidCode(node.child(2)));
			
			//increment
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			code.add(LoadI);
			code.add(PushI, 1);
			code.add(Add);
			code.add(StoreI);
			code.add(Jump, node.getLoopLabel());
			
			code.add(Label, node.getEndLabel());
			
			//put loop info back
			Macros.storeITo(code, RunTime.LOOP_SEQUENCE);
			Macros.storeITo(code, RunTime.LOOP_IDENTIFIER);
			Macros.storeITo(code, RunTime.LOOP_END_INDEX);
			
		}
		
		public void elemLoop(ForNode node) {
			//put loop info on stack for nested loops
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.loadIFrom(code, RunTime.LOOP_END_INDEX);
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			Macros.loadIFrom(code, RunTime.LOOP_SEQUENCE);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_ELEMENT);
			
			//initialize sequence
			code.append(removeValueCode(node.child(0)));
			Macros.storeITo(code, RunTime.LOOP_SEQUENCE);
			
			//initialize ending index
			Macros.loadIFrom(code, RunTime.LOOP_SEQUENCE);
			if(node.child(0).getType() instanceof Array) {
				code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			} else {
				code.add(PushI, RunTime.STRING_RECORD_LENGTH_OFFSET);
			}
			code.add(Add);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.LOOP_END_INDEX);
			
			//initialize index
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			
			//initialize identifier
			code.append(removeAddressCode(node.child(1)));
			Macros.storeITo(code, RunTime.LOOP_IDENTIFIER);
			
			//get type
			Type type = null;
			if(node.child(0).getType() instanceof Array) {
				Array arr = (Array) node.child(0).getType();
				type = arr.getSubtype();
			} else {
				type = PrimitiveType.CHAR;
			}
			
			//initialize element
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			Macros.loadIFrom(code, RunTime.LOOP_SEQUENCE);
			if(node.child(0).getType() instanceof Array) {
				code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			} else {
				code.add(PushI, RunTime.STRING_HEADER_SIZE);
			}
			code.add(Add);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_ELEMENT);
			opcodeForLoad(code, type);
			code.add(StoreI);
			
			//loop
			code.add(Label, node.getLoopLabel());
			Macros.loadIFrom(code, RunTime.LOOP_END_INDEX);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Subtract);
			code.add(JumpFalse, node.getEndLabel());
			
			//body
			code.append(removeVoidCode(node.child(2)));
			
			//increment
			code.add(Label, node.getContinueLabel());
			Macros.loadIFrom(code, RunTime.LOOP_IDENTIFIER);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_ELEMENT);
			code.add(PushI, type.getSize());
			code.add(Add);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_ELEMENT);
			code.add(LoadI);
			code.add(StoreI);
			
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, 1);
			code.add(Add);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Jump, node.getLoopLabel());
			
			code.add(Label, node.getEndLabel());
			
			//put loop info back
			Macros.storeITo(code, RunTime.LOOP_CURRENT_ELEMENT);
			Macros.storeITo(code, RunTime.LOOP_SEQUENCE);
			Macros.storeITo(code, RunTime.LOOP_IDENTIFIER);
			Macros.storeITo(code, RunTime.LOOP_END_INDEX);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
		}

		///////////////////////////////////////////////////////////////////////////
		// expressions
		public void visitLeave(BinaryOperatorNode node) {
			Lextant operator = node.getOperator();

			if(operator == Punctuator.GREATER || operator == Punctuator.LESS || operator == Punctuator.GREATER_EQ ||
			   operator == Punctuator.LESS_EQ || operator == Punctuator.EQUAL || operator == Punctuator.NOT_EQUAL) {
				visitComparisonOperatorNode(node, operator);
			}
			else {
				try {
					visitNormalBinaryOperatorNode(node);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}


		public void visitLeave(UnaryOperatorNode node){
			try {
				visitNormalUnaryOperatorNode(node);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void visitLeave(TrinaryOperatorNode node){
			try {
				visitNormalTrinaryOperatorNode(node);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		public void visitLeave(ExpressionListNode node) {
			newValueCode(node);
			if(node.nChildren() == 0) {
				code.add(PushI, 0);
				RunTime.createEmptyArrayRecord(code, 0, 0);
				return;
			}
			int size = node.child(0).getType().getSize();
			code.add(PushI, node.nChildren());
			if(node.child(0).getType() instanceof Array || node.child(0).getType() == PrimitiveType.STRING) {
				RunTime.createEmptyArrayRecord(code, 2, size); // status: 0010
			} else {
				RunTime.createEmptyArrayRecord(code, 0, size); // status: 0000
			}
			
			
			for(int i = 0; i < node.nChildren(); i++) {
				ASMCodeFragment arg = removeValueCode(node.child(i));
				code.add(Duplicate);
				code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
				code.add(Add);
				code.add(PushI, i * size);
				code.add(Add);
				code.append(arg);
				if(size == 1) {
					code.add(StoreC);
				} else if(size == 4) {
					code.add(StoreI);
				} else if(size == 8) {
					if(node.child(i).getType() == PrimitiveType.RATIONAL) {
						Macros.storeITo(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
						Macros.storeITo(code, RunTime.RATIONAL_NUMERATOR_TEMP);
						
						code.add(Duplicate);
						code.add(PushI, 4);
						code.add(Add);
						Macros.loadIFrom(code, RunTime.RATIONAL_DENOMINATOR_TEMP);
						code.add(StoreI);
						
						Macros.loadIFrom(code, RunTime.RATIONAL_NUMERATOR_TEMP);
						code.add(StoreI);
						
					} else {
						code.add(StoreF);
					}
				}
			}
		}
		
		public void visitLeave(CreateEmptyArrayNode node){
			newValueCode(node);
			if(node.child(1) instanceof IntegerConstantNode) {
				IntegerConstantNode n = (IntegerConstantNode) node.child(1);
				code.add(PushI, n.getValue());
			} else {
				IdentifierNode n = (IdentifierNode)node.child(1);
				Binding b = n.getBinding();
				
				b.generateAddress(code);
				code.add(LoadI);
			}
			ParseNode p = node.child(0);
			Array arr = (Array) p.getType();
			if(arr.getSubtype() instanceof Array || arr.getSubtype() == PrimitiveType.STRING) {
				RunTime.createEmptyArrayRecord(code, 2, arr.getSubtype().getSize()); // status: 0010
			} else {
				RunTime.createEmptyArrayRecord(code, 0, arr.getSubtype().getSize()); // status: 0000
			}
		}
		
		public void visitLeave(ReleaseArrayNode node) {
			newVoidCode(node);
			ASMCodeFragment arg = removeValueCode(node.child(0));
			code.append(arg);
			RunTime.deallocateRecord(code);
			
			
		}
		
		private void visitComparisonOperatorNode(BinaryOperatorNode node,
				Lextant operator) {

			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			ASMCodeFragment arg2 = removeValueCode(node.child(1));
			
			Labeller labeller = new Labeller("compare");
			
			String startLabel = labeller.newLabel("arg1");
			String arg2Label  = labeller.newLabel("arg2");
			String subLabel   = labeller.newLabel("sub");
			String trueLabel  = labeller.newLabel("true");
			String falseLabel = labeller.newLabel("false");
			String joinLabel  = labeller.newLabel("join");
			
			newValueCode(node);
			code.add(Label, startLabel);
			code.append(arg1);
			code.add(Label, arg2Label);
			code.append(arg2);
			code.add(Label, subLabel);
			
			if(node.child(0).getType() == PrimitiveType.FLOATING) {
				code.add(FSubtract);
				if(operator == Punctuator.GREATER) {
					code.add(JumpFPos, trueLabel);
					code.add(Jump, falseLabel);
				} else if(operator == Punctuator.GREATER_EQ) {
					code.add(JumpFNeg, falseLabel);
					code.add(Jump, trueLabel);
				} else if(operator == Punctuator.LESS) {
					code.add(JumpFNeg, trueLabel);
					code.add(Jump, falseLabel);
				} else if(operator == Punctuator.LESS_EQ) {
					code.add(JumpFPos, falseLabel);
					code.add(Jump, trueLabel);
				} else if(operator == Punctuator.EQUAL) {
					code.add(JumpFZero, trueLabel);
					code.add(Jump, falseLabel);
				} else if(operator == Punctuator.NOT_EQUAL) {
					code.add(JumpFZero, falseLabel);
					code.add(Jump, trueLabel);
				}
				
			} else {
				if(node.child(0).getType() == PrimitiveType.RATIONAL) {
					code.append(new RationalSubtractCodeGenerator().generate(node));
					code.add(Pop);
				} else {
					code.add(Subtract);
				}
				
				if(operator == Punctuator.GREATER) {
					code.add(JumpPos, trueLabel);
					code.add(Jump, falseLabel);
				} else if(operator == Punctuator.GREATER_EQ) {
					code.add(JumpNeg, falseLabel);
					code.add(Jump, trueLabel);
				} else if(operator == Punctuator.LESS) {
					code.add(JumpNeg, trueLabel);
					code.add(Jump, falseLabel);
				} else if(operator == Punctuator.LESS_EQ) {
					code.add(JumpPos, falseLabel);
					code.add(Jump, trueLabel);
				} else if(operator == Punctuator.EQUAL) {
					code.add(JumpFalse, trueLabel);
					code.add(Jump, falseLabel);
				} else if(operator == Punctuator.NOT_EQUAL) {
					code.add(JumpFalse, falseLabel);
					code.add(Jump, trueLabel);
				}
			} 
			
			code.add(Label, trueLabel);
			code.add(PushI, 1);
			code.add(Jump, joinLabel);
			code.add(Label, falseLabel);
			code.add(PushI, 0);
			code.add(Jump, joinLabel);
			code.add(Label, joinLabel);

		}
		private void visitNormalBinaryOperatorNode(BinaryOperatorNode node) throws Exception {
			
			newValueCode(node);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			ASMCodeFragment arg2 = null;
			if(node.child(1) instanceof ArrayNode) {
				arg2 = removeValueCode(node.child(0));
			} else {
				arg2 = removeValueCode(node.child(1));
			}
			
			Object variant = node.getSignature().getVariant();
			if(variant instanceof ASMOpcode) {
				code.append(arg1);
				code.append(arg2);
				ASMOpcode opcode = (ASMOpcode)variant;
				code.add(opcode);
			} else if (variant instanceof SimpleCodeGenerator) {
				code.append(arg1);
				code.append(arg2);
				SimpleCodeGenerator generator = (SimpleCodeGenerator)variant;
				ASMCodeFragment fragment = generator.generate(node);
				code.append(fragment);
				if(fragment.isAddress()) {
					code.markAsAddress();
				}
			} else if(variant instanceof FullCodeGenerator) {
				FullCodeGenerator generator = (FullCodeGenerator)variant;
				ASMCodeFragment fragment = generator.generate(node, arg1, arg2);
				code.append(fragment);
				if(fragment.isAddress()) {
					code.markAsAddress();
				}
			} else {
				throw new Exception("Node is not a valid ASMOpcode");
			}
			
		}
		
		private void visitNormalUnaryOperatorNode(UnaryOperatorNode node) throws Exception {
			newValueCode(node);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			
			Object variant = node.getSignature().getVariant();
			if(variant instanceof ASMOpcode) {
				code.append(arg1);
				ASMOpcode opcode = (ASMOpcode)variant;
				code.add(opcode);
			} else if (variant instanceof SimpleCodeGenerator) {
				code.append(arg1);
				SimpleCodeGenerator generator = (SimpleCodeGenerator)variant;
				ASMCodeFragment fragment = generator.generate(node);
				code.append(fragment);
				if(fragment.isAddress()) {
					code.markAsAddress();
				}
			} else {
				throw new Exception("Node is not a valid ASMOpcode");
			}
		}
		
		private void visitNormalTrinaryOperatorNode(TrinaryOperatorNode node) throws Exception {
			newValueCode(node);
			ASMCodeFragment arg1 = removeValueCode(node.child(0));
			ASMCodeFragment arg2 = removeValueCode(node.child(1));
			ASMCodeFragment arg3 = removeValueCode(node.child(2));
			
			Object variant = node.getSignature().getVariant();
			if(variant instanceof ASMOpcode) {
				code.append(arg1);
				code.append(arg2);
				code.append(arg3);
				ASMOpcode opcode = (ASMOpcode)variant;
				code.add(opcode);
			} else if (variant instanceof SimpleCodeGenerator) {
				code.append(arg1);
				code.append(arg2);
				code.append(arg3);
				SimpleCodeGenerator generator = (SimpleCodeGenerator)variant;
				ASMCodeFragment fragment = generator.generate(node);
				code.append(fragment);
				if(fragment.isAddress()) {
					code.markAsAddress();
				}
			} else {
				throw new Exception("Node is not a valid ASMOpcode");
			}
		}
		
		///////////////////////////////////////////////////////////////////////////
		// higher order functions
		public void visitLeave(MapNode node) {
			Labeller labeller = new Labeller("map");
			String endLabel = labeller.newLabel("end");
			String loopLabel = labeller.newLabel("loop");
			newValueCode(node);
			Array arr = (Array) node.getType();
			Type subtype = arr.getSubtype();
			Array originalArray = (Array) node.child(0).getType();
			Type originalSubtype = originalArray.getSubtype();
			
			//store old array
			code.append(removeValueCode(node.child(0)));
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			
			//create new array
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			if(subtype instanceof Array || subtype == PrimitiveType.STRING) {
				RunTime.createEmptyArrayRecord(code, 2, subtype.getSize()); // status: 0010
			} else {
				RunTime.createEmptyArrayRecord(code, 0, subtype.getSize()); // status: 0000
			}
			
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_COPY);
			
			//iterator
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			
			//loop
			code.add(Label, loopLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Subtract);
			code.add(JumpFalse, endLabel);
			
			//move stack pointer down
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, originalSubtype.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//put parameter on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, originalSubtype.getSize());
			code.add(Multiply);
			code.add(Add);
			opcodeForLoad(code, originalSubtype);
			opcodeForStore(code, originalSubtype);
			
			//put variables on stack
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
			
			//call function
			code.append(removeAddressCode(node.child(1)));
			code.add(LoadI);
			code.add(CallV);
			
			//put variables back
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_COPY);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			//put array location on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, subtype.getSize());
			code.add(Multiply);
			code.add(Add);
			
			//store return value to it
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			turnAddressIntoValue(code, subtype);
			opcodeForStore(code, subtype);
			
			//move stack pointer back up
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, subtype.getSize());
			code.add(Add);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//increment
			Macros.incrementInteger(code, RunTime.LOOP_CURRENT_INDEX);
			
			code.add(Jump, loopLabel);
			code.add(Label, endLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);

		}
		
		public void visitLeave(ZipNode node) {
			Labeller labeller = new Labeller("zip");
			String endLabel = labeller.newLabel("end");
			String loopLabel = labeller.newLabel("loop");
			String lengthsMatchLabel = labeller.newLabel("lengths-match");
			newValueCode(node);
			Array arr = (Array) node.getType();
			Type subtype = arr.getSubtype();
			Array originalArray = (Array) node.child(0).getType();
			Type originalSubtype = originalArray.getSubtype();
			Array originalArray2 = (Array) node.child(1).getType();
			Type originalSubtype2 = originalArray2.getSubtype();
			
			//store old arrays
			code.append(removeValueCode(node.child(0)));
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			
			code.append(removeValueCode(node.child(1)));
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL2);
			
			//check if lengths match
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL2);
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			code.add(Subtract);
			code.add(JumpFalse, lengthsMatchLabel);
			code.add(Jump, RunTime.ARRAYS_DIFFERENT_LENGTH_RUNTIME_ERROR);
			code.add(Label, lengthsMatchLabel);
			
			//create new array
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			if(subtype instanceof Array || subtype == PrimitiveType.STRING) {
				RunTime.createEmptyArrayRecord(code, 2, subtype.getSize()); // status: 0010
			} else {
				RunTime.createEmptyArrayRecord(code, 0, subtype.getSize()); // status: 0000
			}
			
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_COPY);
			//iterator
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			
			//loop
			code.add(Label, loopLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Subtract);
			code.add(JumpFalse, endLabel);
			
			//move stack pointer down
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, originalSubtype.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
					
			//put parameter on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, originalSubtype.getSize());
			code.add(Multiply);
			code.add(Add);
			opcodeForLoad(code, originalSubtype);
			opcodeForStore(code, originalSubtype);

			//do same for other parameter
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, originalSubtype2.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
					
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL2);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, originalSubtype2.getSize());
			code.add(Multiply);
			code.add(Add);
			opcodeForLoad(code, originalSubtype2);
			opcodeForStore(code, originalSubtype2);
			
			//put variables on stack
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL2);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
			
			//call function
			code.append(removeAddressCode(node.child(2)));
			code.add(LoadI);
			code.add(CallV);
			
			//put variables back
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_COPY);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL2);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			//put array location on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, subtype.getSize());
			code.add(Multiply);
			code.add(Add);
			
			//store return value to it
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			turnAddressIntoValue(code, subtype);
			opcodeForStore(code, subtype);
			
			//move stack pointer back up
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, subtype.getSize());
			code.add(Add);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//increment
			Macros.incrementInteger(code, RunTime.LOOP_CURRENT_INDEX);
			
			code.add(Jump, loopLabel);
			code.add(Label, endLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
		}
		
		public void visitLeave(ReduceNode node) {
			Labeller labeller = new Labeller("reduce");
			String endLabel = labeller.newLabel("end");
			String loopLabel = labeller.newLabel("loop");
			String falseLabel = labeller.newLabel("false");
			
			String loopLabel2 = labeller.newLabel("loop2");
			String endLabel2 = labeller.newLabel("end2");
			
			newValueCode(node);
			Array arr = (Array) node.getType();
			Type subtype = arr.getSubtype();
			
			//store old array
			code.append(removeValueCode(node.child(0)));
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			
			//get length
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			//iterator
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			
			//true element counter
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.INTEGER_TEMP2);
			
			//loop
			code.add(Label, loopLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Subtract);
			code.add(JumpFalse, endLabel);
			
			//move stack pointer down
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, subtype.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//put parameter on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, subtype.getSize());
			code.add(Multiply);
			code.add(Add);
			opcodeForLoad(code, subtype);
			code.add(Duplicate); //a copy will remain on stack, to be popped if func returns false
			Macros.storeITo(code, RunTime.LOOP_CURRENT_ELEMENT);
			opcodeForStore(code, subtype);
			
			//put variables on stack
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP2);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_ELEMENT);
			
			//call function
			code.append(removeAddressCode(node.child(1)));
			code.add(LoadI);
			code.add(CallV);
			
			//put variables back
			Macros.storeITo(code, RunTime.LOOP_CURRENT_ELEMENT);
			Macros.storeITo(code, RunTime.INTEGER_TEMP2);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			//keep array element on stack if function returned true, pop otherwise
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(LoadC); //will be boolean
			code.add(JumpFalse, falseLabel);
			Macros.incrementInteger(code, RunTime.INTEGER_TEMP2);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_ELEMENT);
			
			code.add(Label, falseLabel);
			
			//move stack pointer back
			Macros.incrementInteger(code, RunTime.STACK_POINTER);
			
			//increment
			Macros.incrementInteger(code, RunTime.LOOP_CURRENT_INDEX);
			
			code.add(Jump, loopLabel);
			code.add(Label, endLabel);
			
			//create new array
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP2);			
			
			if(subtype instanceof Array || subtype == PrimitiveType.STRING) {
				RunTime.createEmptyArrayRecord(code, 2, subtype.getSize()); // status: 0010
			} else {
				RunTime.createEmptyArrayRecord(code, 0, subtype.getSize()); // status: 0000
			}
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_COPY);
			
			//put elements on in reverse order, starting from length - 1
			Macros.decrementInteger(code, RunTime.INTEGER_TEMP2);
			code.add(Label, loopLabel2);
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP2);
			code.add(JumpNeg, endLabel2);
			
			//load array element location
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.INTEGER_TEMP2);
			code.add(PushI, subtype.getSize());
			code.add(Multiply);
			code.add(Add);
			
			//put element on
			code.add(Exchange);
			opcodeForStore(code, subtype);
			
			//decrement
			Macros.decrementInteger(code, RunTime.INTEGER_TEMP2);
			code.add(Jump, loopLabel2);
			
			code.add(Label, endLabel2);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_COPY);
		}
		
		public void visitLeave(FoldNode node) {
			if(node.nChildren() == 2) {
				foldNoBase(node);
			} else {
				foldWithBase(node);
			}
		}
		
		public void foldNoBase(FoldNode node) {
			Labeller labeller = new Labeller("fold");
			String endLabel = labeller.newLabel("end");
			String loopLabel = labeller.newLabel("loop");
			newValueCode(node);			
			Type type = node.getType();
			
			code.append(removeValueCode(node.child(0)));
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			
			//get length
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			//error if length is 0
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			code.add(JumpFalse, RunTime.FOLD_EMPTY_ARRAY_RUNTIME_ERROR);
			
			//load first element
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			turnAddressIntoValue(code, type);
			
			//return element if it's the only one
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			code.add(PushI, 1);
			code.add(Subtract);
			code.add(JumpFalse, endLabel);
			
			//set element counter
			code.add(PushI, 1);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
			
			//loop
			code.add(Label, loopLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Subtract);
			code.add(JumpFalse, endLabel);
			
			//move stack pointer down
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, type.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//put first arg on stack pointer
			code.add(Exchange);
			opcodeForStore(code, type);
			
			//move stack pointer down again
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, type.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//put second arg on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, type.getSize());
			code.add(Multiply);
			code.add(Add);
			opcodeForLoad(code, type);
			opcodeForStore(code, type);
			
			//put variables on stack
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);

			//call function
			code.append(removeAddressCode(node.child(1)));
			code.add(LoadI);
			code.add(CallV);
			
			//put variables back
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);

			//get return value
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			turnAddressIntoValue(code, type);
			
			//move stack pointer back up
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, type.getSize());
			code.add(Add);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//increment
			Macros.incrementInteger(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Jump, loopLabel);
			
			code.add(Label, endLabel);
		}
		
		public void foldWithBase(FoldNode node) {
			Labeller labeller = new Labeller("fold-base");
			String endLabel = labeller.newLabel("end");
			String loopLabel = labeller.newLabel("loop");
			newValueCode(node);			
			Type type = node.getType();
			Array arr = (Array)node.child(0).getType();
			Type subType = arr.getSubtype();
			
			//load base
			code.append(removeValueCode(node.child(2)));
			
			code.append(removeValueCode(node.child(0)));
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			
			
			
			//get length
			code.add(PushI, RunTime.ARRAY_RECORD_LENGTH_OFFSET);
			code.add(Add);
			code.add(LoadI);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			
			//done if length is 0
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			code.add(JumpFalse, endLabel);
			
			//set counter
			code.add(PushI, 0);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);
						
			//loop
			code.add(Label, loopLabel);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Subtract);
			code.add(JumpFalse, endLabel);
			
			//move stack pointer down
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, type.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//put first arg on stack pointer
			code.add(Exchange);
			opcodeForStore(code, type);
			
			//move stack pointer down again
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, subType.getSize());
			code.add(Subtract);
			code.add(Duplicate);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//put second arg on stack pointer
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			code.add(PushI, RunTime.ARRAY_HEADER_SIZE);
			code.add(Add);
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(PushI, subType.getSize());
			code.add(Multiply);
			code.add(Add);
			opcodeForLoad(code, subType);
			opcodeForStore(code, subType);
			
			//put variables on stack
			Macros.loadIFrom(code, RunTime.LOOP_CURRENT_INDEX);
			Macros.loadIFrom(code, RunTime.ARRAY_LENGTH);
			Macros.loadIFrom(code, RunTime.ARRAY_ADDRESS_ORIGINAL);

			//call function
			code.append(removeAddressCode(node.child(1)));
			code.add(LoadI);
			code.add(CallV);
			
			//put variables back
			Macros.storeITo(code, RunTime.ARRAY_ADDRESS_ORIGINAL);
			Macros.storeITo(code, RunTime.ARRAY_LENGTH);
			Macros.storeITo(code, RunTime.LOOP_CURRENT_INDEX);

			//get return value
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			turnAddressIntoValue(code, type);
			
			//move stack pointer back up
			Macros.loadIFrom(code, RunTime.STACK_POINTER);
			code.add(PushI, type.getSize());
			code.add(Add);
			Macros.storeITo(code, RunTime.STACK_POINTER);
			
			//increment
			Macros.incrementInteger(code, RunTime.LOOP_CURRENT_INDEX);
			code.add(Jump, loopLabel);
			
			code.add(Label, endLabel);
		}
		
		///////////////////////////////////////////////////////////////////////////
		// leaf nodes (ErrorNode not necessary)
		public void visit(BooleanConstantNode node) {
			newValueCode(node);
			code.add(PushI, node.getValue() ? 1 : 0);
		}
		public void visit(IdentifierNode node) {
			newAddressCode(node);
			Binding binding = node.getBinding();
			
			binding.generateAddress(code);
		}		
		public void visit(IntegerConstantNode node) {
			newValueCode(node);
			
			code.add(PushI, node.getValue());
		}
		public void visit(CharConstantNode node) {
			newValueCode(node);
			
			code.add(PushI, node.getValue());
		}
		public void visit(FloatingConstantNode node) {
			newValueCode(node);
			code.add(PushF, node.getValue());
		}
		public void visit(StringConstantNode node) {
			newValueCode(node);
			String s = node.getValue();
			code.add(PushI, s.length());
			RunTime.CreateEmptyString(code, 9);
				
			for(int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				code.add(Duplicate);
				code.add(PushI, RunTime.STRING_HEADER_SIZE);
				code.add(Add);
				code.add(PushI, i);
				code.add(Add);
				code.add(PushI, c);
				code.add(StoreC);
			}
			code.add(Duplicate);
			code.add(PushI, RunTime.STRING_HEADER_SIZE);
			code.add(Add);
			code.add(PushI, s.length());
			code.add(Add);
			code.add(PushI, '\0');
			code.add(StoreC);
		}	
		
		public void visit(JumpNode node) {
			newVoidCode(node);
			if(node.getToken().isLextant(Keyword.BREAK)) {
				code.add(Jump, node.getBreakLabel());
			} else if(node.getToken().isLextant(Keyword.CONTINUE)) {
				code.add(Jump, node.getContinueLabel());
			}
		}
	}

}
