package asmCodeGenerator;

import static asmCodeGenerator.codeStorage.ASMOpcode.*;

import asmCodeGenerator.codeStorage.ASMCodeFragment;
import asmCodeGenerator.codeStorage.ASMCodeFragment.CodeType;
import asmCodeGenerator.runtime.RunTime;
import parseTree.ParseNode;
import semanticAnalyzer.types.PrimitiveType;

public class CastCodeGenerator implements SimpleCodeGenerator {
	public ASMCodeFragment generate(ParseNode node) {
		ASMCodeFragment fragment = new ASMCodeFragment(CodeType.GENERATES_VALUE);
		fragment.add(Pop);
		if(node.child(0).getType() == node.child(1).getType()) {
			return fragment;
		}
		
		if(node.child(1).getType() == PrimitiveType.INTEGER) {
			if(node.child(0).getType() == PrimitiveType.FLOATING) {
				fragment.add(ConvertI);
			} else if(node.child(0).getType() == PrimitiveType.RATIONAL) {
				fragment.add(Divide);
			}
			
		} else if(node.child(1).getType() == PrimitiveType.FLOATING) {
			if(node.child(0).getType() == PrimitiveType.RATIONAL) {
				fragment.add(Exchange);
				fragment.add(ConvertF);
				fragment.add(Exchange);
				fragment.add(ConvertF);
				fragment.add(FDivide);
			} else {
				fragment.add(ConvertF);
			}
			
		} else if(node.child(1).getType() == PrimitiveType.BOOLEAN) {
			Labeller labeller = new Labeller("cast");
			String falseLabel = labeller.newLabel("false");
			String trueLabel = labeller.newLabel("true");
			String joinLabel = labeller.newLabel("join");
			
			fragment.add(JumpFalse, falseLabel);
			fragment.add(Jump, trueLabel);
			fragment.add(Label, trueLabel);
			fragment.add(PushI, 1);
			fragment.add(Jump, joinLabel);
			fragment.add(Label, falseLabel);
			fragment.add(PushI, 0);
			fragment.add(Jump, joinLabel);
			fragment.add(Label, joinLabel);
			
		} else if(node.child(1).getType() == PrimitiveType.CHAR) {
			fragment.add(PushI, 127);
			fragment.add(BTAnd);
			
		} else if(node.child(1).getType() == PrimitiveType.RATIONAL) {
			if(node.child(0).getType() == PrimitiveType.FLOATING) {
				fragment.add(PushI, 223092870);
				fragment.add(ConvertF);
				fragment.add(FMultiply);
				fragment.add(ConvertI);
				fragment.add(PushI, 223092870);
				fragment.add(Call, RunTime.LOWEST_TERMS);
			} else {
				fragment.add(PushI, 1);
			}
		}
		return fragment;
	}
}