package parser;

import java.util.ArrayList;
import java.util.Arrays;

import inputHandler.TextLocation;
import logging.PikaLogger;
import parseTree.*;
import parseTree.nodeTypes.CreateEmptyArrayNode;
import parseTree.nodeTypes.ArrayNode;
import parseTree.nodeTypes.AssignmentNode;
import parseTree.nodeTypes.BinaryOperatorNode;
import parseTree.nodeTypes.BooleanConstantNode;
import parseTree.nodeTypes.CallNode;
import parseTree.nodeTypes.CharConstantNode;
import parseTree.nodeTypes.ConditionalNode;
import parseTree.nodeTypes.BlockNode;
import parseTree.nodeTypes.DeclarationNode;
import parseTree.nodeTypes.ErrorNode;
import parseTree.nodeTypes.ExpressionListNode;
import parseTree.nodeTypes.FloatingConstantNode;
import parseTree.nodeTypes.FoldNode;
import parseTree.nodeTypes.ForNode;
import parseTree.nodeTypes.FunctionInvocationNode;
import parseTree.nodeTypes.FunctionNode;
import parseTree.nodeTypes.IdentifierNode;
import parseTree.nodeTypes.IntegerConstantNode;
import parseTree.nodeTypes.JumpNode;
import parseTree.nodeTypes.LambdaNode;
import parseTree.nodeTypes.MapNode;
import parseTree.nodeTypes.NewlineNode;
import parseTree.nodeTypes.ParameterListNode;
import parseTree.nodeTypes.PrintStatementNode;
import parseTree.nodeTypes.ProcedureBlockNode;
import parseTree.nodeTypes.ProgramNode;
import parseTree.nodeTypes.ReduceNode;
import parseTree.nodeTypes.ReleaseArrayNode;
import parseTree.nodeTypes.ReturnNode;
import parseTree.nodeTypes.SpaceNode;
import parseTree.nodeTypes.StringConstantNode;
import parseTree.nodeTypes.TabNode;
import parseTree.nodeTypes.TrinaryOperatorNode;
import parseTree.nodeTypes.UnaryOperatorNode;
import parseTree.nodeTypes.VoidNode;
import parseTree.nodeTypes.ZipNode;
import semanticAnalyzer.types.Array;
import semanticAnalyzer.types.PrimitiveType;
import semanticAnalyzer.types.Type;
import tokens.*;
import lexicalAnalyzer.Keyword;
import lexicalAnalyzer.Lextant;
import lexicalAnalyzer.Punctuator;
import lexicalAnalyzer.Scanner;


public class Parser {
	private Scanner scanner;
	private Token nowReading;
	private Token previouslyRead;
	
	public static ParseNode parse(Scanner scanner) {
		Parser parser = new Parser(scanner);
		return parser.parse();
	}
	public Parser(Scanner scanner) {
		super();
		this.scanner = scanner;
	}
	
	public ParseNode parse() {
		readToken();
		return parseProgram();
	}

	////////////////////////////////////////////////////////////
	// "program" is the start symbol S
	// S -> EXEC mainBlock
	
	private ParseNode parseProgram() {
		ParseNode program = new ProgramNode(nowReading);
		
		while(nowReading.isLextant(Keyword.FUNC, Keyword.VAR, Keyword.CONST, Keyword.STATIC)) {
			if(nowReading.isLextant(Keyword.FUNC)) {
				ParseNode funcBlock = functionBlock();
				program.appendChild(funcBlock);
			} else {
				ParseNode declaration = parseDeclaration();
				program.appendChild(declaration);
			}
		}
				
		expect(Keyword.EXEC);
		ParseNode mainBlock = parseBlock();
		program.appendChild(mainBlock);
		
		if(!(nowReading instanceof NullToken)) {
			return syntaxErrorNode("end of program");
		}
		
		return program;
	}
	
	
	///////////////////////////////////////////////////////////
	// mainBlock
	
	// mainBlock -> { statement* }
	private ParseNode parseBlock() {
		if(!startsBlock(nowReading)) {
			return syntaxErrorNode("Block");
		}
		ParseNode block = new BlockNode(nowReading);
		expect(Punctuator.OPEN_BRACE);
		
		while(startsStatement(nowReading)) {
			ParseNode statement = parseStatement();
			block.appendChild(statement);
		}
		expect(Punctuator.CLOSE_BRACE);
		return block;
	}
	private ParseNode parseProcedureBlock() {
		if(!startsBlock(nowReading)) {
			return syntaxErrorNode("FunctionBlock");
		}
		ParseNode block = new ProcedureBlockNode(nowReading);
		expect(Punctuator.OPEN_BRACE);
		
		while(startsStatement(nowReading)) {
			ParseNode statement = parseStatement();
			block.appendChild(statement);
		}
		expect(Punctuator.CLOSE_BRACE);
		return block;
	}
	
	private boolean startsBlock(Token token) {
		return token.isLextant(Punctuator.OPEN_BRACE);
	}
	
	///////////////////////////////////////////////////////////
	// Functions
	private ParseNode functionBlock() {
		Token token = nowReading; // func
		readToken();
		IdentifierNode identifier = new IdentifierNode(nowReading);
		readToken();
		ParseNode lambda = parseLambda();
		return FunctionNode.withChildren(token, identifier, lambda);
	}
	
	private ParseNode parseLambda() {
		Token token = nowReading; //garbage token
		ParseNode parameters = parseParameters();
		expect(Punctuator.RETURNS);
		ParseNode returnType;
		if(nowReading.getLexeme() == "void") {
			returnType = createVoidNode();
		} else {
			returnType = parseType();
		}
		
		readToken();
		ParseNode body = parseProcedureBlock();
		return LambdaNode.withChildren(token, parameters, returnType, body);
		
	}
	
	private ParseNode createVoidNode() {
		Token token = nowReading;
		ParseNode type = null;
		type = new VoidNode(token);
		type.setType(PrimitiveType.VOID);
		return type;
	}
	
	private ParseNode parseParameters() {
		Token token = nowReading;
		expect(Punctuator.LESS);
		ArrayList<ParseNode> params = new ArrayList<ParseNode>();
		if(!nowReading.isLextant(Punctuator.GREATER)) {
			while(true) {
				ParseNode typeNode = parseType();
				readToken();
				IdentifierNode identifier = new IdentifierNode(nowReading);
				identifier.setType(typeNode.getType());
				params.add(identifier);
				readToken();
				if(nowReading.isLextant(Punctuator.SEPARATOR)) {
					readToken();
				} else {
					break;
				}
			}
		}
		expect(Punctuator.GREATER);
		return ParameterListNode.withChildren(token, params);
	}
	
	
	///////////////////////////////////////////////////////////
	// statements
	
	// statement-> declaration | printStmt | assignment | blockStatement | ifStatement | whileStatement | releaseStatement
	private ParseNode parseStatement() {
		if(!startsStatement(nowReading)) {
			return syntaxErrorNode("statement");
		}
		if(startsDeclaration(nowReading)) {
			return parseDeclaration();
		}
		if(startsJumpStatement(nowReading)) {
			return parseJumpStatement();
		}
		if(startsPrintStatement(nowReading)) {
			return parsePrintStatement();
		}
		if(startsReleaseStatement(nowReading)) {
			return parseReleaseStatement();
		}
		if(startsConditionalStatement(nowReading)) {
			return parseConditionalStatement();
		}
		if(startsIdentifier(nowReading)) {
			return parseAssignment();
		}
		if(startsSubBlock(nowReading)) {
			return parseBlock();
		}
		if(startsReturnStatement(nowReading)) {
			return parseReturnStatement();
		}
		if(startsCallStatement(nowReading)) {
			return parseCallStatement();
		}
		if(startsForStatement(nowReading)) {
			return parseForStatement();
		}
		return syntaxErrorNode("statement");
	}
	private boolean startsStatement(Token token) {
		return startsPrintStatement(token) ||
			   startsJumpStatement(token) ||
			   startsDeclaration(token) ||
			   startsIdentifier(token) ||
			   startsConditionalStatement(token) ||
			   startsReleaseStatement(token) ||
			   startsSubBlock(token) ||
			   startsReturnStatement(token) ||
			   startsCallStatement(token) ||
			   startsForStatement(token);
	}
	
	// printStmt -> PRINT printExpressionList .
	private ParseNode parsePrintStatement() {
		if(!startsPrintStatement(nowReading)) {
			return syntaxErrorNode("print statement");
		}
		PrintStatementNode result = new PrintStatementNode(nowReading);
		
		readToken();
		result = parsePrintExpressionList(result);
		
		expect(Punctuator.TERMINATOR);
		return result;
	}
	private boolean startsPrintStatement(Token token) {
		return token.isLextant(Keyword.PRINT);
	}	
	
	//release statement -> release array
	private ParseNode parseReleaseStatement() {
		if(!startsReleaseStatement(nowReading)) {
			return syntaxErrorNode("release statement");
		}
		Token token = nowReading;
		readToken();
		ParseNode r = parseExpression();
		expect(Punctuator.TERMINATOR);
		return ReleaseArrayNode.withChild(token, r);
		
	}
	
	private boolean startsReleaseStatement(Token token) {
		return token.isLextant(Keyword.RELEASE);
	}
	
	// breakContinueStatement -> break or continue
	private ParseNode parseJumpStatement() {
		Token token = nowReading;
		readToken();
		expect(Punctuator.TERMINATOR);
		return new JumpNode(token);
	}
	
	
	private boolean startsJumpStatement(Token token) {
		return token.isLextant(Keyword.BREAK, Keyword.CONTINUE);
	}

	// This adds the printExpressions it parses to the children of the given parent
	// printExpressionList -> printExpression* bowtie (,|;)  (note that this is nullable)

	private PrintStatementNode parsePrintExpressionList(PrintStatementNode parent) {
		while(startsPrintExpression(nowReading) || startsPrintSeparator(nowReading)) {
			parsePrintExpression(parent);
			parsePrintSeparator(parent);
		}
		return parent;
	}
	

	// This adds the printExpression it parses to the children of the given parent
	// printExpression -> (expr | nl)?     (nullable)
	
	private void parsePrintExpression(PrintStatementNode parent) {
		if(startsExpression(nowReading)) {
			ParseNode child = parseExpression();
			parent.appendChild(child);
		}
		else if(nowReading.isLextant(Keyword.NEWLINE)) {
			readToken();
			ParseNode child = new NewlineNode(previouslyRead);
			parent.appendChild(child);
		}
		else if(nowReading.isLextant(Keyword.TAB)) {
			readToken();
			ParseNode child = new TabNode(previouslyRead);
			parent.appendChild(child);
		}
		// else we interpret the printExpression as epsilon, and do nothing
	}
	private boolean startsPrintExpression(Token token) {
		return startsExpression(token) || token.isLextant(Keyword.NEWLINE, Keyword.TAB);
	}
	
	
	// This adds the printExpression it parses to the children of the given parent
	// printExpression -> expr? ,? nl? 
	
	private void parsePrintSeparator(PrintStatementNode parent) {
		if(!startsPrintSeparator(nowReading) && !nowReading.isLextant(Punctuator.TERMINATOR)) {
			ParseNode child = syntaxErrorNode("print separator");
			parent.appendChild(child);
			return;
		}
		
		if(nowReading.isLextant(Punctuator.SPACE)) {
			readToken();
			ParseNode child = new SpaceNode(previouslyRead);
			parent.appendChild(child);
		}
		else if(nowReading.isLextant(Punctuator.SEPARATOR)) {
			readToken();
		}		
		else if(nowReading.isLextant(Punctuator.TERMINATOR)) {
			// we're at the end of the bowtie and this printSeparator is not required.
			// do nothing.  Terminator is handled in a higher-level nonterminal.
		}
	}
	private boolean startsPrintSeparator(Token token) {
		return token.isLextant(Punctuator.SEPARATOR, Punctuator.SPACE) ;
	}
	
	
	// declaration -> CONST identifier := expression .
	private ParseNode parseDeclaration() {
		if(!startsDeclaration(nowReading)) {
			return syntaxErrorNode("declaration");
		}
		
		if(nowReading.isLextant(Keyword.STATIC)) {
			readToken();
		}
		
		Token declarationToken = nowReading;
		readToken();
		
		ParseNode identifier = parseIdentifier();
		expect(Punctuator.ASSIGN);
		ParseNode initializer = parseExpression();
		expect(Punctuator.TERMINATOR);
		
		return DeclarationNode.withChildren(declarationToken, identifier, initializer);
	}
	private boolean startsDeclaration(Token token) {
		return token.isLextant(Keyword.CONST, Keyword.VAR, Keyword.STATIC);
	}
	
	// assignment -> target := expression
	
	private ParseNode parseAssignment() {
		
		Token assignmentToken = nowReading;
		ParseNode identifier = parseParentheticalExpression();
		expect(Punctuator.ASSIGN);
		ParseNode initializer = parseExpression();
		expect(Punctuator.TERMINATOR);
		
		return AssignmentNode.withChildren(assignmentToken, identifier, initializer);
	}
	
	// subscope -> [statement]*
	
	private boolean startsSubBlock(Token token) {
		return token.isLextant(Punctuator.OPEN_BRACE);
	}
	
	// if(expression) blockStatement (else blockStatement)? |
	// while(expression) blockStatement
	
	private ParseNode parseConditionalStatement() {
		Token conditionToken = nowReading;
		readToken();
		expect(Punctuator.OPEN_PARENTHESIS);
		ParseNode expression = parseExpression();
		expect(Punctuator.CLOSED_PARENTHESIS);
		ParseNode trueBlock = parseBlock();
		if(nowReading.isLextant(Keyword.ELSE)) {
			if(conditionToken.isLextant(Keyword.WHILE)) {
				syntaxError(nowReading, "while loops cannot have an else clause");
			}
			readToken();
			ParseNode elseBlock = parseBlock();
			return ConditionalNode.withChildren(conditionToken, expression, trueBlock, elseBlock);
		} else {
			ParseNode elseBlock = new BlockNode(nowReading);
			return ConditionalNode.withChildren(conditionToken, expression, trueBlock, elseBlock);
		}
		
	}
	
	private boolean startsConditionalStatement(Token token) {
		return token.isLextant(Keyword.IF, Keyword.WHILE);
	}
	
	
	
	//Return and call statements
	private ParseNode parseReturnStatement() {
		Token returnToken = nowReading;
		readToken();
		if(nowReading.isLextant(Punctuator.TERMINATOR)) {
			readToken();
			return new ReturnNode(returnToken);
		} else {
			ParseNode returnValue = parseExpression();
			expect(Punctuator.TERMINATOR);
			return ReturnNode.withChild(returnToken, returnValue);
		}
	}
	
	private boolean startsReturnStatement(Token token) {
		return token.isLextant(Keyword.RETURN);
	}
	
	private ParseNode parseCallStatement() {
		Token token = nowReading;
		readToken();
		//start anywhere above it in descent tree, node type will be checked in semantic analyzer
		CallNode call = CallNode.withChild(token, parseParentheticalExpression());
		expect(Punctuator.TERMINATOR);
		return call;
	}
	
	private boolean startsCallStatement(Token token) {
		return token.isLextant(Keyword.CALL);
	}
	
	//for statement
	private ParseNode parseForStatement() {
		readToken();
		Token token = nowReading; //elem or index
		readToken();
		ParseNode identifier = parseIdentifier();
		expect(Keyword.OF);
		ParseNode expression = parseExpression();
		ParseNode body = parseBlock();
		return ForNode.withChildren(token, expression, identifier, body);
	}
	
	private boolean startsForStatement(Token token) {
		return token.isLextant(Keyword.FOR);
	}
	
	
	///////////////////////////////////////////////////////////
	// expressions
	// expr                     -> comparisonExpression
	// comparisonExpression     -> additiveExpression [> additiveExpression]?
	// additiveExpression       -> multiplicativeExpression [+ multiplicativeExpression]*  (left-assoc)
	// multiplicativeExpression -> atomicExpression [MULT atomicExpression]*  (left-assoc)
	// atomicExpression         -> literal
	// literal                  -> intNumber | identifier | booleanConstant | floatNumber | FloatingConstant

	// expr  -> comparisonExpression
	private ParseNode parseExpression() {		
		if(!startsExpression(nowReading)) {
			return syntaxErrorNode("expression");
		}
		return parseOrExpression();
	}
	
	private boolean startsExpression(Token token) {
		return startsOrExpression(token);
	}
	
	private ParseNode parseOrExpression() {
		if(!startsOrExpression(nowReading)) {
			return syntaxErrorNode("or expression");
		}
		ParseNode left = parseAndExpression();
		if(nowReading.isLextant(Punctuator.OR)) {
			Token orToken = nowReading;
			readToken();
			ParseNode right = parseAndExpression();
			return BinaryOperatorNode.withChildren(orToken, left, right);
		}
		return left;
	}
	
	private boolean startsOrExpression(Token token) {
		return startsAndExpression(token);
	}
	
	private ParseNode parseAndExpression() {
		if(!startsOrExpression(nowReading)) {
			return syntaxErrorNode("or expression");
		}
		ParseNode left = parseComparisonExpression();
		if(nowReading.isLextant(Punctuator.AND)) {
			Token andToken = nowReading;
			readToken();
			ParseNode right = parseComparisonExpression();
			return BinaryOperatorNode.withChildren(andToken, left, right);
		}
		return left;
	}
	
	private boolean startsAndExpression(Token token) {
		return startsComparisonExpression(token);
	}

	// comparisonExpression -> additiveExpression [> additiveExpression]?
	private ParseNode parseComparisonExpression() {
		if(!startsComparisonExpression(nowReading)) {
			return syntaxErrorNode("comparison expression");
		}
		
		ParseNode left = parseAdditiveExpression();
		if(nowReading.isLextant(Punctuator.GREATER, Punctuator.GREATER_EQ, Punctuator.LESS,
								Punctuator.LESS_EQ, Punctuator.EQUAL, Punctuator.NOT_EQUAL)) {
			Token compareToken = nowReading;
			readToken();
			ParseNode right = parseAdditiveExpression();
			
			return BinaryOperatorNode.withChildren(compareToken, left, right);
		}
		return left;

	}
	private boolean startsComparisonExpression(Token token) {
		return startsAdditiveExpression(token);
	}

	// additiveExpression -> multiplicativeExpression [+ multiplicativeExpression]*  (left-assoc)
	private ParseNode parseAdditiveExpression() {
		if(!startsAdditiveExpression(nowReading)) {
			return syntaxErrorNode("additiveExpression");
		}
		
		ParseNode left = parseMultiplicativeExpression();
		while(nowReading.isLextant(Punctuator.ADD, Punctuator.SUBTRACT)) {
			Token additiveToken = nowReading;
			readToken();
			ParseNode right = parseMultiplicativeExpression();
			
			left = BinaryOperatorNode.withChildren(additiveToken, left, right);
		}
		return left;
	}
	private boolean startsAdditiveExpression(Token token) {
		return startsLiteral(token) || startsParentheticalExpression(token) || startsUnaryExpression(token) || startsMapExpression(token) ||
				startsReduceExpression(token) || startsFoldExpression(token);
	}	

	// multiplicativeExpression -> parenthetical operation [MULT parentheticalExpression]*  (left-assoc)
	private ParseNode parseMultiplicativeExpression() {
		if(!startsMultiplicativeExpression(nowReading)) {
			return syntaxErrorNode("multiplicativeExpression");
		}
		
		ParseNode left = parseFoldExpression();
		while(nowReading.isLextant(Punctuator.MULTIPLY, Punctuator.DIVIDE, Punctuator.OVER, Punctuator.EXPRESS_OVER, Punctuator.RATIONALIZE)) {
			Token multiplicativeToken = nowReading;
			readToken();
			ParseNode right = parseFoldExpression();
			
			left = BinaryOperatorNode.withChildren(multiplicativeToken, left, right);
		}
		return left;
	}
	private boolean startsMultiplicativeExpression(Token token) {
		return startsAtomicExpression(token);
	}
	
	private ParseNode parseFoldExpression() {
		ParseNode left = parseMapReduceExpression();
		if(startsFoldExpression(nowReading)) {
			Token token = nowReading;
			readToken();
			if(nowReading.isLextant(Punctuator.OPEN_BRACKET)) {
				readToken();
				ParseNode base = parseExpression();
				expect(Punctuator.CLOSED_BRACKET);
				ParseNode right = parseExpression();
				left = FoldNode.withBase(token, left, right, base);
			} else {
				ParseNode right = parseExpression();
				left = FoldNode.withoutBase(token, left, right);
			}
		}
		return left;
	}
	
	private boolean startsFoldExpression(Token token) {
		return token.isLextant(Keyword.FOLD);
	}
	
	private ParseNode parseMapReduceExpression() {
		ParseNode left = parseUnaryExpression();
		if(startsMapExpression(nowReading)) {
			Token token = nowReading;
			readToken();
			ParseNode right = parseExpression();
			left = MapNode.withChildren(token, left, right);
		}
		if(startsReduceExpression(nowReading)) {
			Token token = nowReading;
			readToken();
			ParseNode right = parseExpression();
			left = ReduceNode.withChildren(token, left, right);
		}
		return left;
	}
	
	private boolean startsMapExpression(Token token) {
		return token.isLextant(Keyword.MAP);
	}
	
	private boolean startsReduceExpression(Token token) {
		return token.isLextant(Keyword.REDUCE);
	}
	
	private ParseNode parseUnaryExpression() {
		if(startsUnaryExpression(nowReading)) {
			if(nowReading.isLextant(Keyword.ZIP)) {
				Token token = nowReading;
				readToken();
				ParseNode left = parseExpression();
				expect(Punctuator.SEPARATOR);
				ParseNode right = parseExpression();
				expect(Punctuator.SEPARATOR);
				ParseNode lambda = parseExpression();
				return ZipNode.withChildren(token, left, right, lambda);
			} else {
				Token token = nowReading;
				readToken();
				ParseNode right = parseExpression();
				return UnaryOperatorNode.withChild(token, right);
			}
		}		
		
		return parseParentheticalExpression();
	}
	
	private boolean startsUnaryExpression(Token token) {
		return token.isLextant(Punctuator.NOT, Keyword.CLONE, Keyword.LENGTH, Keyword.REVERSE, Keyword.ZIP);
	}
	
	private Type parseSubtype() {
		if(nowReading.isLextant(Punctuator.OPEN_BRACKET)) {
			readToken();
			Array arr = new Array(parseSubtype());
			expect(Punctuator.CLOSED_BRACKET);
			return arr;
		} else if(nowReading.getLexeme() == "int") {
			readToken();
			return PrimitiveType.INTEGER;
		} else if(nowReading.getLexeme() == "rat") {
			readToken();
			return PrimitiveType.RATIONAL;
		} else if(nowReading.getLexeme() == "bool") {
			readToken();
			return PrimitiveType.BOOLEAN;
		} else if(nowReading.getLexeme() == "float") {
			readToken();
			return PrimitiveType.FLOATING;
		} else if(nowReading.getLexeme() == "char") {
			readToken();
			return PrimitiveType.CHAR;
		} else if(nowReading.getLexeme() == "string") {
			readToken();
			return PrimitiveType.STRING;
		} else {
			error(nowReading.getLexeme() + " is not a valid type.");
			readToken();
			return PrimitiveType.ERROR;
		}
		
	}
	private ParseNode parseType() {
		TextLocation loc = nowReading.getLocation();
		ParseNode type = null;
		if(nowReading.getLexeme() == "bool") {
			LextantToken t = LextantToken.make(loc, Keyword.TRUE.getLexeme(), Keyword.TRUE);
			type = new BooleanConstantNode(t);
			type.setType(PrimitiveType.BOOLEAN);
		
		} else if(nowReading.getLexeme() == "char") {
			CharConstantToken t = CharConstantToken.make(loc, "c"); 
			type = new CharConstantNode(t);
			type.setType(PrimitiveType.CHAR);
		
		} else if(nowReading.getLexeme() == "string") {
			StringConstantToken t = StringConstantToken.make(loc, "c");
			type = new StringConstantNode(t);
			type.setType(PrimitiveType.STRING);
		
		} else if(nowReading.getLexeme() == "int") {
			IntegerConstantToken t = IntegerConstantToken.make(loc, "0");
			type = new IntegerConstantNode(t);
			type.setType(PrimitiveType.INTEGER);
		
		} else if(nowReading.getLexeme() == "float") {
			FloatingConstantToken t = FloatingConstantToken.make(loc, "0.0");
			type = new FloatingConstantNode(t);
			type.setType(PrimitiveType.FLOATING);
		
		} else if(nowReading.getLexeme() == "rat") {
			IntegerConstantToken t = IntegerConstantToken.make(loc, "0");
			type = new IntegerConstantNode(t);
			type.setType(PrimitiveType.RATIONAL);
		} else if(nowReading.isLextant(Punctuator.OPEN_BRACKET)) {
			expect(Punctuator.OPEN_BRACKET);
			Array arr = new Array(parseSubtype());
			type = new ArrayNode(nowReading);
			type.setType(arr);
		} else {
			error(nowReading.getLexeme() + " is not a valid type.");
		}
		return type;
	}
	
	// parentheticalExpression -> [atomic operation]* | [subexpr] 
	private ParseNode parseParentheticalExpression() {	
		//empty array creation
		if(nowReading.isLextant(Keyword.NEW)) {
			Token token = nowReading; // new
			readToken();
			
			expect(Punctuator.OPEN_BRACKET);
			Array arr = new Array(parseSubtype());
			expect(Punctuator.CLOSED_BRACKET);
			ArrayNode arrNode = new ArrayNode(nowReading);
			arrNode.setType(arr);
			
			expect(Punctuator.OPEN_PARENTHESIS);
			ParseNode right = parseExpression();
			expect(Punctuator.CLOSED_PARENTHESIS);
			
			return CreateEmptyArrayNode.withChildren(token, arrNode, right);
			
		}
		//lambda
		if(nowReading.isLextant(Punctuator.LESS)) {
			Token token = nowReading;
			ParseNode lambda = parseLambda();
			return FunctionNode.withChild(token, lambda);
		}
		//parentheses
		if(nowReading.isLextant(Punctuator.OPEN_PARENTHESIS)) {
			readToken();
			ParseNode ret = parseExpression();
			if(!nowReading.isLextant(Punctuator.CLOSED_PARENTHESIS)) {
				return syntaxErrorNode(")");
			}
			readToken();
			return ret;
		}
		while(nowReading.isLextant(Punctuator.OPEN_BRACKET)) {
			readToken();
			
			if(nowReading.isLextant(Punctuator.CLOSED_BRACKET)) {
				Token token = nowReading;
				ArrayList<ParseNode> nodes = new ArrayList<ParseNode>();
				readToken();
				return ExpressionListNode.withChildren(token, nodes);
			}
			
			ParseNode expr = parseExpression();
			
			// casting
			if(nowReading.isLextant(Punctuator.BAR)) {
				Token token = nowReading; // |
				readToken();
				ParseNode type = parseType();
				readToken();
				if(!nowReading.isLextant(Punctuator.CLOSED_BRACKET)) {
					return syntaxErrorNode("]");
				}
				readToken();
				return BinaryOperatorNode.withChildren(token, expr, type);
				
			} 
			// array with expression list
			else if(nowReading.isLextant(Punctuator.SEPARATOR, Punctuator.CLOSED_BRACKET)){
				ArrayList<ParseNode> nodes = new ArrayList<ParseNode>();
				nodes.add(expr);
				Token token = nowReading; //garbage token
				
				while(nowReading.isLextant(Punctuator.SEPARATOR)) {
					readToken();
					nodes.add(parseExpression());
				}
				expect(Punctuator.CLOSED_BRACKET);
				return ExpressionListNode.withChildren(token, nodes);
				
			} else {
				return syntaxErrorNode("] | or ,");
			}
		}
		//array indexing
		ParseNode left = parseAtomicExpression();
		while(nowReading.isLextant(Punctuator.OPEN_BRACKET)) {
			Token realToken = nowReading;
			Token indexToken = LextantToken.artificial(realToken, Punctuator.ARRAY_INDEXING);
			readToken();
			ParseNode index = parseExpression();
			
			if(nowReading.isLextant(Punctuator.SEPARATOR)) {
				readToken();
				ParseNode index2 = parseExpression();
				expect(Punctuator.CLOSED_BRACKET);
				left = TrinaryOperatorNode.withChildren(indexToken, left, index, index2);
				left.setType(PrimitiveType.STRING);
			} else {
				expect(Punctuator.CLOSED_BRACKET);
				left = BinaryOperatorNode.withChildren(indexToken, left, index);
				left.setType(new Array(index.getType())); //TODO: likely cause of array matching bug
			}
		}
		//function invocation
		if(nowReading.isLextant(Punctuator.OPEN_PARENTHESIS)) {
			Token token = nowReading;
			readToken();
			ArrayList<ParseNode> nodes = new ArrayList<ParseNode>();
			if(!nowReading.isLextant(Punctuator.CLOSED_PARENTHESIS)) {
				nodes.add(parseExpression());
				while(nowReading.isLextant(Punctuator.SEPARATOR)) {
					readToken();
					nodes.add(parseExpression());
				}
			}
			expect(Punctuator.CLOSED_PARENTHESIS);
			//ParameterListNode parameters = ParameterListNode.withChildren(token, nodes);
			left = FunctionInvocationNode.withChildren(token, left, nodes);
		}
		return left;
	}
	
	private boolean startsParentheticalExpression(Token token) {
		return token.isLextant(Punctuator.OPEN_PARENTHESIS, Punctuator.OPEN_BRACKET, Keyword.NEW, Punctuator.LESS);
	}
	
	// atomicExpression -> literal
	private ParseNode parseAtomicExpression() {
		if(!startsAtomicExpression(nowReading)) {
			return syntaxErrorNode("atomic expression");
		}
		return parseLiteral();
	}
	private boolean startsAtomicExpression(Token token) {
		return startsLiteral(token) || startsParentheticalExpression(token) || startsUnaryExpression(token) || startsMapExpression(token) ||
				startsReduceExpression(token) || startsFoldExpression(token);
	}
	
	// literal -> integer | identifier | boolean | float | char
	private ParseNode parseLiteral() {
		if(!startsLiteral(nowReading)) {
			return syntaxErrorNode("literal");
		}
		
		if(startsIntNumber(nowReading)) {
			return parseIntNumber();
		}
		if(startsFloatNumber(nowReading)) {
			return parseFloatNumber();
		}
		if(startsChar(nowReading)) {
			return parseChar();
		}
		if(startsIdentifier(nowReading)) {
			return parseIdentifier();
		}
		if(startsBooleanConstant(nowReading)) {
			return parseBooleanConstant();
		}
		
		if(startsStringConstant(nowReading)) {
			return parseStringConstant();
		}

		return syntaxErrorNode("literal");
	}
	private boolean startsLiteral(Token token) {
		return startsIntNumber(token) ||
			   startsFloatNumber(token) ||
			   startsChar(token) ||
			   startsIdentifier(token) || 
			   startsBooleanConstant(token) ||
			   startsStringConstant(token);
	}

	// number (terminal)
	private ParseNode parseIntNumber() {
		if(!startsIntNumber(nowReading)) {
			return syntaxErrorNode("integer constant");
		}
		readToken();
		return new IntegerConstantNode(previouslyRead);
	}
	
	private ParseNode parseFloatNumber() {
		if(!startsFloatNumber(nowReading)) {
			return syntaxErrorNode("floating constant");
		}
		readToken();
		return new FloatingConstantNode(previouslyRead);
	}
	
	private ParseNode parseChar() {
		if(!startsChar(nowReading)) {
			return syntaxErrorNode("char constant");
		}
		readToken();
		return new CharConstantNode(previouslyRead);
	}
	
	// identifier (terminal)
	private ParseNode parseIdentifier() {
		if(!startsIdentifier(nowReading)) {
			return syntaxErrorNode("identifier");
		}
		readToken();
		IdentifierNode id = new IdentifierNode(previouslyRead);
		return id;
	}
	
	// boolean constant (terminal)
	private ParseNode parseBooleanConstant() {
		if(!startsBooleanConstant(nowReading)) {
			return syntaxErrorNode("boolean constant");
		}
		readToken();
		return new BooleanConstantNode(previouslyRead);
	}
	
	private ParseNode parseStringConstant() {
		if(!startsStringConstant(nowReading)) {
			return syntaxErrorNode("string constant");
		}
		readToken();
		return new StringConstantNode(previouslyRead);
	}
	
	private boolean startsIntNumber(Token token) {
		return token instanceof IntegerConstantToken;
	}
	
	private boolean startsFloatNumber(Token token) {
		return token instanceof FloatingConstantToken;
	}
	
	private boolean startsChar(Token token) {
		return token instanceof CharConstantToken;
	}
	
	private boolean startsIdentifier(Token token) {
		return token instanceof IdentifierToken;
	}

	private boolean startsBooleanConstant(Token token) {
		return token.isLextant(Keyword.TRUE, Keyword.FALSE);
	}
	
	private boolean startsStringConstant(Token token) {
		return token instanceof StringConstantToken;
	}

	private void readToken() {
		previouslyRead = nowReading;
		nowReading = scanner.next();
	}	
	
	// if the current token is one of the given lextants, read the next token.
	// otherwise, give a syntax error and read next token (to avoid endless looping).
	private void expect(Lextant ...lextants ) {
		if(!nowReading.isLextant(lextants)) {
			syntaxError(nowReading, "expecting " + Arrays.toString(lextants));
		}
		readToken();
	}	
	private ErrorNode syntaxErrorNode(String expectedSymbol) {
		syntaxError(nowReading, "expecting " + expectedSymbol);
		ErrorNode errorNode = new ErrorNode(nowReading);
		readToken();
		return errorNode;
	}
	private void syntaxError(Token token, String errorDescription) {
		String message = "" + token.getLocation() + " " + errorDescription;
		error(message);
	}
	private void error(String message) {
		PikaLogger log = PikaLogger.getLogger("compiler.Parser");
		log.severe("syntax error: " + message);
	}	
}

